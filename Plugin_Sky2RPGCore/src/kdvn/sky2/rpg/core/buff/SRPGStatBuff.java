package kdvn.sky2.rpg.core.buff;

import kdvn.sky2.rpg.core.stat.SRPGStat;

public class SRPGStatBuff {
	
	private SRPGStat stat;
	private int value;
	private boolean isPercent;
	
	private long start;
	private long time;
	
	public SRPGStatBuff(SRPGStat stat, double value, boolean isPercent, long start, long time) {
		this.stat = stat;
		this.value = new Double(value).intValue();
		this.isPercent = isPercent;
		this.start = start;
		this.time = time;
	}
	
	public SRPGStat getStat() {
		return this.stat;
	}
	
	public int getValue() {
		return this.value;
	}
	
	public boolean isPercentBuff() {
		return this.isPercent;
	}
	
	public long getStart() {
		return this.start;
	}
	
	public void addTime(long time) {
		this.time += time;
	}
	
	public long getTime() {
		return this.time;
	}
	
	public boolean isStillEffective() {
		return this.start + this.time > System.currentTimeMillis();
	}
	
	public int getSecondsRemain() {
		return new Double((this.time - (System.currentTimeMillis() - this.start)) / 1000).intValue();
	}
	
}
