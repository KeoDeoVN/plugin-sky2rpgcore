package kdvn.sky2.rpg.core.classes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import kdvn.sky2.rpg.core.skill.SRPGSkill;

public enum SRPGClass {
	
	KIEM_KHACH(1, "Kiếm khách", "KK", "§c", Arrays.asList(new SRPGSkill[] {SRPGSkill.SAT_QUY, SRPGSkill.BAO_KIEM})),
	CHIEN_BINH(1, "Chiến binh", "CB", "§6", Arrays.asList(new SRPGSkill[] {SRPGSkill.KIEN_CUONG, SRPGSkill.DIA_CHAN})),
	CUNG_THU(1, "Cung thủ", "CT", "§a", Arrays.asList(new SRPGSkill[] {SRPGSkill.DOC_TIEN, SRPGSkill.TAN_XA_TIEN})),
	THAN_RIU(1, "Thần rìu", "TR", "§b", Arrays.asList(new SRPGSkill[] {SRPGSkill.GIANG_DON, SRPGSkill.RIU_SET})),
	
	KIEM_KHACH_PLUS(2, "Kiếm khách+", "KK+", "§c", Arrays.asList(new SRPGSkill[] {SRPGSkill.SAT_QUY, SRPGSkill.BAO_KIEM, SRPGSkill.QUET_KIEM, SRPGSkill.CUONG_PHONG})),
	CHIEN_BINH_PLUS(2, "Chiến binh+", "CB+", "§6", Arrays.asList(new SRPGSkill[] {SRPGSkill.KIEN_CUONG, SRPGSkill.DIA_CHAN, SRPGSkill.NOI_CONG, SRPGSkill.TRUNG_PHAT})),
	CUNG_THU_PLUS(2, "Cung thủ+", "CT+", "§a", Arrays.asList(new SRPGSkill[] {SRPGSkill.DOC_TIEN, SRPGSkill.TAN_XA_TIEN, SRPGSkill.HOI_PHUC, SRPGSkill.MUA_TIEN})),
	THAN_RIU_PLUS(2, "Thần rìu+", "TR+", "§b", Arrays.asList(new SRPGSkill[] {SRPGSkill.GIANG_DON, SRPGSkill.RIU_SET, SRPGSkill.XONG_PHA, SRPGSkill.TAN_THE_RAGNAROK}));
	
	private int level;
	private String name;
	private String color;
	private String shortName;
	
	private List<SRPGSkill> skills = new ArrayList<SRPGSkill> ();
	
	private SRPGClass() {}
	
	private SRPGClass(int level, String name, String shortName, String color, List<SRPGSkill> skills) {
		this.level = level;
		this.name = name;
		this.color = color;
		this.skills = skills;
		this.shortName = shortName;
	}
	
	public int getLevel1() {
		return this.level;
	}
	
	public String getName1() {
		return this.name;
	}
	
	public String getColorCode1() {
		return this.color;
	}
	
	public List<SRPGSkill> getSkills1() {
		return this.skills;
	}
	
	public String getShortName1() {
		return this.shortName;
	}
	
}
