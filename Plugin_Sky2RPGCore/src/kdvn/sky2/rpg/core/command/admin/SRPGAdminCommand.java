package kdvn.sky2.rpg.core.command.admin;

import java.util.LinkedHashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import kdvn.sky2.rpg.core.buff.SRPGExpBuff;
import kdvn.sky2.rpg.core.buff.SRPGStatBuff;
import kdvn.sky2.rpg.core.cuonghoa.SRPGDaCuongHoa;
import kdvn.sky2.rpg.core.gem.SRPGGem;
import kdvn.sky2.rpg.core.gem.SRPGGemUtils;
import kdvn.sky2.rpg.core.gui.chuyenhoa.SRPGGUIChuyenHoa;
import kdvn.sky2.rpg.core.gui.cuonghoa.SRPGGUICuongHoa;
import kdvn.sky2.rpg.core.gui.duclo.SRPGGUIDucLo;
import kdvn.sky2.rpg.core.gui.enchant.SRPGGUIEnchant;
import kdvn.sky2.rpg.core.gui.epda.SRPGGUIEpDa;
import kdvn.sky2.rpg.core.gui.info.SRPGGUIInfo;
import kdvn.sky2.rpg.core.gui.potential.SRPGGUIPotential;
import kdvn.sky2.rpg.core.gui.see.SRPGGUISee;
import kdvn.sky2.rpg.core.gui.tachda.SRPGGUITachDa;
import kdvn.sky2.rpg.core.gui.tachphuphep.SRPGGUITachPhuPhep;
import kdvn.sky2.rpg.core.item.SItem;
import kdvn.sky2.rpg.core.item.SRPGItem;
import kdvn.sky2.rpg.core.item.SRPGItemUtils;
import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.mob.SRPGMob;
import kdvn.sky2.rpg.core.mob.SRPGMobType;
import kdvn.sky2.rpg.core.mob.SRPGMobUtils;
import kdvn.sky2.rpg.core.player.SRPGPlayer;
import kdvn.sky2.rpg.core.player.SRPGPlayerUtils;
import kdvn.sky2.rpg.core.potential.SRPGPotential;
import kdvn.sky2.rpg.core.rate.SRPGRate;
import kdvn.sky2.rpg.core.sachmayman.SRPGSachMayMan;
import kdvn.sky2.rpg.core.sachphuphep.SRPGSachPhuPhep;
import kdvn.sky2.rpg.core.senchant.SRPGEnchant;
import kdvn.sky2.rpg.core.stat.SRPGStat;
import kdvn.sky2.rpg.core.thuochoiphuc.SRPGThuocHoiPhuc;
import kdvn.sky2.rpg.core.utils.ChatPlaceholder;
import kdvn.sky2.rpg.core.utils.SRPGUtils;
import mk.plugin.eternalcore.feature.dabaoho.EKeepGem;
import mk.plugin.sky2rpgcore.element.SElement;
import mk.plugin.sky2rpgcore.jewelry.JewelryType;
import mk.plugin.sky2rpgcore.jewelry.util.JewelryUtils;
import mk.plugin.sky2rpgcore.nangbac.GradeGUI;
import mk.plugin.sky2rpgcore.nangbac.GradeStone;
import mk.plugin.sky2rpgcore.weapon.AllWeaponGUI;
import mk.plugin.sky2rpgcore.weapon.Grade;
import mk.plugin.sky2rpgcore.weapon.WeaponType;
import mk.plugin.sky2rpgcore.weapon.WeaponUtils;
import mk.plugn.sky2rpgcore.itemgui.SItemGUI;

public class SRPGAdminCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String arg2, String[] args) {
		
		if (!sender.hasPermission("skyrpg.*")) return false;
		try {
			if (args[0].equalsIgnoreCase("reload")) {
				sender.sendMessage("§aReloaded");
				MainSky2RPGCore.getMain().reloadConfig();
			}
			
			else if (args[0].equalsIgnoreCase("setlevel")) {
				Player player = Bukkit.getPlayer(args[1]);
				int level = Integer.parseInt(args[2]);
				player.setLevel(level);
				sender.sendMessage("§aOk, done");
			}
			
			else if (args[0].equalsIgnoreCase("buffstat")) {
				Player player = Bukkit.getPlayer(args[1]);
				SRPGStatBuff buff = new SRPGStatBuff(SRPGStat.valueOf(args[2]), Integer.parseInt(args[3]), Boolean.parseBoolean(args[4]), System.currentTimeMillis(), Integer.parseInt(args[5]) * 1000);
				SRPGPlayerUtils.buffStat(player, buff);
				sender.sendMessage("§aOk, done");
			}
			
			else if (args[0].equalsIgnoreCase("buffexp")) {
				Player player = Bukkit.getPlayer(args[1]);
				SRPGExpBuff buff = new SRPGExpBuff(Integer.parseInt(args[2]), System.currentTimeMillis(), Integer.parseInt(args[3]) * 1000);
				SRPGPlayerUtils.buffExp(player, buff);
				sender.sendMessage("§aOk, done");
			}
			
			else if (args[0].equalsIgnoreCase("sendmess")) {
				Player target = Bukkit.getPlayer(args[1]);
				String mess = "";
				for (int i = 2 ; i < args.length ; i++) {
					mess += args[i] + " ";
				}
				mess = mess.replace("&", "§");
				target.sendMessage(mess);
				sender.sendMessage("§aOk, done");
			}
			
			else if (args[0].equalsIgnoreCase("renameitem")) {
				Player target = Bukkit.getPlayer(args[1]);
				if (args.length == 3) {
					String name = args[2].replace("_", " ");
					ItemStack item = target.getInventory().getItemInMainHand();
					Bukkit.getScheduler().runTaskAsynchronously(MainSky2RPGCore.getMain(), ()  -> {
						SRPGItem rpgI = SRPGItemUtils.fromItemStack(item);
						if (rpgI == null) return;
						rpgI.setName(name.replace("&", "§") + "§8§l™");
						if (rpgI != null) {
							ItemStack i = SRPGItemUtils.setItem(target, item, rpgI);
							target.getInventory().setItemInMainHand(i);
						}
					});
				}
				else {
					new ChatPlaceholder(target, "§aNhập tên bạn muốn đổi (dấu cách là \"_\", chỉ đổi được item có chỉ số)", "skyrpg renameitem "  + target.getName() + " <s>");
				}
				sender.sendMessage("§aOk, done");
			} 
			
			else if (args[0].equals("levelup")) {
				Player target = Bukkit.getPlayer(args[1]);
				if (args.length > 2) {
					int lv = Integer.parseInt(args[2]);
					if (target.getLevel() < lv) {
						target.setLevel(lv);
					}
				}
				else target.setLevel(target.getLevel() + 1);
				sender.sendMessage("§aOk, done");
			}
			
			else if (args[0].equals("setlevel")) {
				Player target = Bukkit.getPlayer(args[1]);
				int lv = Integer.parseInt(args[2]);
				target.setLevel(lv);
				sender.sendMessage("§aOk, done");
			}
			
			else if (args[0].equals("addexp")) {
				Player target = Bukkit.getPlayer(args[1]);
				SRPGPlayerUtils.addExp(target, Integer.parseInt(args[2]));
				target.sendMessage("§aNhận §f" + Integer.parseInt(args[2]) + " exp");
				sender.sendMessage("§aOk, done");
			}
			
			else if (args[0].equalsIgnoreCase("infogui")) {
				Player player = Bukkit.getPlayer(args[1]);
				SRPGGUIInfo.openGUI(player);
			}
			
			else if (args[0].equalsIgnoreCase("potentialgui")) {
				Player player = Bukkit.getPlayer(args[1]);
				SRPGGUIPotential.openGUI(player);
			}
			
//			else if (args[0].equalsIgnoreCase("classgui")) {
//				Player player = Bukkit.getPlayer(args[1]);
//				if (args.length == 3) {
//					SRPGClass cls = SRPGClass.valueOf(args[2].toUpperCase());
//					SRPGGUIClass.openGUI(player, cls);
//				}
//				else {
//					SRPGPlayer rpgP = SRPGPlayerUtils.getData(player);
//					if (rpgP.hasClass()) {
//						SRPGGUIClass.openGUI(player, rpgP.getSClass());
//					} else {
//						player.sendMessage("§cBạn chưa có class");
//					}
//				}
//			}
			
			else if (args[0].equalsIgnoreCase("seegui")) {
				Player target = Bukkit.getPlayer(args[1]);
				Player viewer = Bukkit.getPlayer(args[2]);
				SRPGGUISee.openGUI(target, viewer);
			}
			
			else if (args[0].equalsIgnoreCase("resetpotential")) {
				Player player = Bukkit.getPlayer(args[1]);
				Map<SRPGPotential, Integer> potentials = new LinkedHashMap<SRPGPotential, Integer>();
				SRPGPlayer rpgPlayer = SRPGPlayerUtils.getData(player);
				rpgPlayer.setPotentials(potentials);
				player.sendMessage("§aĐã reset Tiềm năng!");
			}
			
			else if (args[0].equalsIgnoreCase("cuonghoagui")) {
				Player player = Bukkit.getPlayer(args[1]);
				SRPGGUICuongHoa.openGUI(player);
			}
			
			else if (args[0].equalsIgnoreCase("chuyenhoagui")) {
				Player player = Bukkit.getPlayer(args[1]);
				SRPGGUIChuyenHoa.openGUI(player);
			}
			
			else if (args[0].equalsIgnoreCase("epdagui")) {
				Player player = Bukkit.getPlayer(args[1]);
				SRPGGUIEpDa.openGUI(player);
			}
			
			else if (args[0].equalsIgnoreCase("duclogui")) {
				Player player = Bukkit.getPlayer(args[1]);
				SRPGGUIDucLo.openGUI(player);
			}
			
			else if (args[0].equalsIgnoreCase("tachdagui")) {
				Player player = Bukkit.getPlayer(args[1]);
				SRPGGUITachDa.openGUI(player);
			}
			
			else if (args[0].equalsIgnoreCase("enchantgui")) {
				Player player = Bukkit.getPlayer(args[1]);
				SRPGGUIEnchant.openGUI(player);
			}
			
			else if (args[0].equalsIgnoreCase("tachenchantgui")) {
				Player player = Bukkit.getPlayer(args[1]);
				SRPGGUITachPhuPhep.openGUI(player);
			}
			
			else if (args[0].equalsIgnoreCase("nangbacgui")) {
				Player player = Bukkit.getPlayer(args[1]);
				GradeGUI.openGUI(player);
			}
			
			else if (args[0].equalsIgnoreCase("itemgui")) {
				Player player = Bukkit.getPlayer(args[1]);
				SItemGUI.openGUI(player);
			}
			
//			else if (args[0].equals("setclass")) {
//				SRPGPlayerUtils.changeClass(Bukkit.getPlayer(args[2]), SRPGClass.valueOf(args[1]));
//			}
			
			else if (args[0].equalsIgnoreCase("setelement")) {
				SRPGPlayerUtils.changeElement(Bukkit.getPlayer(args[2]), SElement.valueOf(args[1]));
			}
			
			else if (args[0].equalsIgnoreCase("giverandomgem")) {
				Player player = Bukkit.getPlayer(args[1]);
				SRPGRate rate = SRPGRate.valueOf(args[2].toUpperCase());
				Bukkit.getScheduler().runTaskAsynchronously(MainSky2RPGCore.getMain(), () -> {
					SRPGStat stat = SRPGStat.values()[SRPGUtils.randomInt(0, SRPGStat.values().length - 1)];
					SRPGGem gem = new SRPGGem("§6Ngọc §c" + stat.getName(), stat, rate.getTier().getBonus(), rate);
					ItemStack item = SRPGGemUtils.setGem(new ItemStack(Material.IRON_NUGGET), gem);
					SRPGUtils.giveItem(player, item);
					player.sendMessage("§aNhận được ngọc §c" + stat.getName());
				});
			}
			
			// Command In-game
			if (!(sender instanceof Player)) {
				return false;
			}
			Player player = (Player) sender;
			SRPGPlayer rpgP = SRPGPlayerUtils.getData(player);
			
			if (args[0].equalsIgnoreCase("checkstat")) {
				for (SRPGStat stat : SRPGStat.values()) {
					player.sendMessage("§a" + stat.getName() + ": §f" + rpgP.getStat(player, stat));
				}
			}
			
			else if (args[0].equalsIgnoreCase("checkitem")) {
				SRPGItem rpgI = SRPGItemUtils.fromItemStack(player.getInventory().getItemInMainHand());
				player.sendMessage("§aName: §f" + rpgI.getName());
				player.sendMessage("§aGem hole: §f" + rpgI.getGemHole());
				player.sendMessage("§aLevel: §f" + rpgI.getLevel());
				player.sendMessage("§aGem: §f" + rpgI.getGems());
				player.sendMessage("§aSEnchant: §f" + rpgI.getEnchants());
				player.sendMessage("§aStat: §f" + rpgI.getStats() + "");
			}
			
			else if (args[0].equalsIgnoreCase("setitem")) {
				String name = args[1].replace("_", " ").replace("&", "§");
				int level = Integer.parseInt(args[2]);
				int gemHole = Integer.parseInt(args[3]);
				Map<SRPGStat, Integer> stats = SRPGItemUtils.mapStatFromString(args[4]);
				Map<SRPGEnchant, Integer> enchants = new LinkedHashMap<SRPGEnchant, Integer> ();
				SRPGItem rpgItem = new SRPGItem(name, level, gemHole, stats, null, enchants, null, Grade.I, 0);
				ItemStack itemStack = player.getInventory().getItemInMainHand();
				itemStack = SRPGItemUtils.setItem(player, itemStack, rpgItem);
				player.getInventory().setItemInMainHand(itemStack);
				sender.sendMessage("§aOk, done");
			}
			
			else if (args[0].equalsIgnoreCase("setgem")) {
				SRPGStat stat = SRPGStat.valueOf(args[1]);
				int value =  Integer.parseInt(args[2]);
				SRPGRate rate = SRPGRate.valueOf(args[3]);
				String name = args[4].replace("_", " ").replace("&", "§");
				
				SRPGGem gem = new SRPGGem(name, stat, value, rate);
				
				ItemStack itemStack = player.getInventory().getItemInMainHand();
				itemStack = SRPGGemUtils.setGem(itemStack, gem);
				player.getInventory().setItemInMainHand(itemStack);
				
				sender.sendMessage("§aOk, done");
			}
			
			else if (args[0].equalsIgnoreCase("setitemlevel")) {
				int level = Integer.parseInt(args[1]);
				ItemStack itemStack = player.getInventory().getItemInMainHand();
				itemStack = SRPGItemUtils.setLevel(itemStack, level);
				player.getInventory().setItemInMainHand(itemStack);
				sender.sendMessage("§aOk, done");
			}
			
			else if (args[0].equalsIgnoreCase("setitemname")) {
				String name = args[1];
				ItemStack itemStack = player.getInventory().getItemInMainHand();
				SItem si = SRPGItemUtils.fromItemStack(itemStack);
				si.setName(name);
				itemStack = SRPGItemUtils.setItem(player, itemStack, si);
				player.getInventory().setItemInMainHand(itemStack);
				sender.sendMessage("§aOk, done");
			}
			
			else if (args[0].equalsIgnoreCase("setitemdesc")) {
				String desc = "";
				for (int i = 1 ; i < args.length ; i++) desc += args[i] + " ";
				desc = desc.substring(0, desc.length() - 1);
				ItemStack itemStack = player.getInventory().getItemInMainHand();
				SItem si = SRPGItemUtils.fromItemStack(itemStack);
				si.setDesc(desc);
				itemStack = SRPGItemUtils.setItem(player, itemStack, si);
				player.getInventory().setItemInMainHand(itemStack);
				sender.sendMessage("§aOk, done");
			}
			
			else if (args[0].equalsIgnoreCase("setitemgrade")) {
				Grade grade = Grade.valueOf(args[1].toUpperCase());
				ItemStack itemStack = player.getInventory().getItemInMainHand();
				SItem si = SRPGItemUtils.fromItemStack(itemStack);
				si.setGrade(grade);
				itemStack = SRPGItemUtils.setItem(player, itemStack, si);
				player.getInventory().setItemInMainHand(itemStack);
				sender.sendMessage("§aOk, done");
			}
			
			else if (args[0].equalsIgnoreCase("setitemstats")) {
				Map<SRPGStat, Integer> stats = SRPGItemUtils.mapStatFromString(args[1]);
				ItemStack itemStack = player.getInventory().getItemInMainHand();
				SItem si = SRPGItemUtils.fromItemStack(itemStack);
				si.setStats(stats);;
				itemStack = SRPGItemUtils.setItem(player, itemStack, si);
				player.getInventory().setItemInMainHand(itemStack);
				sender.sendMessage("§aOk, done");
			}
			
			
			else if (args[0].equalsIgnoreCase("setitemgemhole")) {
				int hole = Integer.valueOf(args[1]);
				ItemStack itemStack = player.getInventory().getItemInMainHand();
				SItem si = SRPGItemUtils.fromItemStack(itemStack);
				si.setGemHole(hole);;
				itemStack = SRPGItemUtils.setItem(player, itemStack, si);
				player.getInventory().setItemInMainHand(itemStack);
				sender.sendMessage("§aOk, done");
			}
			
			
			
			else if (args[0].equals("removeclass")) {
				SRPGPlayerUtils.removeClass(player);
				sender.sendMessage("§aOk, done");
			}
			
			else if (args[0].equalsIgnoreCase("getcuonghoa")) {
				for (SRPGDaCuongHoa dch : SRPGDaCuongHoa.values()) {
					SRPGUtils.giveItem(player, dch.getItem());
				}
				for (SRPGSachMayMan smm : SRPGSachMayMan.values()) {
					SRPGUtils.giveItem(player, smm.getItem());
				}
			}
			
			else if (args[0].equalsIgnoreCase("getenchant")) {
				SRPGSachPhuPhep spp = new SRPGSachPhuPhep(SRPGEnchant.valueOf(args[1]));
				SRPGUtils.giveItem(player, spp.getItem());
			}
			
			else if (args[0].equalsIgnoreCase("spawnmob")) {
				int lv = Integer.parseInt(args[1]);
				EntityType type = EntityType.valueOf(args[2]);
				LivingEntity z = (LivingEntity) player.getWorld().spawnEntity(player.getLocation(), type);
				SRPGMobType mobType = SRPGMobType.valueOf(args[3]);
				SRPGMobUtils.setRPGMob(z, new SRPGMob(lv, mobType), z.getName() + " Lv." + lv);
			}
			
			else if (args[0].equalsIgnoreCase("getthuochoiphuc")) {
				SRPGThuocHoiPhuc type = SRPGThuocHoiPhuc.valueOf(args[1]);
				SRPGUtils.giveItem(player, type.createItem());
			}
			
			else if (args[0].equalsIgnoreCase("getweapon")) {
				player.getInventory().addItem(WeaponUtils.getWeapon(player, WeaponType.valueOf(args[1])));
				player.sendMessage("Ok");
			}
			
			else if (args[0].equalsIgnoreCase("getdnb")) {
				player.getInventory().addItem(GradeStone.getItem());
				player.sendMessage("Ok");
			}
			
			else if (args[0].equalsIgnoreCase("getdbh")) {
				player.getInventory().addItem(EKeepGem.getItem());
				player.sendMessage("Ok");
			}
			
			else if (args[0].equalsIgnoreCase("weapongui")) {
				AllWeaponGUI.openGUI(player, 1);
			}
			
			else if (args[0].equalsIgnoreCase("getjewelry")) {
				JewelryType jt = JewelryType.valueOf(args[1].toUpperCase());
				player.getInventory().addItem(JewelryUtils.getJewelry(player, jt));
			}
			
		}
		catch (ArrayIndexOutOfBoundsException e) {
			sendTut(sender);
		}
		
		return false;
	}
	
	public void sendTut(CommandSender sender) {
		sender.sendMessage("§7-- §e§lAll commands §7-------------------------------------");
		sender.sendMessage("§a/skyrpg reload: §6Reload config");
		sender.sendMessage("§a/skyrpg setlevel <player> <level>: §6Set player level");
		sender.sendMessage("§a/skyrpg buffstat <player> <stat> <value> <isPercent> <time(seconds)>: §6Buff stat");
		sender.sendMessage("§a/skyrpg buffexp <player> <value> <time(seconds)>: §6Buff exp");
		sender.sendMessage("§a/skyrpg sendmess <player> <message>: §6Send message");
		sender.sendMessage("§a/skyrpg renameitem <player> <!name>: §6Rename item");
		sender.sendMessage("§a/skyrpg levelup <player> <!toLevel>: §6Level up player");
		sender.sendMessage("§a/skyrpg addexp <player> <exp>: §6Add exp");
		sender.sendMessage("§a/skyrpg infogui <player>: §6Open player gui");
		sender.sendMessage("§a/skyrpg potentialgui <player>: §6Open potential gui");
		sender.sendMessage("§a/skyrpg classgui <player>: §6Open player's class gui");
		sender.sendMessage("§a/skyrpg classgui <player> <class>: §6Open player's class gui");
		sender.sendMessage("§a/skyrpg seegui <target> <viewer>: §6Open see gui of <target> for <viewer>");
		sender.sendMessage("§a/skyrpg cuonghoagui <player>: §6Open cuong hoa gui");
		sender.sendMessage("§a/skyrpg chuyenhoagui <player>: §6Open chuyen hoa gui");
		sender.sendMessage("§a/skyrpg epdagui <player>: §6Open ep da gui");
		sender.sendMessage("§a/skyrpg duclogui <player>: §6Open duc lo gui");
		sender.sendMessage("§a/skyrpg tachdagui <player>: §6Open tach da gui");
		sender.sendMessage("§a/skyrpg enchantgui <player>: §6Open enchant gui");
		sender.sendMessage("§a/skyrpg tachenchantgui <player>: §6Open tach enchant gui");
		sender.sendMessage("§a/skyrpg nangbacgui <player>: §6Open tach enchant gui");
		sender.sendMessage("§a/skyrpg itemgui <player>: §6Open item gui");
		sender.sendMessage("§a/skyrpg setlevel <player> <level>: §6Set player level");
		sender.sendMessage("§a/skyrpg resetpotential <player>: §6Reset potential point");
		sender.sendMessage("§a/skyrpg setsetelement <e> <player>: §6Set class");
		sender.sendMessage("§a/skyrpg randomgem <player> <rate> (THO/THUONG/TRUNG/CAO/CUC_PHAM/HUYEN_THOAI): §6Give random gem");
		sender.sendMessage("");
		sender.sendMessage("§7-- §e§lIngame only §7--------------------------------------");
		sender.sendMessage("§a/skyrpg checkstat: §6Check stat");
		sender.sendMessage("§a/skyrpg checkitem: §6Check item");
		sender.sendMessage("§a/skyrpg setitem <name> <level> <gemHole> <stats>: §6Set item");
		sender.sendMessage("§a/skyrpg setgem <stat> <value> <rate> <name>: §6Set gem");
		sender.sendMessage("§a/skyrpg setitemlevel <level>: §6Set item level");
		sender.sendMessage("§a/skyrpg setitemname <name>: §6Set item name");
		sender.sendMessage("§a/skyrpg setitemdesc <desc>: §6Set item desc");
		sender.sendMessage("§a/skyrpg setitemgrade <desc>: §6Set item tier (I, II, III, IV, V)");
		sender.sendMessage("§a/skyrpg setitemstats <stats>: §6Set item stats");
		sender.sendMessage("§a/skyrpg setitemenchants <enchants>: §6Set item stats");
		sender.sendMessage("§a/skyrpg setitemgemhole <hole>: §6Set item gem hole");
		sender.sendMessage("§a/skyrpg removeclass: §6Remove class");
		sender.sendMessage("§a/skyrpg getcuonghoa: §6Get item cuong hoa");
		sender.sendMessage("§a/skyrpg getenchant <name>: §6Get phu phep");
		sender.sendMessage("§a/skyrpg spawnmob <lv> <entityType> <mobType>: §6Spawn mob");
		sender.sendMessage("§a/skyrpg getthuochoiphuc <type>: §6Get thuoc hoi phuc");
		sender.sendMessage("§a/skyrpg getweapon <type>: §6Get weapon");
		sender.sendMessage("§a/skyrpg getdnb: §6Get grade stone");
		sender.sendMessage("§a/skyrpg getdbh: §6Get Đá bảo hộ");
		sender.sendMessage("§a/skyrpg weapongui: §6WG");
		sender.sendMessage("§a/skyrpg getjewelry <type>: §6Yeu em");
		sender.sendMessage("");
	}
	
	
}
