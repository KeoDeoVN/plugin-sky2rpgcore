package kdvn.sky2.rpg.core.cuonghoa;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import kdvn.sky2.rpg.core.utils.ItemStackUtils;

public enum SRPGDaCuongHoa {
	
	CAP_1("§7§lĐá cường hóa I", 1, Material.CLAY_BALL),
	CAP_2("§9§lĐá cường hóa II", 4, Material.LIGHT_BLUE_DYE),
	CAP_3("§c§lĐá cường hóa III", 6, Material.ROSE_RED),
	CAP_4("§6§lĐá cường hóa IV", 8,  Material.ORANGE_DYE),
	CAP_5("§3§lĐá cường hóa V", 10, Material.CYAN_DYE);
	
	private String name;
	private int level;
	private Material material;
	
	private SRPGDaCuongHoa(String name, int level, Material material) {
		this.name = name;
		this.level = level;
		this.material = material;
	}
	
	public String getName() {
		return this.name;
	}
	
	public int getLevel() {
		return this.level;
	}
	
	public Material getMaterial() {
		return this.material;
	}
	
	public ItemStack getItem() {
		ItemStack item = new ItemStack(this.getMaterial());
		ItemStackUtils.setDisplayName(item, this.getName());
		ItemStackUtils.addEnchantEffect(item);
		item = ItemStackUtils.setTag(item, "sRPG.dacuonghoa", this.name());
		
		return item;
	}
	
	public static boolean isThatItem(ItemStack item) {
		return ItemStackUtils.hasTag(item, "sRPG.dacuonghoa");
	}
	
	public static SRPGDaCuongHoa fromItem(ItemStack item) {
		if (!isThatItem(item)) return null;
		return SRPGDaCuongHoa.valueOf(ItemStackUtils.getTag(item, "sRPG.dacuonghoa"));
	}
	
}
