package kdvn.sky2.rpg.core.cuonghoa;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;
import org.bukkit.inventory.ItemStack;

public class SkyCuongHoaEvent extends PlayerEvent {

	private ItemStack item;
	private int oldLevel;
	private int newLevel;
	
	public SkyCuongHoaEvent(Player player, ItemStack item, int oldLevel, int newLevel) {
		super(player);
		this.item = item;
		this.oldLevel = oldLevel;
		this.newLevel = newLevel;
	}
	
	public ItemStack getItemStack() {
		return this.item;
	}
	
	public int getOldLevel() {
		return this.oldLevel;
	}
	
	public int getNewLevel() {
		return this.newLevel;
	}
	
	private static final HandlerList handlers = new HandlerList();
	
	public static HandlerList getHandlerList() {
		return handlers;
	}
	
	@Override
	public HandlerList getHandlers() {
		return handlers;
	}
	
}
