package kdvn.sky2.rpg.core.damagecheck;

import java.util.HashMap;
import java.util.Map;

public class SRPGDamageCauser {
	
	private Map<String, Double> damage = new HashMap<String, Double> ();
	
	public void addDamage(String player, double damage) {
		double before = 0;
		if (this.damage.containsKey(player)) {
			before = this.damage.get(player);
		}
		this.damage.put(player, before + damage);
	}
	
	public void remove(String player) {
		if (this.damage.containsKey(player)) {
			this.damage.remove(player);
		}
	}
	
	public double get(String player) {
		if (this.damage.containsKey(player)) {
			return this.damage.get(player);
		}
		return 0;
	}
	
	public Map<String, Double> getMap() {
		return this.damage;
	}
	
}
