package kdvn.sky2.rpg.core.gem;

import kdvn.sky2.rpg.core.rate.SRPGRate;
import kdvn.sky2.rpg.core.stat.SRPGStat;

public class SRPGGem {
	
	private String name;
	private SRPGStat stat;
	private int value;
	private SRPGRate rate;
	
	public SRPGGem(String name, SRPGStat stat, int value, SRPGRate rate) {
		this.stat = stat;
		this.value = value;
		this.rate = rate;
		this.name = name;
	}
 	
	public SRPGStat getStat() {
		return this.stat;
	}
	
	public int getValue() {
		return this.value;
	}
	
	public SRPGRate getRate() {
		return this.rate;
	}
	
	public String getName() {
		return this.name;
	}
	
}
