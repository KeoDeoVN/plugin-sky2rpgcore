package kdvn.sky2.rpg.core.gem;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

import kdvn.sky2.rpg.core.item.SRPGItemUtils;
import kdvn.sky2.rpg.core.utils.ItemStackUtils;

public class SRPGGemUtils {
	
	public static ItemStack setGem(ItemStack itemStack, SRPGGem gem) {
		itemStack = updateGem(itemStack, gem);
		itemStack = ItemStackUtils.setTag(itemStack, "sRPGGem", SRPGItemUtils.gemToString(gem));
		return itemStack;
	}
	
	public static SRPGGem fromItemStack(ItemStack itemStack) {
		if (!ItemStackUtils.hasTag(itemStack, "sRPGGem")) return null;
		return SRPGItemUtils.gemFromString(ItemStackUtils.getTag(itemStack, "sRPGGem"));
	}
	
	public static ItemStack updateGem(ItemStack itemStack, SRPGGem gem) {
		List<String> lore = new ArrayList<String> ();
		ItemStackUtils.setDisplayName(itemStack, gem.getName());
		lore.add("§aLoại: §fĐá quý");
		lore.add("§aĐánh giá: §f" + gem.getRate().getTier().getName());
		lore.add("§aChỉ số: §f" + gem.getStat().getName());
		lore.add("§aGiá trị: §f" + gem.getValue() + "%");
		ItemStackUtils.setLore(itemStack, lore);
		ItemStackUtils.addFlag(itemStack, ItemFlag.HIDE_ATTRIBUTES);
		ItemStackUtils.addFlag(itemStack, ItemFlag.HIDE_ENCHANTS);
		ItemStackUtils.addEnchantEffect(itemStack);
		
		return itemStack;
	}
	
}
