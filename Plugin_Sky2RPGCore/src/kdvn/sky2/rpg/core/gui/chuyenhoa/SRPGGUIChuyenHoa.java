package kdvn.sky2.rpg.core.gui.chuyenhoa;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import kdvn.sky2.rpg.core.item.SRPGItem;
import kdvn.sky2.rpg.core.item.SRPGItemUtils;
import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.price.MoneyAPI;
import kdvn.sky2.rpg.core.utils.GUIUtils;
import kdvn.sky2.rpg.core.utils.ItemStackUtils;
import kdvn.sky2.rpg.core.utils.SRPGUtils;
import net.minecraft.server.v1_13_R2.SoundEffects;

public class SRPGGUIChuyenHoa {
	
	
	public static final String TITLE = "§c§lCHUYỂN CẤP CƯỜNG HÓA";
	
	public static final List<Integer> ITEM_SLOTS = Arrays.asList(new Integer[] {11, 15});
	public static final int BUTTON_SLOT = 22;
	
	public static final int FEE = 10000;
	
	public static void openGUI(Player player, ItemStack item) {
		Inventory inv = Bukkit.createInventory(null, 36, TITLE);
		
		Bukkit.getScheduler().runTaskAsynchronously(MainSky2RPGCore.getMain(), () -> {
			SRPGUtils.sendSound(player, SoundEffects.BLOCK_ENDER_CHEST_OPEN, 1, 1);
			for (int i = 0 ; i < inv.getSize() ;  i++) {
				inv.setItem(i, GUIUtils.getBlackSlot());
			}
			for (int i = 12 ; i <= 14 ; i ++) {
				inv.setItem(i, SRPGUtils.getGreenSlot());
			}
			ITEM_SLOTS.forEach(i -> {
				inv.setItem(i, GUIUtils.getItemSlot(new ItemStack(Material.BLUE_STAINED_GLASS_PANE, 1),  "§aÔ chứa §oItem"));
			});
			inv.setItem(BUTTON_SLOT, GUIUtils.getButton(Arrays.asList(new String[] {"§aBấm để thực hiện", "§6Phí: §f" + FEE + "$"})));
			inv.setItem(35, getInfo());
			
			if (item != null) {
				inv.setItem(ITEM_SLOTS.get(0), item);
			}
		});
		
		player.openInventory(inv);
	}
	
	public static void openGUI(Player player) {
		openGUI(player, null);
	}
	
	public static void eventClick(InventoryClickEvent e) {
		if (e.getClickedInventory() == null) return;
		Inventory inv = e.getWhoClicked().getOpenInventory().getTopInventory();
		if (!e.getView().getTitle().equals(TITLE)) return;
		e.setCancelled(true);
		Player player = (Player) e.getWhoClicked();
		int slot = e.getSlot();
		
		if (e.getClickedInventory() == player.getOpenInventory().getTopInventory()) {
			if (slot == BUTTON_SLOT) {
				ItemStack item1 = inv.getItem(ITEM_SLOTS.get(0));
				ItemStack item2 = inv.getItem(ITEM_SLOTS.get(1));
				SRPGItem rpgI1 = SRPGItemUtils.fromItemStack(item1);
				SRPGItem rpgI2 = SRPGItemUtils.fromItemStack(item2);
				if (rpgI1 == null || rpgI2 == null) {
					player.sendMessage("§cKhông thể thực thi, item không đúng!");
					return;
				}
				// Fee
				if (!MoneyAPI.moneyCost(player, FEE)) {
					player.sendMessage("§cBạn cần §f" + FEE + "$ §cđể thực hiện");
					return;
				}
				// Do
				int lv1 = rpgI1.getLevel();
				rpgI1.setLevel(rpgI2.getLevel());
				rpgI2.setLevel(lv1);
				// Give
				SRPGUtils.giveItem(player, SRPGItemUtils.setItem(player, item1, rpgI1));
				SRPGUtils.giveItem(player, SRPGItemUtils.setItem(player, item2, rpgI2));
				// Set
				ITEM_SLOTS.forEach(i -> {
					inv.setItem(i, null);
				});
				// Close
				player.closeInventory();
				player.sendTitle("§a§lTHÀNH CÔNG ^_^", "§7Chuyển cấp 2 vũ khí", 0, 40, 0);
				SRPGUtils.sendSound(player, SoundEffects.ENTITY_FIREWORK_ROCKET_LAUNCH, 1, 1);
			}
		}
		else {
			if (e.getClick() == ClickType.SHIFT_LEFT) {
				SRPGUtils.sendSound(player, SoundEffects.BLOCK_LEVER_CLICK, 1f, 1f);
				Inventory bInv = player.getOpenInventory().getBottomInventory();
				ItemStack clickedItem = bInv.getItem(slot);
				if (SRPGItemUtils.isRPGItem(clickedItem)) {
					if (!GUIUtils.placeItem(clickedItem, slot, inv, bInv, ITEM_SLOTS)) {
						player.sendMessage("§cĐã để Vật phẩm rồi");
						return;
					}
				}
			} else player.sendMessage("§cẤn §fShift + Chuột trái §cđể đặt item");
		}
	}
	
	public static void eventClose(InventoryCloseEvent e) {
		Player player = (Player) e.getPlayer();
		Inventory inv = e.getInventory();
		if (!e.getView().getTitle().equals(TITLE)) return;
		
		if (!GUIUtils.checkCheck(player)) return;
		
		List<ItemStack> items = new ArrayList<ItemStack> ();
		ITEM_SLOTS.forEach(i -> {
			items.add(inv.getItem(i));
		});
		
		items.forEach(item -> {
			if (item == null || ItemStackUtils.hasTag(item, GUIUtils.SLOT_ITEM_TAG)) return;
			SRPGUtils.giveItem(player, item);
		});
	}
	
	private static ItemStack getInfo() {
		ItemStack item = new ItemStack(Material.PAPER);
		ItemStackUtils.setDisplayName(item, "§6§lHƯỚNG DẪN");
		ItemStackUtils.addLoreLine(item, "§f1. Đặt 2 vật phẩm");
		ItemStackUtils.addLoreLine(item, "§f2. Cấp độ 2 vật phẩm sẽ hoán đổi cho nhau");
		
		return item;
	}
	
	
}
