package kdvn.sky2.rpg.core.gui.cuonghoa;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import kdvn.sky2.rpg.core.cuonghoa.SRPGDaCuongHoa;
import kdvn.sky2.rpg.core.cuonghoa.SkyCuongHoaEvent;
import kdvn.sky2.rpg.core.item.SRPGItem;
import kdvn.sky2.rpg.core.item.SRPGItemUtils;
import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.sachmayman.SRPGSachMayMan;
import kdvn.sky2.rpg.core.utils.GUIUtils;
import kdvn.sky2.rpg.core.utils.ItemStackUtils;
import kdvn.sky2.rpg.core.utils.SRPGUtils;
import net.minecraft.server.v1_13_R2.SoundEffects;

public class SRPGGUICuongHoa {
	
	
	public static final int ITEM_SLOT = 11;
	public static final int DCH_SLOT = 13;
	public static final int AMULET_SLOT = 15;
	public static final int RESULT_SLOT = 23;
	public static final int BUTTON_SLOT = 21;
	public static final int INFO_SLOT = 35;
	
	public static String TITLE = "§c§lCƯỜNG HÓA VẬT PHẨM";
	
	public static void openGUI(Player player, ItemStack item) {
		Inventory inv = Bukkit.createInventory(null, 36, TITLE);
		
		Bukkit.getScheduler().runTaskAsynchronously(MainSky2RPGCore.getMain(), () -> {
			SRPGUtils.sendSound(player, SoundEffects.BLOCK_ENDER_CHEST_OPEN, 1, 1);
			for (int i = 0 ; i < inv.getSize() ;  i++) {
				inv.setItem(i, GUIUtils.getBlackSlot());
			}
			inv.setItem(ITEM_SLOT, GUIUtils.getItemSlot(new ItemStack(Material.LIME_STAINED_GLASS_PANE, 1), "§aÔ chứa §oVật phẩm"));
			inv.setItem(DCH_SLOT, GUIUtils.getItemSlot(new ItemStack(Material.BLUE_STAINED_GLASS_PANE, 1), "§aÔ chứa §oĐá cường hóa"));
			inv.setItem(AMULET_SLOT, GUIUtils.getItemSlot(new ItemStack(Material.WHITE_STAINED_GLASS_PANE, 1), "§aÔ chứa §oSách may mắn §r§7(Không bắt buộc)"));
			inv.setItem(RESULT_SLOT, GUIUtils.getItemSlot(new ItemStack(Material.PINK_STAINED_GLASS_PANE, 1), "§aÔ chứa §oSản phẩm"));
			inv.setItem(BUTTON_SLOT, GUIUtils.getButton(Arrays.asList(new String[] {"§aTỉ lệ thành công: §b0%", "§6Click để thực hiện"})));
			inv.setItem(INFO_SLOT, getInfo());
			if (item != null) {
				inv.setItem(ITEM_SLOT, item);
				// Update result
				Bukkit.getScheduler().runTaskAsynchronously(MainSky2RPGCore.getMain(), () -> {
					ItemStack clone = item.clone();
					ItemStackUtils.setDisplayName(clone, ItemStackUtils.getName(clone) + " §e(Sản phẩm)");
					SRPGItem rpgI = SRPGItemUtils.fromItemStack(clone);
					rpgI.setLevel(rpgI.getLevel() + 1);
					clone = SRPGItemUtils.setItem(player, clone, rpgI);
					inv.setItem(RESULT_SLOT, clone);
				});
			}
		});
		
		player.openInventory(inv);
	}
	
	public static void openGUI(Player player) {
		openGUI(player, null);
	}
	
	public static void eventClick(InventoryClickEvent e) {
		if (e.getClickedInventory() == null) return;
		Inventory inv = e.getWhoClicked().getOpenInventory().getTopInventory();
		if (!e.getView().getTitle().equals(TITLE)) return;
		e.setCancelled(true);
		Player player = (Player) e.getWhoClicked();
		int slot = e.getSlot();
		
		// Action on top inv
		if (e.getClickedInventory() == player.getOpenInventory().getTopInventory()) {
			if (slot == BUTTON_SLOT) {
				// Get Item
				ItemStack item = GUIUtils.getItem(inv, ITEM_SLOT);
				if (item == null) {
					player.sendMessage("§cThiếu Vật phẩm!");
					return;
				}
				// Get DCH
				ItemStack dchI = GUIUtils.getItem(inv, DCH_SLOT);
				if (dchI == null) {
					player.sendMessage("§cThiếu Đá cường hóa!");
					return;
				}
				// Get Amulet
				ItemStack amuletI = GUIUtils.getItem(inv, AMULET_SLOT);
				// Object
				SRPGItem rpgI = SRPGItemUtils.fromItemStack(item);
				SRPGDaCuongHoa dch = SRPGDaCuongHoa.fromItem(dchI);
				SRPGSachMayMan amulet = SRPGSachMayMan.fromItem(amuletI);
				double chance = getChance(dch, amulet, rpgI.getLevel() + 1);
				// Do
				if (SRPGUtils.rate(chance)) {
					player.sendTitle("§a§lTHÀNH CÔNG ^_^", "§6§l+" + rpgI.getLevel() + " >> +" + (rpgI.getLevel() + 1), 0, 15, 0);
					SRPGUtils.sendSound(player, SoundEffects.ENTITY_FIREWORK_ROCKET_LAUNCH, 1, 1);
					rpgI.setLevel(rpgI.getLevel() + 1);
					if (rpgI.getLevel() >= 5) {
						SRPGUtils.broadcast("§f[§6Thông báo§f] §aPlayer §f" + player.getName() + " §ađã ép thành công vật phẩm lên +" + rpgI.getLevel() + ", lực chiến lên một tầm cao mới");
					}
					// Call event
					Bukkit.getPluginManager().callEvent(new SkyCuongHoaEvent(player, item, rpgI.getLevel() - 1, rpgI.getLevel()));
				} else {
					String s = "§c§l+" + rpgI.getLevel() + " >> +";
					int down = 0;
					if (amulet == null) {
						down = 1;
						s +=  "" + (rpgI.getLevel() - 1);
					}
					else s += "" + rpgI.getLevel();
					rpgI.setLevel(rpgI.getLevel() - down);
					player.sendTitle("§7§lTHẤT BẠI T_T", s, 0, 15, 0);
					SRPGUtils.sendSound(player, SoundEffects.ENTITY_GHAST_SCREAM, 1, 1);
					
					// Call event
					Bukkit.getPluginManager().callEvent(new SkyCuongHoaEvent(player, item, rpgI.getLevel() + down, rpgI.getLevel()));
				}
				ItemStack i = SRPGItemUtils.setItem(player, item, rpgI);
				SRPGUtils.giveItem(player, i);
				// Close and reopen
				inv.setItem(DCH_SLOT, null);
				inv.setItem(ITEM_SLOT, null);
				inv.setItem(AMULET_SLOT, null);
				player.closeInventory();
				Bukkit.getScheduler().runTaskLater(MainSky2RPGCore.getMain(), () -> {
					openGUI(player);
				}, 10);
			}
		} 
		// Action on bottom inv
		else {
			if (e.getClick() == ClickType.SHIFT_LEFT) {
				SRPGUtils.sendSound(player, SoundEffects.BLOCK_LEVER_CLICK, 1f, 1f);
				Inventory bInv = player.getOpenInventory().getBottomInventory();
				ItemStack clickedItem = bInv.getItem(slot);
				if (SRPGItemUtils.isRPGItem(clickedItem)) {
					if (!GUIUtils.placeItem(clickedItem, slot, inv, bInv, ITEM_SLOT)) {
						player.sendMessage("§cĐã để Vật phẩm rồi");
						return;
					}
					// Update result
					Bukkit.getScheduler().runTaskAsynchronously(MainSky2RPGCore.getMain(), () -> {
						ItemStack clone = clickedItem.clone();
						ItemStackUtils.setDisplayName(clone, ItemStackUtils.getName(clone) + " §e(Sản phẩm)");
						SRPGItem rpgI = SRPGItemUtils.fromItemStack(clone);
						rpgI.setLevel(rpgI.getLevel() + 1);
						clone = SRPGItemUtils.setItem(player, clone, rpgI);
						inv.setItem(RESULT_SLOT, clone);
					});
					return;
				}
				else if (SRPGDaCuongHoa.fromItem(clickedItem) != null) {
					if (!GUIUtils.placeItem(clickedItem, slot, inv, bInv, DCH_SLOT)) {
						player.sendMessage("§cĐã để Đá cường hoá rồi");
						return;
					}
				}
				else if (SRPGSachMayMan.fromItem(clickedItem) != null) {
					if (!GUIUtils.placeItem(clickedItem, slot, inv, bInv, AMULET_SLOT)) {
						player.sendMessage("§cĐã để Sách may mắn rồi");
						return;
					}
				}
				// Update chance
				ItemStack item = GUIUtils.getItem(inv, ITEM_SLOT);
				ItemStack dchI = GUIUtils.getItem(inv, DCH_SLOT);
				ItemStack amuletI = GUIUtils.getItem(inv, AMULET_SLOT);
				if (item != null && dchI != null) {
					SRPGItem rpgI = SRPGItemUtils.fromItemStack(item);
					SRPGDaCuongHoa dch = SRPGDaCuongHoa.fromItem(dchI);
					SRPGSachMayMan amulet = SRPGSachMayMan.fromItem(amuletI);
					double chance = SRPGUtils.round(getChance(dch, amulet, rpgI.getLevel() + 1));
					inv.setItem(BUTTON_SLOT, GUIUtils.getButton(Arrays.asList(new String[] {"§aTỉ lệ thành công: §b" + chance + "%", "§6Click để thực hiện"})));
				}
				
			} else player.sendMessage("§cẤn §fShift + Chuột trái §cđể đặt item");
		}
	}
	
	public static void eventClose(InventoryCloseEvent e) {
		Player player = (Player) e.getPlayer();
		Inventory inv = e.getInventory();
		if (!e.getView().getTitle().equals(TITLE)) return;
		
		if (!GUIUtils.checkCheck(player)) return;
		
		List<ItemStack> items = new ArrayList<ItemStack> ();
		items.add(inv.getItem(AMULET_SLOT));
		items.add(inv.getItem(DCH_SLOT));
		items.add(inv.getItem(ITEM_SLOT));
		
		items.forEach(item -> {
			if (item == null || ItemStackUtils.hasTag(item, GUIUtils.SLOT_ITEM_TAG)) return;
			SRPGUtils.giveItem(player, item);
		});
	}
	
	private static ItemStack getInfo() {
		ItemStack item = new ItemStack(Material.PAPER);
		ItemStackUtils.setDisplayName(item, "§6§lHƯỚNG DẪN");
		ItemStackUtils.addLoreLine(item, "§f1. Cường hóa thất bại bị giảm cấp");
		ItemStackUtils.addLoreLine(item, "§f2. §oSách an toàn §r§fvà §oSách may mắn §r§fgiúp không bị giảm");
		ItemStackUtils.addLoreLine(item, "§f3. §oSách may mắn §r§fcòn giúp tăng tỉ lệ thành công");
		
		return item;
	}
	
	private static double getChance(SRPGDaCuongHoa dch, SRPGSachMayMan sach, int nextLv) {
		// Giới hạn cấp độ đá
		if (nextLv >= 5 && dch.getLevel() <= 2) return 0;
		if (nextLv >= 7 && dch.getLevel() <= 3) return 0;
		if (nextLv >= 10 && dch.getLevel() <= 4) return 0;
		int bonus = 0;
		if (sach != null) bonus = sach.getBonus(); 
		return Math.min(getBaseChance(nextLv) * dch.getLevel() * ((double) (100 + bonus) / 100), 100);
	}
	
	private static double getBaseChance(int nextLv) {
		if (nextLv <= 0) return 100;
		switch (nextLv) {
		case 1: return 100;
		case 2: return 12.5;
		case 3: return 8.5;
		case 4: return 6.5;
		case 5: return 5;
		case 6: return 4;
		case 7: return 3;
		case 8: return 2;
		case 9: return 1;
		case 10: return 0.8;
		case 11: return 0.5;
		case 12: return 0.1;
		case 13: return 0.05;
		case 14: return 0.025;
		case 15: return 0.01;
		}
		return 0;
	}
	
}
