package kdvn.sky2.rpg.core.gui.duclo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import kdvn.sky2.rpg.core.item.SRPGItem;
import kdvn.sky2.rpg.core.item.SRPGItemUtils;
import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.price.PointAPI;
import kdvn.sky2.rpg.core.utils.GUIUtils;
import kdvn.sky2.rpg.core.utils.ItemStackUtils;
import kdvn.sky2.rpg.core.utils.SRPGUtils;
import net.minecraft.server.v1_13_R2.SoundEffects;

public class SRPGGUIDucLo {
	
	public static final String TITLE = "§c§lĐỤC LỖ NGỌC";
	
	private static final int ITEM_SLOT = 1;
	private static final int BUTTON_SLOT = 7;
	private static final int POINT = 45;
	
	private static final int MAX_HOLES = 5;
	
	public static void openGUI(Player player, ItemStack item) {
		Inventory inv = Bukkit.createInventory(null, 9, TITLE);
		player.openInventory(inv);
		
		Bukkit.getScheduler().runTask(MainSky2RPGCore.getMain(), () -> {
			SRPGUtils.sendSound(player, SoundEffects.BLOCK_ENDER_CHEST_OPEN, 1, 1);
			for (int i = 0 ; i < inv.getSize() ; i++) {
				inv.setItem(i, SRPGUtils.getBlackSlot());
			}
			inv.setItem(BUTTON_SLOT, GUIUtils.getButton(Arrays.asList(new String[] {"§aBấm để thực hiện", "§6Giá: §e" + POINT + "P"})));
			inv.setItem(ITEM_SLOT, GUIUtils.getItemSlot(new ItemStack(Material.GREEN_STAINED_GLASS_PANE, 1), "§aÔ chứa §oVật phẩm"));
			if (item != null) {
				inv.setItem(ITEM_SLOT, item);
			}
		});
	}
	
	public static void openGUI(Player player) {
		openGUI(player, null);
	}
	
	public static void eventClick(InventoryClickEvent e) {
		if (e.getClickedInventory() == null) return;
		Inventory inv = e.getWhoClicked().getOpenInventory().getTopInventory();
		if (!e.getView().getTitle().equals(TITLE)) return;
		e.setCancelled(true);
		Player player = (Player) e.getWhoClicked();
		int slot = e.getSlot();
		
		// Action on top inv
		if (e.getClickedInventory() == player.getOpenInventory().getTopInventory()) {
			if (slot == BUTTON_SLOT) {
				ItemStack item = GUIUtils.getItem(inv, ITEM_SLOT);
				SRPGItem rpgI = SRPGItemUtils.fromItemStack(item);
				if (rpgI == null) {
					player.sendMessage("§cVật phẩm không đúng");
					return;
				}
				if (rpgI.getGemHole() >= MAX_HOLES) {
					player.sendMessage("§cSố lỗ tối đa");
					return;
				}
				if (!PointAPI.pointCost(player, POINT)) {
					player.sendMessage("§cTài sản của bạn không đủ để chi trả");
					return;
				}
				rpgI.setGemHole(rpgI.getGemHole() + 1);
				item = SRPGItemUtils.setItem(player, item, rpgI);
				SRPGUtils.giveItem(player, item);
				player.sendTitle("§a§lTHÀNH CÔNG ^_^", "Đục lỗ thành công", 0, 15, 0);
				
				SRPGUtils.sendSound(player, SoundEffects.ENTITY_FIREWORK_ROCKET_LAUNCH, 0.7f, 1f);
				
				// Done
				inv.setItem(ITEM_SLOT, null);
				player.closeInventory();
				Bukkit.getScheduler().runTaskLater(MainSky2RPGCore.getMain(), () -> {
					openGUI(player);
				}, 10);
			}
		} else {
			if (e.getClick() == ClickType.SHIFT_LEFT) {
				SRPGUtils.sendSound(player, SoundEffects.BLOCK_LEVER_CLICK, 1f, 1f);
				Inventory bInv = player.getOpenInventory().getBottomInventory();
				ItemStack clickedItem = bInv.getItem(slot);
				
				// Place 
				if (SRPGItemUtils.isRPGItem(clickedItem)) {
					if (!GUIUtils.placeItem(clickedItem, slot, inv, bInv, ITEM_SLOT)) {
						player.sendMessage("§cĐã để Vật phẩm rồi");
						return;
					}
				}
			} else player.sendMessage("§cẤn §fShift + Chuột trái §cđể đặt item");
		}
	}
	
	public static void eventClose(InventoryCloseEvent e) {
		Player player = (Player) e.getPlayer();
		Inventory inv = e.getInventory();
		if (!e.getView().getTitle().equals(TITLE)) return;
		
		if (!GUIUtils.checkCheck(player)) return;
		
		List<ItemStack> items = new ArrayList<ItemStack> ();
		items.add(inv.getItem(ITEM_SLOT));
		
		items.forEach(item -> {
			if (item == null || ItemStackUtils.hasTag(item, GUIUtils.SLOT_ITEM_TAG)) return;
			SRPGUtils.giveItem(player, item);
		});
	}
	
}
