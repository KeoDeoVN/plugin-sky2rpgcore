package kdvn.sky2.rpg.core.gui.enchant;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import kdvn.sky2.rpg.core.item.SRPGItem;
import kdvn.sky2.rpg.core.item.SRPGItemUtils;
import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.sachmayman.SRPGSachMayMan;
import kdvn.sky2.rpg.core.sachphuphep.SRPGSachPhuPhep;
import kdvn.sky2.rpg.core.utils.GUIUtils;
import kdvn.sky2.rpg.core.utils.ItemStackUtils;
import kdvn.sky2.rpg.core.utils.SRPGUtils;
import net.minecraft.server.v1_13_R2.SoundEffects;

public class SRPGGUIEnchant {
	
	public static final int ITEM_SLOT = 10;
	public static final int SACH_ENCHANT_SLOT = 11;
	public static final int BUTTON_SLOT = 23;
	public static final int RESULT_SLOT = 16;
	public static final int SACH_SLOT = 12;
	public static final int TUT_SLOT = 35; 
	
	public static final int MAX_LEVEL = 5;
	
	public static final String TITLE = "§c§lPHÙ PHÉP ĐẶC BIỆT";
	
	public static void openGUI(Player player, ItemStack item) {
		Inventory inv = Bukkit.createInventory(null, 36, TITLE);
		
		Bukkit.getScheduler().runTaskAsynchronously(MainSky2RPGCore.getMain(), () -> {
			SRPGUtils.sendSound(player, SoundEffects.BLOCK_ENDER_CHEST_OPEN, 1, 1);
			for (int i = 0 ; i < inv.getSize() ;  i++) {
				inv.setItem(i, GUIUtils.getBlackSlot());
			}
			inv.setItem(ITEM_SLOT, GUIUtils.getItemSlot(new ItemStack(Material.GREEN_STAINED_GLASS_PANE, 1), "§aÔ chứa §oVật phẩm"));
			inv.setItem(SACH_ENCHANT_SLOT, GUIUtils.getItemSlot(new ItemStack(Material.BLUE_STAINED_GLASS_PANE, 1), "§aÔ chứa §oSách phù phép đặc biệt"));
			inv.setItem(SACH_SLOT, GUIUtils.getItemSlot(new ItemStack(Material.WHITE_STAINED_GLASS_PANE, 1), "§aÔ chứa §oSách may mắn §r§7(Không bắt buộc)"));
			inv.setItem(RESULT_SLOT, GUIUtils.getItemSlot(new ItemStack(Material.PINK_STAINED_GLASS_PANE, 1), "§aÔ chứa §oSản phẩm"));
			inv.setItem(BUTTON_SLOT, GUIUtils.getButton(Arrays.asList(new String[] {"§aTỉ lệ thành công: §b0%", "§6Click để thực hiện"})));
			inv.setItem(TUT_SLOT, getInfo());
			if (item != null) inv.setItem(ITEM_SLOT, item);
			
		});
		player.openInventory(inv);
	}
	
	public static void openGUI(Player player) {
		openGUI(player, null);
	}
	
	public static void eventClick(InventoryClickEvent e) {
		if (e.getClickedInventory() == null) return;
		Inventory inv = e.getWhoClicked().getOpenInventory().getTopInventory();
		if (!e.getView().getTitle().equals(TITLE)) return;
		e.setCancelled(true);
		Player player = (Player) e.getWhoClicked();
		int slot = e.getSlot();
		
		// Action on top inv
		if (e.getClickedInventory() == player.getOpenInventory().getTopInventory()) {
			if (slot == BUTTON_SLOT) {
				ItemStack sachEItem = GUIUtils.getItem(inv, SACH_ENCHANT_SLOT);
				ItemStack itemItem = GUIUtils.getItem(inv, ITEM_SLOT);
				ItemStack sachItem = GUIUtils.getItem(inv, SACH_SLOT);
				
				// Check
				if (!SRPGItemUtils.isRPGItem(itemItem)) {
					player.sendMessage("§cChưa đặt Vật phẩm");
					return;
				}
				if (!SRPGSachPhuPhep.isThatItem(sachEItem)) {
					player.sendMessage("§cChưa đặt Sách phù phép đặc biệt");
					return;
				}
				
				// Do
				SRPGItem rpgI = SRPGItemUtils.fromItemStack(itemItem);
				SRPGSachPhuPhep spp = SRPGSachPhuPhep.fromItem(sachEItem);
				SRPGSachMayMan sach = SRPGSachMayMan.fromItem(sachItem);
				// Chance
				int chance = 50;
				if (sach != null) chance = Math.min(chance * (100 + sach.getBonus()) / 100, 100);
				if (rpgI.getEnchant(spp.getEnchant()) >= 5) chance = 0;
				
				// Do
				if (SRPGUtils.rate(chance)) {
					rpgI.addEnchant(spp.getEnchant());
					player.sendTitle("§a§lTHÀNH CÔNG ^_^", "Phù phép thành công", 0, 15, 0);
					SRPGUtils.sendSound(player, SoundEffects.ENTITY_FIREWORK_ROCKET_LAUNCH, 0.7f, 1f);
				} else {
					player.sendTitle("§7§lTHẤT BẠI T_T", "Chúc bạn may mắn lần sau", 0, 15, 0);
					SRPGUtils.sendSound(player, SoundEffects.ENTITY_GHAST_SCREAM, 0.7f, 1f);
				}
				
				// Give
				itemItem = SRPGItemUtils.setItem(player, itemItem, rpgI);
				SRPGUtils.giveItem(player, itemItem);
				
				// Clear
				inv.setItem(SACH_ENCHANT_SLOT, null);
				inv.setItem(ITEM_SLOT, null);
				inv.setItem(SACH_SLOT, null);
				player.closeInventory();
				Bukkit.getScheduler().runTaskLater(MainSky2RPGCore.getMain(), () -> {
					openGUI(player);
				}, 10);
				
			}
		} else {
			if (e.getClick() == ClickType.SHIFT_LEFT) {
				SRPGUtils.sendSound(player, SoundEffects.BLOCK_LEVER_CLICK, 1f, 1f);
				Inventory bInv = player.getOpenInventory().getBottomInventory();
				ItemStack clickedItem = bInv.getItem(slot);
				
				// Place 
				if (SRPGItemUtils.isRPGItem(clickedItem)) {
					if (!GUIUtils.placeItem(clickedItem, slot, inv, bInv, ITEM_SLOT)) {
						player.sendMessage("§cĐã để Vật phẩm rồi");
						return;
					}
				}
				if (SRPGSachPhuPhep.fromItem(clickedItem) != null) {
					if (!GUIUtils.placeItem(clickedItem, slot, inv, bInv, SACH_ENCHANT_SLOT)) {
						player.sendMessage("§cĐã để Sách phù phép đặc biệt rồi");
						return;
					}
				}
				if (SRPGSachMayMan.isThatItem(clickedItem)) {
					if (!GUIUtils.placeItem(clickedItem, slot, inv, bInv, SACH_SLOT)) {
						player.sendMessage("§cĐã để Vật phẩm rồi");
						return;
					}
				}
				
				// Chance
				ItemStack sachItem = GUIUtils.getItem(inv, SACH_SLOT);
				SRPGSachMayMan sach = SRPGSachMayMan.fromItem(sachItem);
				int chance = 50;
				if (sach != null) chance = chance * (100 + sach.getBonus()) / 100;
				
				// Update button
				SRPGSachPhuPhep spp = SRPGSachPhuPhep.fromItem(GUIUtils.getItem(inv, SACH_ENCHANT_SLOT));
				if (spp != null) {
					inv.setItem(BUTTON_SLOT, GUIUtils.getButton(Arrays.asList(new String[] {"§aTỉ lệ thành công: §b" + chance + "%", "§6Click để thực hiện"})));
				}

				// Update result
				ItemStack item = GUIUtils.getItem(inv, ITEM_SLOT);
				if (item != null && spp != null) {
					ItemStack clone = item.clone();
					ItemStackUtils.setDisplayName(clone, ItemStackUtils.getName(clone) + " §e(Sản phẩm)");
					SRPGItem rpgI = SRPGItemUtils.fromItemStack(clone);
					if (rpgI.getBlankGemHole() > 0) {
						rpgI.addEnchant(spp.getEnchant());
					}
					inv.setItem(RESULT_SLOT, SRPGItemUtils.setItem(player, clone, rpgI));
				}
			} else player.sendMessage("§cẤn §fShift + Chuột trái §cđể đặt item");
		}
	}
	
	public static void eventClose(InventoryCloseEvent e) {
		Player player = (Player) e.getPlayer();
		Inventory inv = e.getInventory();
		if (!e.getView().getTitle().equals(TITLE)) return;
		
		if (!GUIUtils.checkCheck(player)) return;
		
		List<ItemStack> items = new ArrayList<ItemStack> ();
		items.add(inv.getItem(SACH_ENCHANT_SLOT));
		items.add(inv.getItem(ITEM_SLOT));
		items.add(inv.getItem(SACH_SLOT));
		
		items.forEach(item -> {
			if (item == null || ItemStackUtils.hasTag(item, GUIUtils.SLOT_ITEM_TAG)) return;
			SRPGUtils.giveItem(player, item);
		});
		
	}
	
	private static ItemStack getInfo() {
		ItemStack item = new ItemStack(Material.PAPER);
		ItemStackUtils.setDisplayName(item, "§6§lHƯỚNG DẪN");
		ItemStackUtils.addLoreLine(item, "§f1. §oSách an toàn §r§fkhông có tác dụng khi Phù phép");
		ItemStackUtils.addLoreLine(item, "§f2. §oSách may mắn §r§fgiúp tăng tỉ lệ thành công");
		ItemStackUtils.addLoreLine(item, "§f3. Khi ép sách vào enchant đã có thì sẽ tăng cấp");
		ItemStackUtils.addLoreLine(item, "§f4. Ví dụ: Đồ có Giáp lửa I, ép Sách Giáp Lửa I thì đồ sẽ có Giáp lửa II");
		ItemStackUtils.addLoreLine(item, "§f5. Cấp độ enchant tối đa là " + MAX_LEVEL);
		
		return item;
	}
	
}
