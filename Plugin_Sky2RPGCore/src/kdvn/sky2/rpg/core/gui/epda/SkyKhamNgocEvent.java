package kdvn.sky2.rpg.core.gui.epda;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

public class SkyKhamNgocEvent extends PlayerEvent  {
	
	public SkyKhamNgocEvent(Player who) {
		super(who);
	}

	private static final HandlerList handlers = new HandlerList();
	
	public static HandlerList getHandlerList() {
		return handlers;
	}
	
	@Override
	public HandlerList getHandlers() {
		return handlers;
	}
	
}
