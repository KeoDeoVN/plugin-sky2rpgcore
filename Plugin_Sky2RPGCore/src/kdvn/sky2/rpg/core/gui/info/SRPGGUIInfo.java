package kdvn.sky2.rpg.core.gui.info;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.permbuff.SPermBuff;
import kdvn.sky2.rpg.core.player.SRPGPlayer;
import kdvn.sky2.rpg.core.player.SRPGPlayerUtils;
import kdvn.sky2.rpg.core.potential.SRPGPotential;
import kdvn.sky2.rpg.core.power.SRPGPowerUtils;
import kdvn.sky2.rpg.core.stat.SRPGStat;
import kdvn.sky2.rpg.core.utils.ItemStackUtils;
import kdvn.sky2.rpg.core.utils.SRPGUtils;

public class SRPGGUIInfo {
	
	public static final String TITLE = "§2§lTHÔNG TIN NGƯỜI CHƠI";
	
	public static void openGUI(Player player) {
		Inventory inv = Bukkit.createInventory(null, 9, TITLE);
		player.openInventory(inv);
		
		// Load item
		Bukkit.getScheduler().runTaskAsynchronously(MainSky2RPGCore.getMain(), () -> {
			for (int i = 0 ; i < inv.getSize() ; i++) {
				inv.setItem(i, SRPGUtils.getBlackSlot());
			}
			inv.setItem(2, getStatInfo(player));
			inv.setItem(4, getExpInfo(player));
			inv.setItem(6, getBuffInfo(player));
		});
	}
	
	public static ItemStack getStatInfo(Player player) {
		ItemStack item = new ItemStack(Material.PAPER, 1);
		ItemStackUtils.setDisplayName(item, "§6§lCHỈ SỐ");
		SRPGPlayer rpgP = SRPGPlayerUtils.getData(player);
		List<String> lore = new ArrayList<String> ();
		lore.add("§7------------------");
		if (rpgP.hasElement()) {
			lore.add("§3Hệ: §f" + rpgP.getElement().getColor() + rpgP.getElement().getName());
		}
		lore.add("§3Điểm tiềm năng: §f" + SRPGPlayerUtils.getRemainPotentialPoint(player));
		lore.add("§3Lực chiến: §f" + SRPGPowerUtils.calculatePower(player));
		lore.add("§7------------------");
		for (int i = 0 ; i < SRPGPotential.values().length ; i++) {
			SRPGPotential po = SRPGPotential.values()[i];
			lore.add("§c" + po.getName() + ": §f" + rpgP.getPotential(po));
		}
		lore.add("§7------------------");
		for (SRPGStat stat : SRPGStat.values()) {
			lore.add("§a" + stat.getName() + ": §f" + rpgP.getStat(player, stat) + " §7(" + SRPGUtils.round(SRPGPlayerUtils.getStatValue(player, stat)) + ")");
		}
		ItemStackUtils.setLore(item, lore);
		
		return item;
	}
	
	public static ItemStack getExpInfo(Player player) {
		ItemStack item = new ItemStack(Material.EXPERIENCE_BOTTLE, 1);
		ItemStackUtils.addFlag(item, ItemFlag.HIDE_ATTRIBUTES);
		ItemStackUtils.setDisplayName(item, "§6§lKINH NGHIỆM");
		List<String> lore = new ArrayList<String> ();
		lore.add("§7------------------");
		lore.add("§aTỉ lệ: §f" + SRPGUtils.round(player.getExp() * 100) + "%");
		int maxExp = SRPGPlayerUtils.getExpToLevel(player.getLevel() + 1);
		lore.add("§aKinh nghiệm: §f" + new Double(maxExp * player.getExp()).intValue() + "/" + maxExp);
		ItemStackUtils.setLore(item, lore);
		
		return item;
	}
	
	public static ItemStack getBuffInfo(Player player) {
		ItemStack item = new ItemStack(Material.DRAGON_BREATH);
		ItemStackUtils.addFlag(item, ItemFlag.HIDE_ATTRIBUTES);
		ItemStackUtils.setDisplayName(item, "§6§lBUFF");
		SRPGPlayer rpgP = SRPGPlayerUtils.getData(player);
		List<String> lore = new ArrayList<String> ();
		lore.add("§7------------------");
		rpgP.getStatBuffs().forEach(buff -> {
			String s = buff.isPercentBuff() ? "%" : "";
			lore.add("§a+" + buff.getValue() + s + " " + buff.getStat().getName() + " §7(" + buff.getSecondsRemain() + "s)");
		});
		rpgP.getExpBuffs().forEach(buff -> {
			lore.add("§a+" + buff.getValue() + "% Exp" + " §7(" + buff.getSecondsRemain() + "s)");
		});
		
		// Permbuff
		SPermBuff.permBuffs.values().forEach(buff -> {
			if (!player.hasPermission(buff.getPermission())) return;
			buff.getBuffs().forEach((stat, value) -> {
				lore.add("§6+" + value + "% " + stat.getName());
			});
		});;
		
		ItemStackUtils.setLore(item, lore);
		
		return item;
	}
	
	public static void eventClick(InventoryClickEvent e) {
		if (!e.getView().getTitle().contains(TITLE)) return;
		e.setCancelled(true);
	}
	
}
