package kdvn.sky2.rpg.core.gui.potential;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

import kdvn.sky2.rpg.core.gui.info.SRPGGUIInfo;
import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.player.SRPGPlayer;
import kdvn.sky2.rpg.core.player.SRPGPlayerUtils;
import kdvn.sky2.rpg.core.potential.SRPGPotential;
import kdvn.sky2.rpg.core.utils.ItemStackUtils;
import kdvn.sky2.rpg.core.utils.SRPGUtils;

public class SRPGGUIPotential {
	
	public static final String TITLE = "§2§lTIỀM NĂNG";
	
	public static void openGUI(Player player) {
		Inventory inv = Bukkit.createInventory(null, 9, TITLE);
		player.openInventory(inv);
		
		// Load item
		Bukkit.getScheduler().runTaskAsynchronously(MainSky2RPGCore.getMain(), () -> {
			for (int i = 0 ; i < inv.getSize() ; i++) {
				inv.setItem(i, SRPGUtils.getBlackSlot());
			}
			getSlots().forEach((i, p) -> {
				inv.setItem(i, getItem(p));
			});
			inv.setItem(8, SRPGGUIInfo.getStatInfo(player));
		});
	}
	
	public static Map<Integer, SRPGPotential> getSlots() {
		Map<Integer, SRPGPotential> map = new HashMap<Integer, SRPGPotential> ();
		map.put(3, SRPGPotential.SUC_MANH);
		map.put(4, SRPGPotential.THE_LUC);
		map.put(5, SRPGPotential.NHANH_NHEN);
		
		return map;
	}
	
	public static Map<SRPGPotential, Material> getIcons() {
		Map<SRPGPotential, Material> map = new HashMap<SRPGPotential, Material> ();
		map.put(SRPGPotential.SUC_MANH, Material.IRON_SWORD);
		map.put(SRPGPotential.THE_LUC, Material.IRON_CHESTPLATE);
		map.put(SRPGPotential.NHANH_NHEN, Material.IRON_BOOTS);
		
		return map;
	}
	
	public static ItemStack getItem(SRPGPotential p) {
		Material material = getIcons().get(p);
		ItemStack item = new ItemStack(material, 1);
		ItemStackUtils.addFlag(item, ItemFlag.HIDE_ATTRIBUTES);
		ItemStackUtils.setDisplayName(item, "§6§l+1 " + p.getName().toUpperCase());
		ItemStackUtils.addLoreLine(item, "§7--------------");
		p.getStats().forEach((stat, value) -> {
			ItemStackUtils.addLoreLine(item, "§e" + stat.getName() + " §f+" + value);
		});
		
		return item;
	}
	
	public static void eventClick(InventoryClickEvent e) {
		if (!e.getView().getTitle().contains(TITLE)) return;
		e.setCancelled(true);

		Inventory inv = e.getInventory();
		Player player = (Player) e.getWhoClicked();
		SRPGPlayer rpgP = SRPGPlayerUtils.getData(player);
		Map<Integer, SRPGPotential> slots = getSlots();
		int slot = e.getSlot();
		if (slots.containsKey(slot)) {
			int remain = SRPGPlayerUtils.getRemainPotentialPoint(player);
			if (remain <= 0) {
				player.sendMessage("§cKhông còn điểm dư");
				return;
			}
			SRPGPotential po = slots.get(slot);
			int point = rpgP.getPotential(po);
			point++;
			rpgP.setPotential(po, point);
			
			// Update stat
			Bukkit.getScheduler().runTaskAsynchronously(MainSky2RPGCore.getMain(), () -> {
				SRPGPlayerUtils.updatePlayer(player);
				inv.setItem(8, SRPGGUIInfo.getStatInfo(player));
			});
		}
	}
	
}
