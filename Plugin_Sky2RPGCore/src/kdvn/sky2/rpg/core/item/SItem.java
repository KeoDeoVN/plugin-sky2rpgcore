package kdvn.sky2.rpg.core.item;

import java.util.List;
import java.util.Map;

import kdvn.sky2.rpg.core.gem.SRPGGem;
import kdvn.sky2.rpg.core.senchant.SRPGEnchant;
import kdvn.sky2.rpg.core.stat.SRPGStat;
import mk.plugin.sky2rpgcore.weapon.Grade;

public class SItem extends SRPGItem {

	public SItem(String name, int level, int gemHole, Map<SRPGStat, Integer> stats, List<SRPGGem> gems, Map<SRPGEnchant, Integer> enchants, String desc, Grade tier, int gradeExp) {
		super(name, level, gemHole, stats, gems, enchants, desc, tier, gradeExp);
	}

}
