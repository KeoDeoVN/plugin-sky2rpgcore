package kdvn.sky2.rpg.core.item;

import java.util.List;
import java.util.Map;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import kdvn.sky2.rpg.core.gem.SRPGGem;
import kdvn.sky2.rpg.core.senchant.SRPGEnchant;
import kdvn.sky2.rpg.core.stat.SRPGStat;

public class SItemUtils {
	
	public static final int MAX_ITEM_LEVEL = SRPGItemUtils.MAX_ITEM_LEVEL;
	
	public static boolean isRPGItem(ItemStack item) {
		return SRPGItemUtils.isRPGItem(item);
	}
	
	public static void updateItem(Player player, ItemStack itemStack, SRPGItem rpgItem) {
		SRPGItemUtils.updateItem(player, itemStack, rpgItem);
	}
	
	public static ItemStack setItem(Player player, ItemStack itemStack, SRPGItem rpgItem) {
		return SRPGItemUtils.setItem(player, itemStack, rpgItem);
	}
	
	public static SItem fromItemStack(ItemStack itemStackk) {
		return SRPGItemUtils.fromItemStack(itemStackk);
	}

	public static ItemStack setLevel(ItemStack itemStack, int level) {
		return SRPGItemUtils.setLevel(itemStack, level);
	}
	
	public static ItemStack addLevel(ItemStack itemStack, int level) {
		return SRPGItemUtils.addLevel(itemStack, level);
	}
	
	public static String mapStatToString(Map<SRPGStat, Integer> map) {
		return SRPGItemUtils.mapStatToString(map);
	}
	
	public static Map<SRPGStat, Integer> mapStatFromString(String s) {
		return SRPGItemUtils.mapStatFromString(s);
	}
	
	public static String mapSEnchantToString(Map<SRPGEnchant, Integer> map) {
		return SRPGItemUtils.mapSEnchantToString(map);
	}
	
	public static Map<SRPGEnchant, Integer> mapSEnchantFromString(String s) {
		return SRPGItemUtils.mapSEnchantFromString(s);
	}
	
	public static String gemToString(SRPGGem gem) {
		return SRPGItemUtils.gemToString(gem);
	}
	 
	public static String listGemToString(List<SRPGGem> list) {
		return SRPGItemUtils.listGemToString(list);
	}
	
	public static SRPGGem gemFromString(String s) {
		return SRPGItemUtils.gemFromString(s);
	}
	
	public static List<SRPGGem> listGemFromString(String s) {
		return SRPGItemUtils.listGemFromString(s);
	}

}
