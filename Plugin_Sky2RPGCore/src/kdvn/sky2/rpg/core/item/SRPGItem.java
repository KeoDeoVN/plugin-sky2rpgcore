package kdvn.sky2.rpg.core.item;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import kdvn.sky2.rpg.core.buff.SRPGStatBuff;
import kdvn.sky2.rpg.core.gem.SRPGGem;
import kdvn.sky2.rpg.core.senchant.SRPGEnchant;
import kdvn.sky2.rpg.core.stat.SRPGStat;
import mk.plugin.eternalcore.feature.set.ESet;
import mk.plugin.eternalcore.feature.set.ESetUtils;
import mk.plugin.sky2rpgcore.nangbac.GradeUtils;
import mk.plugin.sky2rpgcore.weapon.Grade;


public class SRPGItem implements Cloneable {
	
	private String name;
	private int level;
	private int gemHole;
	private Map<SRPGStat, Integer> stats = new LinkedHashMap<SRPGStat, Integer> ();
	private List<SRPGGem> gems = new ArrayList<SRPGGem> ();
	private Map<SRPGEnchant, Integer> enchants = new LinkedHashMap<SRPGEnchant, Integer> ();
	private String desc;
	private Grade tier;
	private int gradeExp;
	
	public SRPGItem(String name, int level, int gemHole, Map<SRPGStat, Integer> stats, List<SRPGGem> gems, Map<SRPGEnchant, Integer> enchants, String desc, Grade tier, int gradeExp) {
		this.name = ChatColor.stripColor(name);
		this.level = Math.max(level, 0);
		this.stats = stats == null ? this.stats : stats;
		this.gems = gems == null ? this.gems : gems;
		this.enchants = enchants == null ? this.enchants : enchants;
		this.gemHole = gemHole;
		this.desc = desc;
		this.tier = tier;
		this.gradeExp = gradeExp;
	}
	
	// Getters
	
	@Override
	public SItem clone() {
		return new SItem(name, level, gemHole, stats, gems, enchants, desc, tier, gradeExp);
	}
	
	public int getGradeExp() {
		return this.gradeExp;
	}
	
	public String getName() {
		return this.name;
	}
	
	public int getLevel() {
		return this.level;
	}
	
	public Grade getGrade() {
		for (Grade g : Grade.values()) {
			if (GradeUtils.getExpTo(g) <= this.gradeExp) this.tier = g;
		}
		return this.tier;
	}
	
	public String getDesc() {
		return this.desc;
	}
	
	public int getCalculatedStat(Player player, SRPGStat stat) {
		int value = 0;
		if (this.stats.containsKey(stat)) value += getStat(stat);
		value += 2 * this.level;
		
		// Grade
		value *= tier.getBonus();
		
		// Gem
		for (SRPGGem gem : gems) {
			if (gem.getStat().equals(stat)) {
				value *= (double) (100 + gem.getValue()) / 100;
			}
		}
		
		// ESet
		if (ESetUtils.isInAnySet(this.name)) {
			ESet set = ESetUtils.getSetFromItem(name);
			int amount = ESetUtils.getAmount(player, set);
			if (amount > 1) {
				for (SRPGStatBuff buff : set.getBuffs(amount)) {
					if (buff.getStat() == stat) {
						value = value * (100 +  buff.getValue()) / 100;
					}

				};
			}
		}
		return value;
	}
	
	public int getBonusStat(Player player, SRPGStat stat) {
		if (getStat(stat) == 0) return 0;
		return getCalculatedStat(player, stat) - getStat(stat);
	}
	
	public int getStat(SRPGStat stat) {
		return this.stats.getOrDefault(stat, 0);
	}
	
	public Map<SRPGStat, Integer> getStats() {
		return this.stats;
	}
	
	public List<SRPGGem> getGems() {
		return this.gems;
	}
	
	public Map<SRPGEnchant, Integer> getEnchants() {
		return this.enchants;
	}
	
	public int getEnchant(SRPGEnchant enchant) {
		if (this.enchants.containsKey(enchant)) return this.enchants.get(enchant);
		return 0;
	}
	
	public int getGemHole() {
		return this.gemHole;
	}
	
	public int getBlankGemHole() {
		return this.gemHole - this.gems.size();
	}
	
	// Setters
	
	public void setGradeExp(int gradeExp) {
		this.gradeExp = gradeExp;
	}
	
	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	public void setTier(Grade tier) {
		this.tier = tier;
		this.gradeExp = Math.max(GradeUtils.getExpTo(tier), this.gradeExp);
	}
	
	public void setGrade(Grade tier) {
		setTier(tier);
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setLevel(int level) {
		this.level = level;
	}
	
	public void setStats(Map<SRPGStat, Integer> stats) {
		this.stats = stats;
	}
	
	public void setGems(List<SRPGGem> gems) {
		this.gems = gems;
	}
	
	public void addGem(SRPGGem gem) {
		this.gems.add(gem);
	}
	
	public void removeGem(SRPGGem gem) {
		this.gems.remove(gem);
	}
	
	public void setEnchants(Map<SRPGEnchant, Integer> enchants) {
		this.enchants = enchants;
	}
	
	public void addEnchant(SRPGEnchant enchant) {
		this.enchants.put(enchant, getEnchant(enchant) + 1);
	}
	
	public void removeEnchant(SRPGEnchant enchant) {
		this.enchants.remove(enchant);
	}
	
	public void setGemHole(int gemHole) {
		this.gemHole = gemHole;
	}
	
	
}
