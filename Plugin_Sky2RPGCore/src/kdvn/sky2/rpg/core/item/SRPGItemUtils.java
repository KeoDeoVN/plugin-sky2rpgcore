package kdvn.sky2.rpg.core.item;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.inventory.meta.ItemMeta;

import kdvn.sky2.lockeditem.utils.LIConfigUtils;
import kdvn.sky2.rpg.core.gem.SRPGGem;
import kdvn.sky2.rpg.core.rate.SRPGRate;
import kdvn.sky2.rpg.core.senchant.SRPGEnchant;
import kdvn.sky2.rpg.core.stat.SRPGStat;
import kdvn.sky2.rpg.core.utils.ItemStackUtils;
import kdvn.sky2.rpg.core.utils.SRPGUtils;
import mk.plugin.eternalcore.feature.set.ESet;
import mk.plugin.eternalcore.feature.set.ESetUtils;
import mk.plugin.sky2rpgcore.jewelry.Jewelry;
import mk.plugin.sky2rpgcore.jewelry.util.JewelryUtils;
import mk.plugin.sky2rpgcore.weapon.Grade;
import mk.plugin.sky2rpgcore.weapon.Weapon;
import mk.plugin.sky2rpgcore.weapon.WeaponUtils;

public class SRPGItemUtils {
	
	public static final int MAX_ITEM_LEVEL = 15;
	
	public static boolean isRPGItem(ItemStack item) {
		if (ItemStackUtils.isNull(item)) return false;
		return ItemStackUtils.hasTag(item, "sRPG");
	}
	
	public static void updateItem(Player player, ItemStack itemStack, SRPGItem rpgItem) {
		List<String> lore = new ArrayList<String> ();
		
		// Locked
		lore.add(LIConfigUtils.LOCKED_LORES.get(0));
		lore.add("");
		
		// Stat
		rpgItem.getStats().forEach((stat, value) -> {
			lore.add(stat.getColor() + stat.getName() + ": §7+" + value + " §7(" + rpgItem.getGrade().getColor() + "+" + rpgItem.getBonusStat(player, stat) + "§7)");
		});
		lore.add("");
		
		// Vallina enchant ⌘
		boolean hasEnchant = false;
		ItemMeta meta = itemStack.getItemMeta();
		if (meta.getEnchants().size() != 0) {
			hasEnchant = true;
			meta.getEnchants().keySet().forEach(e -> {
				lore.add("§2Phép: §d" + SRPGUtils.getEnchantFormat(e, meta.getEnchantLevel(e)));
			});
		}
		// Special enchant
		if (rpgItem.getEnchants().size() != 0) {
			hasEnchant = true;
			rpgItem.getEnchants().forEach((e, i) -> {
				lore.add("§2Phép: §c" + e.getName() + " " + SRPGUtils.soLaMa(i));
			});
		}
		if (hasEnchant) lore.add("");
		
		// Gem
		boolean hasGem = rpgItem.getGems().size() != 0;
		boolean hasSlot = rpgItem.getBlankGemHole() > 0;
		if (hasGem || hasSlot) {
			rpgItem.getGems().forEach(gem -> {
				lore.add("§b■ " + "+" + gem.getValue() + "% " + gem.getStat().getName());
			});
			for (int i = 0 ; i < rpgItem.getBlankGemHole() ; i++) {
				lore.add("§b□ <Lỗ ngọc trống>");
			}
		}
		
		
		// Set
		String name = ChatColor.stripColor(rpgItem.getName());
		if (player != null) {
			// Set
			if (ESetUtils.isInAnySet(name)) {
				lore.add("");
				// Check if amount >= 2
				ESet set = ESetUtils.getSetFromItem(name);
				int amount = ESetUtils.getAmount(player, set);
				lore.add("§6Set : §e" + set.getName() + " [" + amount + "/" + set.getItems().size() + "]");
				// Add desc line
				lore.add(set.getDesc(amount));
			}
		}
		
		// Name
		String displayName = rpgItem.getGrade().getColor() +  "§l" + rpgItem.getName().replace("_", " ");
		Grade tier = rpgItem.getGrade();
		String sl = rpgItem.getLevel() <= 0 ? "" : "§f§l[§c§l+" + rpgItem.getLevel() + "§f§l]";
		String tiers = "§r§f§l<" + tier.getColor() + "§l" + tier.name() + "§f§l>";
		displayName = tiers + " " + displayName + " " + sl;
		
		// Desc
		if (rpgItem.getDesc() != null) {
			int maxsize = 0;
			for (String s : lore) {
				if (ChatColor.stripColor(s).length() > maxsize) maxsize = ChatColor.stripColor(s).length();
			}
			String desc = rpgItem.getDesc();
			List<String> descs = SRPGUtils.toList(desc, maxsize - 3, "§f");
			lore.add(2, "");
			lore.addAll(2, descs);
		}
		
		// Weapon
		if (WeaponUtils.isWeapon(itemStack)) {
			Weapon w = WeaponUtils.fromItemStack(itemStack);
			lore.add("");
			lore.add("§cS.Trái: §f" + w.getType().getSkillName());
			itemStack.setType(w.getType().getMaterial());
			((Damageable) meta).setDamage(w.getType().getDurabilities().get(rpgItem.getGrade().getNumber() - 1));
		}
		
		// Jewelry
		if (JewelryUtils.isJewelry(itemStack)) {
			Jewelry w = JewelryUtils.fromItemStack(itemStack);
			lore.add("");
			lore.add("§cNội tại: §f" + w.getType().getPassiveName());
			itemStack.setType(w.getType().getMaterial());
			((Damageable) meta).setDamage(w.getType().getDurabilities().get(rpgItem.getGrade().getNumber() - 1));
		}
		
		// Set item
		meta.setDisplayName(displayName);
		meta.setLore(lore);
		meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_UNBREAKABLE, ItemFlag.HIDE_ENCHANTS);
		meta.setUnbreakable(true);
		itemStack.setItemMeta(meta);
	}
	
	public static ItemStack setItem(Player player, ItemStack itemStack, SRPGItem rpgItem) {
		updateItem(player, itemStack, rpgItem);
		itemStack = setWithoutUpdate(player, itemStack, rpgItem);
		
		return itemStack;
	}
	
	public static ItemStack setWithoutUpdate(Player player, ItemStack itemStack, SRPGItem rpgItem) {
		Map<String, String> tags = new HashMap<String, String> ();
		tags.put("sRPG", "true");
		tags.put("sRPG.name", rpgItem.getName());
		tags.put("sRPG.level", rpgItem.getLevel() + "");
		tags.put("sRPG.gemHole", rpgItem.getGemHole() + "");
		tags.put("sRPG.stats", mapStatToString(rpgItem.getStats()));
		tags.put("sRPG.gems", listGemToString(rpgItem.getGems()));
		tags.put("sRPG.senchants", mapSEnchantToString(rpgItem.getEnchants()));
		tags.put("sRPG.grade", rpgItem.getGrade().name());
		tags.put("sRPG.gradeExp", rpgItem.getGradeExp() + "");
		if (rpgItem.getDesc() != null) tags.put("sRPG.desc", rpgItem.getDesc());
		itemStack = ItemStackUtils.setTag(itemStack, tags);
		
		return itemStack;
	}
	
	public static SItem fromItemStack(ItemStack itemStackk) {
		if (!isRPGItem(itemStackk)) return null;
		ItemStack itemStack = itemStackk.clone();
		
		// Debug
		try {
			String name = ItemStackUtils.getTag(itemStack, "sRPG.name");
			int level = Integer.parseInt(ItemStackUtils.getTag(itemStack, "sRPG.level"));
			int gemHole = Integer.parseInt(ItemStackUtils.getTag(itemStack, "sRPG.gemHole"));
			Map<SRPGStat, Integer> stats = mapStatFromString(ItemStackUtils.getTag(itemStack, "sRPG.stats"));
			List<SRPGGem> gems = listGemFromString(ItemStackUtils.getTag(itemStack, "sRPG.gems"));
			Map<SRPGEnchant, Integer> enchants = mapSEnchantFromString(ItemStackUtils.getTag(itemStack, "sRPG.senchants"));
			String desc = null;
			if (ItemStackUtils.hasTag(itemStack, "sRPG.desc")) {
				desc = ItemStackUtils.getTag(itemStack, "sRPG.desc");
			}
			Grade tier = Grade.I;
			if (ItemStackUtils.hasTag(itemStack, "sRPG.grade")) {
				tier = Grade.valueOf(ItemStackUtils.getTag(itemStack, "sRPG.grade"));
			}
			int gradeExp = 0;
			if (ItemStackUtils.hasTag(itemStack, "sRPG.gradeExp")) {
				gradeExp = Integer.valueOf(ItemStackUtils.getTag(itemStack, "sRPG.gradeExp"));
			}
			
			
			SItem rpgItem = new SItem(name, level, gemHole, stats, gems, enchants, desc, tier, gradeExp);
			
			return rpgItem;
		}
		catch (Exception e) {
			e.printStackTrace();
			Player mankaiStep = Bukkit.getPlayer("MankaiStep");
			if (mankaiStep != null) {
				mankaiStep.getInventory().addItem(itemStack);
				mankaiStep.sendTitle("Bug ne anh gi dep trai oi", "Bugggggg", 0, 100, 0);
			}
		}
		
		SItem rpgItem = null;
		
		
		return rpgItem;
	}
	
	public static ItemStack setLevel(ItemStack itemStack, int level) {
		SRPGItem rpgI = fromItemStack(itemStack);
		rpgI.setLevel(level);
		return setItem(null, itemStack, rpgI);
	}
	
	public static ItemStack addLevel(ItemStack itemStack, int level) {
		SRPGItem rpgI = fromItemStack(itemStack);
		rpgI.setLevel(Math.min(MAX_ITEM_LEVEL, rpgI.getLevel() + level));
		return setItem(null, itemStack, rpgI);
	}
	
	// From String and To String
	
	public static String mapStatToString(Map<SRPGStat, Integer> map) {
		String result = "";
		if (map.size() == 0) return result;
		for (SRPGStat e : map.keySet()) {
			result += e.name() + ":" + map.get(e) + ";";
		}
		result = result.substring(0, result.length() - 1);
		return result;
	}
	
	public static Map<SRPGStat, Integer> mapStatFromString(String s) {
		Map<SRPGStat, Integer> stats = new LinkedHashMap<SRPGStat, Integer> ();
		if (s.length() == 0) return stats;
		String[] list = s.split(";");
		for (int i = 0 ; i < list.length ; i ++) {
			stats.put(SRPGStat.valueOf(list[i].split(":")[0]), Integer.parseInt(list[i].split(":")[1]));
		}
		return stats;
	}
	
	public static String mapSEnchantToString(Map<SRPGEnchant, Integer> map) {
		String result = "";
		if (map.size() == 0) return result;
		for (SRPGEnchant e : map.keySet()) {
			result += e.name() + ":" + map.get(e) + ";";
		}
		result = result.substring(0, result.length() - 1);
		return result;
	}
	
	public static Map<SRPGEnchant, Integer> mapSEnchantFromString(String s) {
		Map<SRPGEnchant, Integer> stats = new LinkedHashMap<SRPGEnchant, Integer> ();
		if (s.length() == 0) return stats;
		String[] list = s.split(";");
		for (int i = 0 ; i < list.length ; i ++) {
			stats.put(SRPGEnchant.valueOf(list[i].split(":")[0]), Integer.parseInt(list[i].split(":")[1]));
		}
		return stats;
	}
	
	public static String gemToString(SRPGGem gem) {
		String s = "";
		s += gem.getStat().name();
		s += ":" + gem.getValue();
		s += ":" + gem.getRate().name();
		s += ":" + gem.getName();
		return s;
	}
	 
	public static String listGemToString(List<SRPGGem> list) {
		String s = "";
		if (list.size() == 0) return s;
		for (SRPGGem gem : list) {
			s += gemToString(gem) + ";";
		}
		return s.substring(0, s.length() - 1);
	}
	
	public static SRPGGem gemFromString(String s) {
		String[] list = s.split(":");
		SRPGGem gem = new SRPGGem(list[3], SRPGStat.valueOf(list[0]), Integer.parseInt(list[1]), SRPGRate.valueOf(list[2]));
		return gem;
	}
	
	public static List<SRPGGem> listGemFromString(String s) {
		String[] list = s.split(";");
		List<SRPGGem> gems = new ArrayList<SRPGGem> ();
		if (s.length() == 0) return gems;
		for (String ss : list) {
			gems.add(gemFromString(ss));
		}
		return gems;
	}

	
	
	
	
	
	
	
	
	
}
