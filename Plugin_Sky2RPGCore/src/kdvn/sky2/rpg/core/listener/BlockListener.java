package kdvn.sky2.rpg.core.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

import mk.plugin.sky2rpgcore.specialmine.SMineUtils;

public class BlockListener implements Listener {
	
	@EventHandler
	public void onBreak(BlockBreakEvent e) {
		SMineUtils.playerBreak(e);
	}
}
