package kdvn.sky2.rpg.core.listener;

import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntitySpawnEvent;

import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.mob.SRPGMobUtils;

public class MobSpawnListener implements Listener {
	
	@EventHandler
	public void onSpawn(EntitySpawnEvent e) {
		Entity entity = e.getEntity();
		if (entity instanceof LivingEntity) {
			LivingEntity le = (LivingEntity) entity;
			Bukkit.getScheduler().runTaskLaterAsynchronously(MainSky2RPGCore.getMain(), () -> {
				if (SRPGMobUtils.checkName(le)) {
					SRPGMobUtils.setRPGFromName(le, le.getName());
				}
			}, 5);
		}
	}
}
