package kdvn.sky2.rpg.core.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.player.SRPGPlayerUtils;
import kdvn.sky2.rpg.core.utils.ChatPlaceholder;

public class SRPGChatListener implements Listener {
	
	// Chatplaceholder
	@EventHandler
	public void onPlaceholderChat(AsyncPlayerChatEvent e) {
		Player player = e.getPlayer();
		if (ChatPlaceholder.cps.containsKey(player)) {
			player.sendMessage("§7Yêu cầu chat kết thúc!");
			ChatPlaceholder cp = ChatPlaceholder.cps.get(player);
			String s = e.getMessage();
			cp.runCmd(s.replace("&", "§"), MainSky2RPGCore.getMain());
			ChatPlaceholder.cps.remove(player);
			e.setCancelled(true);
		}
	}
	
	// Prefix chat
	@EventHandler
	public void onChat(AsyncPlayerChatEvent e) {
		Player player = e.getPlayer();
		String prefix = SRPGPlayerUtils.getChatPrefix(player);
		e.setFormat(prefix + " " + e.getFormat());
		
		// Sky2XacMinh
//		if (Bukkit.getPluginManager().isPluginEnabled("Sky2XacMinh")) {
//			e.setFormat(XacMinhUtils.getPrefixChat(player) + e.getFormat());
//		}

	}
	
}
