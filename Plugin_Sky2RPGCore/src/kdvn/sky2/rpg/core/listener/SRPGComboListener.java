package kdvn.sky2.rpg.core.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.inventory.EquipmentSlot;

import kdvn.sky2.rpg.core.skill.SRPGSkillCombo;
import kdvn.sky2.rpg.core.utils.SRPGUtils;
import net.minecraft.server.v1_13_R2.SoundEffects;

public class SRPGComboListener implements Listener {
	
	@EventHandler
	public void onInteract(PlayerInteractEvent e) {
		if (e.getAction() != Action.RIGHT_CLICK_AIR && e.getAction() != Action.LEFT_CLICK_AIR) return;
		if (!SRPGSkillCombo.isPlayerComboed(e.getPlayer())) return;
		if (e.getHand() != EquipmentSlot.HAND) return;

		SRPGUtils.sendSound(e.getPlayer(), SoundEffects.BLOCK_WOODEN_BUTTON_CLICK_ON, 1f, 1f);
		if (e.getAction() == Action.LEFT_CLICK_AIR || e.getAction() == Action.LEFT_CLICK_BLOCK) {
			SRPGSkillCombo.addOne(e.getPlayer(), 0);
			SRPGSkillCombo.sendTitleCombo(e.getPlayer());
		}
		else if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			SRPGSkillCombo.addOne(e.getPlayer(), 1);
			SRPGSkillCombo.sendTitleCombo(e.getPlayer());
		}
	}
	
	@EventHandler
	public void onSneak(PlayerToggleSneakEvent e) {
		if (e.isSneaking()) {
			SRPGSkillCombo.addCombo(e.getPlayer());
		} else {
			SRPGSkillCombo.comboExcute(e.getPlayer());
			SRPGSkillCombo.huyCombo(e.getPlayer());
		}
		
	}
	
}
