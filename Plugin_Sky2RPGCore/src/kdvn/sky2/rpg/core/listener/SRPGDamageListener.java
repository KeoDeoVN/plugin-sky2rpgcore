package kdvn.sky2.rpg.core.listener;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import com.google.common.collect.Lists;

import kdvn.sky2.rpg.core.damagecheck.SRPGDamageCheck;
import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.mob.SRPGMobUtils;
import kdvn.sky2.rpg.core.player.SRPGPlayer;
import kdvn.sky2.rpg.core.player.SRPGPlayerUtils;
import kdvn.sky2.rpg.core.senchant.SRPGEnchant;
import kdvn.sky2.rpg.core.skills.SRPGSkillChoang;
import kdvn.sky2.rpg.core.skills.SRPGSkillGiapLua;
import kdvn.sky2.rpg.core.skills.SRPGSkillKhoHeo;
import kdvn.sky2.rpg.core.skills.SRPGSkillMu;
import kdvn.sky2.rpg.core.skills.SRPGSkillNhanhChan;
import kdvn.sky2.rpg.core.skills.SRPGSkillTenNo;
import kdvn.sky2.rpg.core.stat.SRPGStat;
import kdvn.sky2.rpg.core.utils.SRPGUtils;
import mk.plugin.sky2rpgcore.element.SElement;
import mk.plugin.sky2rpgcore.hologram.HologramUtils;
import mk.plugin.sky2rpgcore.weapon.Weapon;
import mk.plugin.sky2rpgcore.weapon.WeaponType;
import mk.plugin.sky2rpgcore.weapon.WeaponUtils;
import net.minecraft.server.v1_13_R2.SoundEffects;

public class SRPGDamageListener implements Listener {
	
	public final int ENTITY_DEFAULT_SUCTHU = 50;
	
	private static Map<String, Long> cooldownAttack = new HashMap<String, Long> ();	
	
	public static boolean isCooldownAttack(Player player) {
		if (!cooldownAttack.containsKey(player.getName())) return false;
		if (cooldownAttack.get(player.getName()) < System.currentTimeMillis()) return false;
		return true;
	}
	
	public static void setCooldownAttack(Player player, long timeMilis, ItemStack item) {
		if (item != null) player.setCooldown(item.getType(), new Long(timeMilis / 50).intValue());
		cooldownAttack.put(player.getName(), System.currentTimeMillis() + timeMilis);	
	}
	
	public static void removeCooldownAttack(Player player) {
		cooldownAttack.remove(player.getName());
	}
	
	@EventHandler
	public void onClick(PlayerInteractEvent e) {
		if (e.getAction() == Action.LEFT_CLICK_AIR) {
			Player player = e.getPlayer();
			
			// Shift
			if (player.isSneaking()) {
				e.setCancelled(true);
				return;
			}
			
			ItemStack item = e.getItem();
			if (!WeaponUtils.isWeapon(item)) return;
			Weapon w = WeaponUtils.fromItemStack(item);
			if (w.getType() == WeaponType.BOW) {
				if (isCooldownAttack(player)) return;
				Arrow arrow = player.launchProjectile(Arrow.class);
				player.playSound(player.getLocation(), Sound.ENTITY_ARROW_SHOOT, 1, 1);
				setCooldownAttack(player, new Double(SRPGPlayerUtils.getStatValue(player, SRPGStat.TOC_DANH) * 1000).longValue(), item);
				
				
				Bukkit.getScheduler().runTaskLater(MainSky2RPGCore.getMain(), () -> {
					arrow.remove();
				}, 10);
			}
		}
	}
	
	
	// Player damage entity
	@EventHandler
	public void onPlayerDamageEntity(EntityDamageByEntityEvent e) {
		// Check if NPC
		Entity en = e.getEntity();
		if (en.hasMetadata("NPC")) {
			return;
		}
		if (e.getCause() == DamageCause.ENTITY_SWEEP_ATTACK) {
			e.setCancelled(true);
			return;
		}
		
		
		// Player Damage entity
		if (e.getDamager() instanceof Player || e.getDamager() instanceof Arrow) {
			if (e.getEntity() instanceof LivingEntity) {
				
				// Check player
				Player p;
				if (e.getDamager() instanceof Projectile) {
					Projectile a = (Projectile) e.getDamager();
					if (a.getShooter() instanceof Player) {
						p = (Player) a.getShooter();
					} else return;
				} else p = (Player) e.getDamager();
				Player player = p;
				
				// Cooldown
				ItemStack item = player.getInventory().getItemInMainHand();
				if (WeaponUtils.isWeapon(item)) {
					Weapon w = WeaponUtils.fromItemStack(item);
					if (w.getType() != WeaponType.BOW) {
						if (!en.hasMetadata("notattack")) {
							if (isCooldownAttack(player)) {
								e.setCancelled(true);
								return;
							} else setCooldownAttack(player, new Double(SRPGPlayerUtils.getStatValue(player, SRPGStat.TOC_DANH) * 1000).longValue(), player.getInventory().getItemInMainHand());
						} else en.removeMetadata("notattack", MainSky2RPGCore.getMain());
					}
				}
				
			
				
				LivingEntity entity = (LivingEntity) e.getEntity();
//				SRPGPlayer rpgP = SRPGPlayerUtils.getData(player);
				
				double damage = e.getDamage();
				List<String> holos = Lists.newArrayList();
				
				// Attak while combo
				if (player.isSneaking()) {
					e.setCancelled(true);
					return;
				}

				// + Stat
				double statDamage = SRPGPlayerUtils.getStatValue(player, SRPGStat.SAT_THUONG);
				damage += statDamage;
				
				// + Crit
				boolean crit = false;
				double critChance = SRPGPlayerUtils.getStatValue(player, SRPGStat.CHI_MANG);
				if (SRPGUtils.rate(critChance)) {
					crit = true;
					damage *= 2;
					// Effet Crit
					Bukkit.getScheduler().runTaskAsynchronously(MainSky2RPGCore.getMain(), () -> {
						Location loc = entity.getLocation();
						loc.getWorld().spawnParticle(Particle.CLOUD, loc.clone().add(0, 1.5, 0.0), 7);
						SRPGUtils.sendSound(player, SoundEffects.BLOCK_ANVIL_LAND, 0.3f, 1f);
					});
				}
				
				// Suc thu, Xuyen giap
				if (!(entity instanceof Player)) {
					if (!SRPGUtils.rate(SRPGPlayerUtils.getStatValue(player, SRPGStat.XUYEN_GIAP))) {
						double defenseValue = ENTITY_DEFAULT_SUCTHU;
						e.setDamage(e.getDamage() * (1 - ((double) defenseValue / 100)));
					} else 	holos.add("§a§oXuyên giáp");
				}
				
				
				// Skill Kho Heo
				int levelKhoHeo = SRPGPlayerUtils.getEnchant(SRPGEnchant.KHO_HEO, player);
				if (levelKhoHeo > 0) {
					((SRPGSkillKhoHeo) SRPGEnchant.KHO_HEO.getISkill()).doIt(entity, levelKhoHeo);
					holos.add("§d§oKhô héo");
				}
				
				// Skill Mu
				int levelMu = SRPGPlayerUtils.getEnchant(SRPGEnchant.MU, player);
				if (levelMu > 0) {
					((SRPGSkillMu) SRPGEnchant.MU.getISkill()).doIt(entity, levelMu);
					holos.add("§d§oMù");
				}
				
				// Skill Nhanh chan
				int levelnhanhChan = SRPGPlayerUtils.getEnchant(SRPGEnchant.NHANH_CHAN, player);
				if (levelnhanhChan > 0) {
					((SRPGSkillNhanhChan) SRPGEnchant.NHANH_CHAN.getISkill()).start(player, levelnhanhChan);
				}
				
				// If target is player
				if (e.getEntity() instanceof Player) {
					Player target = (Player) e.getEntity();
					
					// Skill Choang
					int levelChoang = SRPGPlayerUtils.getEnchant(SRPGEnchant.CHOANG, player);
					if (levelChoang > 0) {
						((SRPGSkillChoang) SRPGEnchant.CHOANG.getISkill()).doIt(target, levelnhanhChan);
						holos.add("§d§oChoáng");
					}
					
					// Element counter
					SRPGPlayer rT = SRPGPlayerUtils.getData(target);
					SRPGPlayer rP = SRPGPlayerUtils.getData(player);
					
					if (rP.hasElement() && rT.hasElement()) {
						if (rT.getElement().getCounter() == rP.getElement()) damage *= 1.25;
					}
				}
				
				// Element
				SRPGPlayer rP = SRPGPlayerUtils.getData(player);
				if (rP.getElement() != null) {
					SElement element = rP.getElement();
					player.getWorld().spawnParticle(element.getParticle(), entity.getLocation().add(0, 1, 0), 10, 0.5f, 0.5f, 0.5f, 0.1f);
				}
				
				// Damage = arrow
				if (e.getDamager() instanceof Arrow) {
					// Skill doc tien
//					if (SRPGSkillUtils.canExecute(player, SRPGSkill.DOC_TIEN)) {
//						((SRPGSkillDocTien) SRPGSkill.DOC_TIEN.getISkill()).doIt(entity, rpgP.getSClass().getLevel());
//						holos.add("§d§oĐộc tiễn");
//					}
					// Skill ten no
					int lvTenNo = SRPGPlayerUtils.getEnchant(SRPGEnchant.TEN_NO, player);
					if (lvTenNo > 0) {
						((SRPGSkillTenNo) SRPGEnchant.TEN_NO.getISkill()).doIt(entity, lvTenNo);
						holos.add("§d§oTên nổ");
					}
				}
				
				// Skill Giang don
//				if (SRPGSkillUtils.canExecute(player, SRPGSkill.GIANG_DON)) {
//					((SRPGSkillGiangDon) SRPGSkill.GIANG_DON.getISkill()).doIt(entity, rpgP.getSClass().getLevel());
//					holos.add("§d§oGiáng đòn");
//				}
				
				// Damage entity not player
				if (!(entity instanceof Player)) {
					// Skill Sat quy
//					if (SRPGSkillUtils.canExecute(player, SRPGSkill.SAT_QUY)) {
//						damage = ((SRPGSkillSatQuy) SRPGSkill.SAT_QUY.getISkill()).changeDamage(damage, rpgP.getSClass().getLevel());
//						holos.add("§d§oSát quỷ");
//					}
				}
				
				
				e.setDamage(damage);

				boolean isCrit = crit;
				// After damage
				final double healthBefore = entity.getHealth();
				new BukkitRunnable() {
					@Override
					public void run() {
						double healthAfter = entity.getHealth();
						// Get damage
						double damage = 0;
						if (entity.isDead()) {
							damage = healthBefore;
						}
						else {;
							damage = healthBefore - healthAfter;
						}
						holos.add(0, isCrit ? "§6§l§o-" + SRPGUtils.round(damage) : "§c§l§o-" + SRPGUtils.round(damage));
						// Actionbar
//						SRPGActionBar.damageSend(player, entity, healthAfter, entity.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue(), damage);
						
						// Stat Hut mau
						double value = SRPGPlayerUtils.getStatValue(player, SRPGStat.HUT_MAU);
						SRPGUtils.addHealth(player, (double) damage * value / 100);
						
						// Holograms
						HologramUtils.hologram(MainSky2RPGCore.getMain(), holos, 15, player, entity, 1);
						
						// Damage check
						if (!SRPGMobUtils.isMob(entity)) return;
						SRPGDamageCheck.addDamage(entity, player, damage);

					}
				}.runTaskLaterAsynchronously(MainSky2RPGCore.getMain(), 1);
			}
		}
		
	}
	
	// Player get damaged
	@EventHandler
	public void onPlayerDamagedByEntity(EntityDamageByEntityEvent e) {
		if (e.getEntity() instanceof Player) {
			Player player = (Player) e.getEntity();
			
			// Check if NPC
			if (player.hasMetadata("NPC")) return;
//			SRPGPlayer rpgP = SRPGPlayerUtils.getData(player);
			
			// Stat Dodge
			double dodgeChance = SRPGPlayerUtils.getStatValue(player, SRPGStat.NE);
			if (SRPGUtils.rate(dodgeChance)) {
				Location loc = player.getLocation();
				loc.add(loc.getDirection().multiply(1.3f));
				SRPGUtils.hologram(SRPGUtils.randomLoc(loc, 1), "§2§lNé", 15, player);
				if (e.getDamager() instanceof Player) {
					SRPGUtils.hologram(SRPGUtils.randomLoc(loc, 1), "§c§lNé", 15, (Player) e.getDamager());
				}
				e.setCancelled(true);
				return;
			}
			
			boolean hasSucthu = true;
			
			// Damaged my player
			if (e.getDamager() instanceof Player) {
				Player damager = (Player) e.getDamager();
				double xuyenGiapValue = SRPGPlayerUtils.getStatValue(damager, SRPGStat.XUYEN_GIAP);
				if (SRPGUtils.rate(xuyenGiapValue)) hasSucthu = false;
			} 
			if (hasSucthu) {
				double defenseValue = SRPGPlayerUtils.getStatValue(player, SRPGStat.SUC_THU);
				e.setDamage(e.getDamage() * (1 - ((double) defenseValue / 100)));
			}
			
			// Damaged by player and entity
			if (e.getDamager() instanceof LivingEntity) {
				LivingEntity entity = (LivingEntity) e.getDamager();
				
				// Skill Giap lua
				int levelGiapLua = SRPGPlayerUtils.getEnchant(SRPGEnchant.GIAP_LUA, player);
				if (levelGiapLua > 0) {
					((SRPGSkillGiapLua) SRPGEnchant.GIAP_LUA.getISkill()).ignite(entity, levelGiapLua);
				}
				
				// Skill Kien cuong
//				if (SRPGSkillUtils.canExecute(player, SRPGSkill.KIEN_CUONG)) {
//					double afterDamage = ((SRPGSkillKienCuong) SRPGSkill.KIEN_CUONG.getISkill()).change(e.getDamage(), rpgP.getSClass().getLevel());
//					e.setDamage(afterDamage);
//				}
				
				// Not for player
//				if (entity.getType() != EntityType.PLAYER) {
//					// Phản đòn
//					double phanDon = SRPGPlayerUtils.getStatValue(player, SRPGStat.PHAN_DON);
//					SRPGPlayerUtils.applyPhanDonEntity(player, entity, e.getDamage(), new Double(phanDon).intValue());
//				}
				
			}
		}	
	}
	
}
