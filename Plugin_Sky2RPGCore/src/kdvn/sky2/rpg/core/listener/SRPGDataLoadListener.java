package kdvn.sky2.rpg.core.listener;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerSwapHandItemsEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import com.google.common.collect.Lists;

import kdvn.sky2.lockeditem.utils.LIUtils;
import kdvn.sky2.rpg.core.item.SItem;
import kdvn.sky2.rpg.core.item.SItemUtils;
import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.player.SRPGPlayerUtils;

public class SRPGDataLoadListener implements Listener{
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		Player player = e.getPlayer();
		SRPGPlayerUtils.playerJoin(player);
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent e) {
		Player player = e.getPlayer();
		SRPGPlayerUtils.playerQuit(player);
	}
	
	@EventHandler
	public void onCloseInv(InventoryCloseEvent e) {
		if (e.getInventory().getType() != InventoryType.CRAFTING) return;
		Player player = (Player) e.getPlayer();
		Bukkit.getScheduler().runTaskAsynchronously(MainSky2RPGCore.getMain(), () -> {
			SRPGPlayerUtils.updatePlayer(player);
		});
	}
	
	@EventHandler
	public void onSwitchItem(PlayerItemHeldEvent e) {
		Player player = e.getPlayer();
		Bukkit.getScheduler().runTaskAsynchronously(MainSky2RPGCore.getMain(), () -> {
			SRPGPlayerUtils.updatePlayer(player);
		});
	}
	
	@EventHandler
	public void onSwitchHand(PlayerSwapHandItemsEvent e) {
		Player player = e.getPlayer();
		Bukkit.getScheduler().runTaskAsynchronously(MainSky2RPGCore.getMain(), () -> {
			if (e.isCancelled()) return;
			SRPGPlayerUtils.updatePlayer(player);
		});
	}
	
	// Update item
	@EventHandler
	public static void onOpenInventory(InventoryCloseEvent e) {
		Inventory inv = e.getInventory();
		if (inv.getType() == InventoryType.CRAFTING) {
			Player player = (Player) e.getPlayer();
			PlayerInventory pi = player.getInventory();
			List<ItemStack> list = Lists.newArrayList(pi.getArmorContents());
			list.add(pi.getItemInMainHand());
			for (ItemStack item : list) {
				if (item != null) {
					if (SItemUtils.isRPGItem(item)) {
						SItem si = SItemUtils.fromItemStack(item);
						if (LIUtils.isOwner(item, player.getName())) {
							SItemUtils.updateItem(player, item, si);
						}
					}
				}
			}
		}
	}
	
}
