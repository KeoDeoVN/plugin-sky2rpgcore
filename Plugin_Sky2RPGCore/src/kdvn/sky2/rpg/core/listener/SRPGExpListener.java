package kdvn.sky2.rpg.core.listener;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerLevelChangeEvent;

import kdvn.sky2.rpg.core.damagecheck.SRPGDamageCheck;
import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.mob.SRPGMob;
import kdvn.sky2.rpg.core.mob.SRPGMobUtils;
import kdvn.sky2.rpg.core.player.SRPGPlayerUtils;

public class SRPGExpListener implements Listener {
	
	// Level up
	@EventHandler
	public void onLevelUp(PlayerLevelChangeEvent e) {
		if (e.getNewLevel() <= e.getOldLevel()) {
			return;
		}
		int newLevel = e.getNewLevel();
		if (newLevel > SRPGPlayerUtils.MAX_LEVEL) {
			newLevel = SRPGPlayerUtils.MAX_LEVEL;
			e.getPlayer().setLevel(SRPGPlayerUtils.MAX_LEVEL);
			return;
		}

		int newLv = newLevel;
		int oldLv = e.getOldLevel();
		SRPGPlayerUtils.levelUpEffet(e.getPlayer(), oldLv, newLv);
		
	}
	
	// Kill mob
	@SuppressWarnings("deprecation")
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onKillMob(EntityDeathEvent e) {
		if (e.getEntity() instanceof LivingEntity) {
			Player playerK = e.getEntity().getKiller();
			if (playerK == null)
				return;
			
			LivingEntity le = (LivingEntity) e.getEntity();
			if (!SRPGMobUtils.isMob(le)) return;
			
			SRPGMob rpgMob = SRPGMobUtils.fromMob(le);
			
			double maxHealth = le.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue();
			Bukkit.getScheduler().runTaskLater(MainSky2RPGCore.getMain(), () -> {
				SRPGDamageCheck.getDamage(le).forEach((player, damage) -> {
					OfflinePlayer p = Bukkit.getOfflinePlayer(player);
					if (p == null || !p.isOnline()) return;
					SRPGPlayerUtils.gainExp(Bukkit.getPlayer(player), rpgMob, damage, maxHealth);
				});
			}, 4);
		}
	}
	
}
