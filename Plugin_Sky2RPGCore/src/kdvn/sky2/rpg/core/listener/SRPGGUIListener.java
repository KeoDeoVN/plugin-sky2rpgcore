package kdvn.sky2.rpg.core.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;

import kdvn.sky2.rpg.core.gui.chuyenhoa.SRPGGUIChuyenHoa;
import kdvn.sky2.rpg.core.gui.cuonghoa.SRPGGUICuongHoa;
import kdvn.sky2.rpg.core.gui.duclo.SRPGGUIDucLo;
import kdvn.sky2.rpg.core.gui.enchant.SRPGGUIEnchant;
import kdvn.sky2.rpg.core.gui.epda.SRPGGUIEpDa;
import kdvn.sky2.rpg.core.gui.info.SRPGGUIInfo;
import kdvn.sky2.rpg.core.gui.potential.SRPGGUIPotential;
import kdvn.sky2.rpg.core.gui.see.SRPGGUISee;
import kdvn.sky2.rpg.core.gui.tachda.SRPGGUITachDa;
import kdvn.sky2.rpg.core.gui.tachphuphep.SRPGGUITachPhuPhep;
import mk.plugin.sky2rpgcore.jewelry.gui.JewelryGUI;
import mk.plugin.sky2rpgcore.nangbac.GradeGUI;
import mk.plugin.sky2rpgcore.weapon.AllWeaponGUI;
import mk.plugn.sky2rpgcore.itemgui.SItemGUI;

public class SRPGGUIListener implements Listener {
	
	@EventHandler
	public void onClick(InventoryClickEvent e) {
		SRPGGUIInfo.eventClick(e);
//		SRPGGUIClass.eventClick(e);
		SRPGGUIPotential.eventClick(e);
		SRPGGUISee.eventClick(e);
		SRPGGUICuongHoa.eventClick(e);
		SRPGGUIChuyenHoa.eventClick(e);
		SRPGGUIEpDa.eventClick(e);
		SRPGGUIDucLo.eventClick(e);
		SRPGGUITachDa.eventClick(e);
		SRPGGUIEnchant.eventClick(e);
		SRPGGUITachPhuPhep.eventClick(e);
		GradeGUI.eventClick(e);
		SItemGUI.eventClick(e);
		AllWeaponGUI.eventClick(e);
		JewelryGUI.eventClick(e);
	}
	
	@EventHandler
	public void onDrag(InventoryDragEvent e) {
		SRPGGUISee.eventDrag(e);
	}
	
	@EventHandler
	public void onClose(InventoryCloseEvent e) {
		SRPGGUICuongHoa.eventClose(e);
		SRPGGUIChuyenHoa.eventClose(e);
		SRPGGUIEpDa.eventClose(e);
		SRPGGUIDucLo.eventClose(e);
		SRPGGUITachDa.eventClose(e);
		SRPGGUIEnchant.eventClose(e);
		SRPGGUITachPhuPhep.eventClose(e);
		GradeGUI.eventClose(e);
		SItemGUI.eventClose(e);
		JewelryGUI.eventClose(e);
	}
	
}
