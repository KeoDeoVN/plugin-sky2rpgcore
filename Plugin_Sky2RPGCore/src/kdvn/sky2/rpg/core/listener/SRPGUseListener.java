package kdvn.sky2.rpg.core.listener;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.thuochoiphuc.SRPGThuocHoiPhuc;
import kdvn.sky2.rpg.core.utils.SRPGUtils;
import net.minecraft.server.v1_13_R2.SoundEffects;

public class SRPGUseListener implements Listener {
	
	@EventHandler
	public void onUseThuoc(PlayerInteractEvent e) {
		ItemStack item = e.getItem();
		if (item == null || item.getType() == Material.AIR) return;
		SRPGThuocHoiPhuc thuoc = SRPGThuocHoiPhuc.fromItem(item);
		if (thuoc == null) return;
		
		// Use
		e.setCancelled(true);
		Player player = e.getPlayer();
		int health = thuoc.getHealth();
		
		Bukkit.getScheduler().runTaskLaterAsynchronously(MainSky2RPGCore.getMain(), () -> {
			// Add health
			SRPGUtils.addHealth(player, health);
			player.sendTitle("", "§a+" + health + " HP", 0, 10, 0);
			SRPGUtils.sendSound(player, SoundEffects.ENTITY_EXPERIENCE_ORB_PICKUP, 0.5f, 0.5f);
			
			// Removeitem
			if (item.getAmount() > 1) item.setAmount(item.getAmount() - 1);
			else player.getInventory().setItemInMainHand(null);
		}, 0);

	}
	

}
