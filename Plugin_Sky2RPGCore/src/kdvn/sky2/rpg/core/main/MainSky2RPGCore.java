package kdvn.sky2.rpg.core.main;

import java.io.File;
import java.util.List;

import org.black_ixx.playerpoints.PlayerPoints;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;

import kdvn.sky2.rpg.core.command.admin.SRPGAdminCommand;
import kdvn.sky2.rpg.core.command.player.JewelryCommand;
import kdvn.sky2.rpg.core.command.player.SRPGClassCommand;
import kdvn.sky2.rpg.core.command.player.SRPGInfoCommand;
import kdvn.sky2.rpg.core.command.player.SRPGPotentialCommand;
import kdvn.sky2.rpg.core.command.player.SRPGSeeCommand;
import kdvn.sky2.rpg.core.command.player.SRPGTiemRenCommand;
import kdvn.sky2.rpg.core.listener.BlockListener;
import kdvn.sky2.rpg.core.listener.DeathListener;
import kdvn.sky2.rpg.core.listener.MobSpawnListener;
import kdvn.sky2.rpg.core.listener.SRPGChatListener;
import kdvn.sky2.rpg.core.listener.SRPGComboListener;
import kdvn.sky2.rpg.core.listener.SRPGDamageListener;
import kdvn.sky2.rpg.core.listener.SRPGDataLoadListener;
import kdvn.sky2.rpg.core.listener.SRPGExpListener;
import kdvn.sky2.rpg.core.listener.SRPGGUIListener;
import kdvn.sky2.rpg.core.listener.SRPGUseListener;
import kdvn.sky2.rpg.core.party.PartyConsoleCommands;
import kdvn.sky2.rpg.core.party.PartyEventListener;
import kdvn.sky2.rpg.core.party.PartyPlayerCommands;
import kdvn.sky2.rpg.core.permbuff.SPermBuff;
import kdvn.sky2.rpg.core.player.SRPGPlayerDatabase;
import kdvn.sky2.rpg.core.player.SRPGPlayerUtils;
import kdvn.sky2.rpg.core.power.SRPGPowerPlaceholder;
import kdvn.sky2.rpg.core.storage.SRPGMySQL;
import kdvn.sky2.rpg.core.task.SRPGBuffCheckTask;
import kdvn.sky2.rpg.core.task.SRPGHealTask;
import me.realized.tokenmanager.api.TokenManager;
import mk.plugin.eternalcore.feature.set.ESetUtils;
import mk.plugin.sky2rpgcore.jewelry.task.JewelryTask;
import net.milkbowl.vault.economy.Economy;

public class MainSky2RPGCore extends JavaPlugin {
	
	private static MainSky2RPGCore main;	
	private static SRPGMySQL db;
	private static Economy econ = null;
	private static PlayerPoints playerPoints = null;
	public static TokenManager tokenM = null;
	
	public static MainSky2RPGCore getMain() {return main;}
	public static SRPGMySQL getDatabase() {return db;}
    public static Economy getEcononomy() { return econ; }   
    public static PlayerPoints getPP() { return playerPoints; }  
    public static TokenManager getTokenManager() { return tokenM; }	
    
    public static List<String> SPECIAL_MINE_WORLDS = null;
    
	private FileConfiguration config;
	
	@Override
	public void onEnable() {
		main = this;
		
		// Config
		this.saveDefaultConfig();
		reloadConfig();
		
		// Hook
		hookOtherPlugins();
		
		// Load from database
		loadDatabase();
		
		// ...
		registerCommands();
		registerEvents();
		registerPlaceholders();
		runTasks();
		
		
		// Online players
		loadDataOnlinePlayers();
	}
	
	@Override
	public void onDisable() {
		// Save data
		Bukkit.getOnlinePlayers().forEach(p -> {
			SRPGPlayerDatabase.savePlayer(p);
		});
	}
	
	public void reloadConfig() {
		File file = new File(this.getDataFolder(), "config.yml");
		config = YamlConfiguration.loadConfiguration(file);
		
		SPermBuff.load(config);
		ESetUtils.loadSets(config);
		SPECIAL_MINE_WORLDS = config.getStringList("special-mine.worlds");
	}
	
	private void registerEvents() {
		Bukkit.getPluginManager().registerEvents(new SRPGDamageListener(), this);
		Bukkit.getPluginManager().registerEvents(new SRPGChatListener(), this);
		Bukkit.getPluginManager().registerEvents(new SRPGDataLoadListener(), this);
		Bukkit.getPluginManager().registerEvents(new SRPGGUIListener(), this);
		Bukkit.getPluginManager().registerEvents(new SRPGComboListener(), this);
		Bukkit.getPluginManager().registerEvents(new SRPGExpListener(), this);
		Bukkit.getPluginManager().registerEvents(new SRPGUseListener(), this);
		Bukkit.getPluginManager().registerEvents(new PartyEventListener(), this);
		Bukkit.getPluginManager().registerEvents(new DeathListener(), this);
		Bukkit.getPluginManager().registerEvents(new BlockListener(), this);
		Bukkit.getPluginManager().registerEvents(new MobSpawnListener(), this);
	}
	
	private void registerPlaceholders() {
		new SRPGPowerPlaceholder().register();
	}
	
	private void runTasks() {
		new SRPGHealTask().runTaskTimerAsynchronously(this, 20, 20);
		new SRPGBuffCheckTask().runTaskTimerAsynchronously(this, 20, 20);
		new JewelryTask().runTaskTimer(this, 0, 20);
	}
	
	private void registerCommands() {
		this.getCommand("skyrpg").setExecutor(new SRPGAdminCommand());
		this.getCommand("see").setExecutor(new SRPGSeeCommand());
		this.getCommand("xem").setExecutor(new SRPGSeeCommand());
		this.getCommand("tiemnang").setExecutor(new SRPGPotentialCommand());
		this.getCommand("potential").setExecutor(new SRPGPotentialCommand());
		this.getCommand("party").setExecutor(new PartyPlayerCommands());
		this.getCommand("partyadmin").setExecutor(new PartyConsoleCommands());
		this.getCommand("kynang").setExecutor(new SRPGClassCommand());
		this.getCommand("class").setExecutor(new SRPGClassCommand());
		this.getCommand("skill").setExecutor(new SRPGClassCommand());
		this.getCommand("info").setExecutor(new SRPGInfoCommand());
		this.getCommand("thongtin").setExecutor(new SRPGInfoCommand());
		this.getCommand("me").setExecutor(new SRPGInfoCommand());
		this.getCommand("player").setExecutor(new SRPGInfoCommand());
		this.getCommand("tiemren").setExecutor(new SRPGTiemRenCommand());
		this.getCommand("trangsuc").setExecutor(new JewelryCommand());
	}
	
	private void loadDatabase() {
		String host = config.getString("database.host");
		String port = config.getString("database.port");
		String name = config.getString("database.name");
		String user = config.getString("database.user");
		String password = config.getString("database.password");
		db = new SRPGMySQL(host, port, name, user, password);
		SRPGPlayerDatabase.createTables();
	}
	
	private void loadDataOnlinePlayers() {
		Bukkit.getOnlinePlayers().forEach(p -> {
			SRPGPlayerUtils.playerJoin(p);
		});
	}
	
	private void hookOtherPlugins() {
		hookVault();
		hookPlayerPoint();
		tokenM = (TokenManager) Bukkit.getPluginManager().getPlugin("TokenManager");
	}
	
	private boolean hookVault() {
	    if (getServer().getPluginManager().getPlugin("Vault") == null) {
	            return false;
	     }
	    RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
	    if (rsp == null) {
	    	return false;
	    }
	    econ = rsp.getProvider();
	    return econ != null;
	}
	
    private boolean hookPlayerPoint() {
    	final Plugin pl = this.getServer().getPluginManager().getPlugin("PlayerPoints");
    	playerPoints = PlayerPoints.class.cast(pl);
    	return playerPoints != null;
    	
    }
    
    public static WorldGuardPlugin getWorldGuardPlugin() {
		Plugin pl = Bukkit.getPluginManager().getPlugin("WorldGuard");
		if (pl == null) {
			return null;
		}
		return (WorldGuardPlugin) pl;
	}
	
}
