package kdvn.sky2.rpg.core.mob;

public class SRPGMob {
	
	private int level;
	private SRPGMobType mobType;
	
	public SRPGMob(int level, SRPGMobType mobType) {
		this.level = level;
		this.mobType = mobType;
	}

	public int getLevel() {
		return level;
	}

	public SRPGMobType getMobType() {
		return mobType;
	}

	
	
}
