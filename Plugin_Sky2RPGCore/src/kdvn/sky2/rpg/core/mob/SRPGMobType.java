package kdvn.sky2.rpg.core.mob;

public enum SRPGMobType {
	
	THUONG("Thường", 1) {
		@Override
		public int getHealth(int level) {
			return level * (level / 10 + 1) * 5 * 3 / 2;
		}

		@Override
		public int getDamage(int level) {
			return level;
		}
	},
	TINH_ANH("Tinh anh", 5) {
		@Override
		public int getHealth(int level) {
			return level * (level / 10 + 1) * 5 * 5 / 3;
		}

		@Override
		public int getDamage(int level) {
			return level * 2;
		}
	},
	TRUM("Trùm", 15) {
		@Override
		public int getHealth(int level) {
			return level * (level / 10 + 1) * 5 * 3 / 2 * 2;
		}

		@Override
		public int getDamage(int level) {
			return level * 20;
		}
	},
	CHUA_TE("Chúa tể", 100) {
		@Override
		public int getHealth(int level) {
			return level * (level / 10 + 1) * 5 * 2 * 3;
		}

		@Override
		public int getDamage(int level) {
			return level * 50;
		}
	};
	
	public abstract int getHealth(int level);
	public abstract int getDamage(int level);
	
	private String name;
	private int expBonus;
	
	private SRPGMobType(String name, int expBonus) {
		this.name = name;
		this.expBonus = expBonus;
	}
	
	public String getName() {
		return this.name;
	}
	
	public int getExpBonus() {
		return this.expBonus;
	}
	
	
}
