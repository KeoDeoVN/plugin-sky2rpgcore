package kdvn.sky2.rpg.core.mob;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bukkit.attribute.Attribute;
import org.bukkit.entity.LivingEntity;
import org.bukkit.metadata.FixedMetadataValue;

import kdvn.sky2.rpg.core.main.MainSky2RPGCore;

public class SRPGMobUtils {

	
	public static boolean checkName(LivingEntity entity) {
		String s = entity.getName();
		return getLevel(s) != 0 && getName(s) != null;
	}
	
	public static boolean isMob(LivingEntity entity) {
		return entity.hasMetadata("sRPG.mob");
	}
	
	public static SRPGMob fromMob(LivingEntity entity) {
		if (!isMob(entity)) return null;
		SRPGMob mob = (SRPGMob) entity.getMetadata("sRPG.mob").get(0).value();
		return mob;
	}
	
	public static void setRPGMob(LivingEntity entity, SRPGMob mob, String name) {
		int health = mob.getMobType().getHealth(mob.getLevel());
		int damage = mob.getMobType().getDamage(mob.getLevel());
		
		entity.setMetadata("sRPG.mob", new FixedMetadataValue(MainSky2RPGCore.getMain(), mob));
		
		entity.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(health);
		entity.setHealth(health);
		entity.getAttribute(Attribute.GENERIC_ATTACK_DAMAGE).setBaseValue(damage);
		
		entity.setCustomNameVisible(true);
		entity.setCustomName(name.replace("&", "§"));
	}
	
	public static void setRPGFromName(LivingEntity entity, String sName) {
		String name = getName(sName);
		int level = getLevel(sName);
		SRPGMobType type = getType(sName);
		setRPGMob(entity, new SRPGMob(level, type), name);
	}
	
	// [Name]
	private static String getName(String s) {
		String regex = "\\[(?<name>.*)]";
		Pattern p = Pattern.compile(regex);
		Matcher matcher = p.matcher(s);
		while (matcher.find()) {
			return matcher.group("name");
		}
		return null;
	}
	
	// <Level>
	private static int getLevel(String s) {
		String regex = "<(?<name>.*)>";
		Pattern p = Pattern.compile(regex);
		Matcher matcher = p.matcher(s);
		while (matcher.find()) {
			return Integer.parseInt(matcher.group("name"));
		}
		return 0;
	}
	
	// %Type%
	private static SRPGMobType getType(String s) {
		String regex = "%(?<name>.*)%";
		Pattern p = Pattern.compile(regex);
		Matcher matcher = p.matcher(s);
		while (matcher.find()) {
			return SRPGMobType.valueOf(matcher.group("name").toUpperCase());
		}
		return SRPGMobType.THUONG;
	}
	
}
