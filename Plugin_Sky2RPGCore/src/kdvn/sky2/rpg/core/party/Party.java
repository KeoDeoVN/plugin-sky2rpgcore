package kdvn.sky2.rpg.core.party;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;

public class Party {
	
	public List<Player> players = new ArrayList<Player> ();
	public Player partyCreater = null;
	
	public Party(Player partyCreator) {
		this.partyCreater = partyCreator;
		this.players.add(partyCreator);
	}
	
	public Party(Player partyCreator, List<Player> players) {
		this.players = players;
	}
	
}
