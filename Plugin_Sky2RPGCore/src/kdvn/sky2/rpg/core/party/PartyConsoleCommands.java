package kdvn.sky2.rpg.core.party;



import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class PartyConsoleCommands implements CommandExecutor {

	PartyManager manager = PartyManager.get();
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String arg2, String[] args) {
		
		if (sender instanceof Player) return false;
		if (args.length == 0) return false;
		
		if (args[0].equalsIgnoreCase("request")) {
			Player player = Bukkit.getPlayer(args[1]);
			try {
				Player target = Bukkit.getPlayer(args[2]);
				manager.requestJoin(player, target);
			}
			catch (NullPointerException e) {
				player.sendMessage("§aKhông tìm thấy người chơi này!");
			}
		}
		else if (args[0].equalsIgnoreCase("create")) {
			Player player = Bukkit.getPlayer(args[1]);
			manager.createParty(player);
		}
		else if (args[0].equalsIgnoreCase("kick")) {
			Player player = Bukkit.getPlayer(args[1]);
			if (manager.getPartyFromMaster(player) == null) {
				Party party = manager.getPartyFromPlayer(player);
				manager.kick(party, player);
			}
			else {
				Party party = manager.getPartyFromMaster(player);
				manager.removeParty(party);
			}
		}
		else if (args[0].equalsIgnoreCase("info")) {
			Player player = Bukkit.getPlayer(args[1]);
			Party party = manager.getPartyFromPlayer(player);
			if (party != null) {
				player.sendMessage("§aChủ Party: §f" + party.partyCreater.getName());
				player.sendMessage("§2Thành viên:");
				for (int i = 1 ; i < party.players.size() + 1 ; i++) {
					Player p = party.players.get(i - 1);
					if (p != party.partyCreater) {
						player.sendMessage(" §6" + i + ". §e" + p.getName());
					}
				}
			}
		}
		
		return false;
	}

}
