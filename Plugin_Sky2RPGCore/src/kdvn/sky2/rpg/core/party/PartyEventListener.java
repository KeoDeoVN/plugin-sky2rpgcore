package kdvn.sky2.rpg.core.party;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PartyEventListener implements Listener {
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent e) {
		PartyGUI.eventListener(e);
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent e) {
		Player player = e.getPlayer();
		if (PartyManager.get().getPartyFromMaster(player) != null) {
			PartyManager.get().removeParty(player);
			return;
		}
		if (PartyManager.get().getPartyFromPlayer(player) != null) {
			Party party = PartyManager.get().getPartyFromPlayer(player);
			PartyManager.get().kick(party, player);
		}
	}
	
}
