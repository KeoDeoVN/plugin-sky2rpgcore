package kdvn.sky2.rpg.core.party;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import kdvn.sky2.rpg.core.main.MainSky2RPGCore;

public class PartyManager {
	
	private static PartyManager inst;
	private PartyManager() {}
	public static PartyManager get() {
		inst = inst == null ? new PartyManager() : inst;
		return inst;
	}
	
	public List<Party> parties = new ArrayList<Party> ();
	
	public Map<Party, Player> invitations = new HashMap<Party, Player> ();
	public Map<Player, Long> cooldownInvitation = new HashMap<Player, Long> ();
	
	public final int MAX_PLAYERS = 5;
	
	public void createParty(Player player) {
		Party party = new Party(player);
		this.parties.add(party);
		player.sendMessage("§aParty đã được lập!");
	}
	
	public void removeParty(Player player) {
		Party p = getPartyFromMaster(player);
		if (p != null) {
			removeParty(p);
		}
	}
	
	public void removeParty(Party party) {
		party.players.forEach(pl -> {
			pl.sendMessage("§aParty đã giải tán!");
			pl.sendTitle("", "§aParty đã giải tán", 0, 20, 0);
		});
		if (parties.contains(party)) {
			parties.remove(party);
		}
	}
	
	public Party getPartyFromPlayer(Player player) {
		for (Party p : parties) {
			if (p.players.contains(player)) return p;
		}
		return null;
	}
	
	public Party getPartyFromMaster(Player player) {
		for (Party p : parties) {
			if (p.partyCreater == player) return p;
		}
		return null;
	}
	
	public void requestJoin(Player sender, Player target) {
		Party party = getPartyFromMaster(target);
		if (party == null) {
			sender.sendMessage("§aNgười chơi không có Party");
			return;
		}
		if (party.players.size() >= MAX_PLAYERS) {
			sender.sendMessage("§aParty đã max player!");
			return;
		}
		requestJoin(sender, party);
	}
	
	public void requestJoin(Player sender, Party party) {
		if (cooldownInvitation.containsKey(sender)) {
			long time = System.currentTimeMillis() - cooldownInvitation.get(sender);
			if (time < 30 * 1000) {
				sender.sendMessage("§aĐợi " + new Double(30 - time / 1000) + " giây nữa để gửi!");
				return;
			}
		}
		cooldownInvitation.put(sender, System.currentTimeMillis());
		
		Player master = party.partyCreater;
		master.sendMessage("§f" + sender.getName() + " §amuốn tham gia vào party");
		master.sendMessage("§6/party dongy §ahoặc §c/party tuchoi");
		master.sendTitle("", "§aCó yêu cầu gia nhập Party", 10, 40, 10);
		
		invitations.put(party, sender);
		
		Bukkit.getScheduler().runTaskLaterAsynchronously(MainSky2RPGCore.getMain(), () -> {
			if (invitations.containsKey(party)) {
				Player p = invitations.get(party);
				if (p == sender) {
					refuseRequestJoin(master);
				}
			}
		}, 20 * 20);
		
	}
	
	public void refuseRequestJoin(Player player) {
		Party party = getPartyFromMaster(player);
		if (party != null) {
			if (invitations.containsKey(party)) {
				Player sender = invitations.get(party);
				sender.sendMessage("§cLời yêu cầu vào party bị từ chối!");
				player.sendMessage("§6Bạn đã từ chối lời mời vào party của " + sender.getName());
				invitations.remove(party);
			}
		}
	}
	
	public void acceptRequestJoin(Player player) {
		Party party = getPartyFromMaster(player);
		if (party != null) {
			if (invitations.containsKey(party)) {
				Player sender = invitations.get(party);
				sender.sendMessage("§cBạn đã được chấp nhận");
				player.sendMessage("§6Bạn đã chấp nhận");
				invitations.remove(party);
				party.players.add(sender);
			}
		}
	}
	
	public void kick(Player master, Player player) {
		Party party = getPartyFromMaster(player);
		kick(party, player);

	}
	
	public void kick(Party party, Player player) {
		if (party != null) {
			party.players.remove(player);
			player.sendMessage("§cBạn bị kick khỏi party");
			party.players.forEach(p -> {
				p.sendMessage("§6" + player.getName() + " bị kick khỏi party");
			});
		}
	}
	
	public List<Player> getNearByMembers(Player player, double radius) {
		Party party = getPartyFromPlayer(player);
		List<Player> players = new ArrayList<Player> ();
		if (party == null) return players;
		party.players.forEach(p -> {
			if (p == player) return;
			if (p.getWorld() == player.getWorld()) {
				if (p.getLocation().distance(player.getLocation()) <= radius) {
					players.add(p);
				}
			}
		});
		
		return players;
	}
	
	
	
}
