package kdvn.sky2.rpg.core.player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.entity.Player;

import kdvn.sky2.rpg.core.buff.SRPGExpBuff;
import kdvn.sky2.rpg.core.buff.SRPGStatBuff;
import kdvn.sky2.rpg.core.permbuff.SPermBuff;
import kdvn.sky2.rpg.core.potential.SRPGPotential;
import kdvn.sky2.rpg.core.stat.SRPGStat;
import mk.plugin.sky2rpgcore.element.SElement;

public class SRPGPlayer {
	
	private int level = 0;
	private Map<SRPGStat, Integer> itemStats = new HashMap<SRPGStat, Integer> ();
	private Map<SRPGPotential, Integer> potentials = new HashMap<SRPGPotential, Integer> ();
	private SElement element = null;
	private List<SRPGStatBuff> statBuffs = new ArrayList<SRPGStatBuff> ();
	private List<SRPGExpBuff> expBuffs = new ArrayList<SRPGExpBuff> ();
	
	public SRPGPlayer(int level, Map<SRPGStat, Integer> stats, Map<SRPGPotential, Integer> potentials, SElement element, List<SRPGStatBuff> statBuffs, List<SRPGExpBuff> expBuffs) {
		this.level = level;
		this.itemStats = stats == null ? this.itemStats : stats;
		this.potentials = potentials == null ? this.potentials : potentials;
		this.element = element;
		this.statBuffs = statBuffs == null ? this.statBuffs : statBuffs;
		this.expBuffs = expBuffs == null ? this.expBuffs : expBuffs;
	}
	
	// Getters
	
	public int getLevel() {
		return this.level;
	}
	
	public int getStat(Player player, SRPGStat stat) {
		int value = 0;
		
		// Item
		if (this.itemStats.containsKey(stat)) {
			value += this.itemStats.get(stat);
		}
		
		// Potential
		for (SRPGPotential p : this.potentials.keySet()) {
			int point = this.getPotential(p);
			if (p.getStats().containsKey(stat)) value += p.getStats().get(stat) * point;
		}
		
		// Buff
		for (SRPGStatBuff buff : this.statBuffs) {
			if (buff.getStat() == stat) {
				if (buff.isPercentBuff()) {
					value = value * (100 + buff.getValue()) / 100;
				} else value += buff.getValue();
			} 
		}
		
		// Permbuff
		for (SPermBuff buff : SPermBuff.permBuffs.values()) {
			if (player.hasPermission(buff.getPermission()) && buff.getBuffs().containsKey(stat)) {
				value = value * (100 + buff.getBuffs().get(stat)) / 100;
			}
		}
		
		// Element
		if (this.element != null) {
			if (this.element.getBuff().getStat() == stat) value = new Double(value * (double) (100 + this.element.getBuff().getValue()) / 100).intValue();
		}
		
		return value;
	}
	
	public Map<SRPGStat, Integer> getItemStats() {
		return this.itemStats;
	}
	
	public boolean hasElement() {
		return this.element != null;
	}
	
	public SElement getElement() {
		return this.element;
	}
	
	public List<SRPGStatBuff> getStatBuffs() {
		return this.statBuffs;
	}
	
	public List<SRPGExpBuff> getExpBuffs() {
		return this.expBuffs;
	}
	
	public int getExpBuff() {
		int i = 0;
		for (SRPGExpBuff buff : this.expBuffs) {
			i += buff.getValue();
		}
		return i;
	}
	
	public int getPotential(SRPGPotential potential) {
		if (this.potentials.containsKey(potential)) return this.potentials.get(potential);
		return 0;
	}
	
	public Map<SRPGPotential, Integer> getPotentials() {
		return this.potentials;
	}
	
	// Setters
	
	public void setItemStats(Map<SRPGStat, Integer> stats) {
		this.itemStats = stats;
	}
	
	public void setPotentials(Map<SRPGPotential, Integer> potentials) {
		this.potentials = potentials;
	}
	
	public void setPotential(SRPGPotential potential, int value) {
		this.potentials.put(potential, value);
	}
	
	public void setElement(SElement element) {
		this.element = element;
	}
	
	public void setStatBuffs(List<SRPGStatBuff> statBuffs) {
		this.statBuffs = statBuffs;
	}
	
	public void setExpBuffs(List<SRPGExpBuff> expBuffs) {
		this.expBuffs = expBuffs;
	}
	
	public void removeStatBuff(SRPGStatBuff buff) {
		this.statBuffs.remove(buff);
	}
	
	public void removeExpBuff(SRPGExpBuff buff) {
		this.expBuffs.remove(buff);
	}
	
	
}
