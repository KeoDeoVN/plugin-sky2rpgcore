package kdvn.sky2.rpg.core.player;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.entity.Player;

import kdvn.sky2.rpg.core.buff.SRPGExpBuff;
import kdvn.sky2.rpg.core.buff.SRPGStatBuff;
import kdvn.sky2.rpg.core.classes.SRPGClass;
import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.potential.SRPGPotential;
import kdvn.sky2.rpg.core.stat.SRPGStat;
import kdvn.sky2.rpg.core.storage.SRPGMySQL;
import mk.plugin.sky2rpgcore.element.SElement;

public class SRPGPlayerDatabase {
	
	private static SRPGMySQL storage = MainSky2RPGCore.getDatabase();
	
	public static void createTables() {
		Map<String, String> queries = new HashMap<String, String> ();
		queries.put("player", "create table player(PLAYER varchar(50) not null, LEVEL int(5), POTENTIAL varchar(1000), CLASS varchar(50), STAT_BUFF varchar(1000), EXP_BUFF varchar(1000), primary key (PLAYER))");
		queries.forEach((table, cmd) -> {
			try {
				if (storage.ifTableExist(table)) return;
				PreparedStatement ps = storage.getConnection().prepareStatement(cmd);
				ps.execute();
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}
	
	public static void initData(String playerName) {
		String query = "insert into player(PLAYER, LEVEL, POTENTIAL, CLASS, STAT_BUFF, EXP_BUFF) values(?, ?, ?, ?, ?, ?)";
		try {
			PreparedStatement ps = storage.getConnection().prepareStatement(query);
			ps.setString(1, playerName);
			ps.setInt(2, 0);
			ps.setString(3, "");
			ps.setString(4, "");
			ps.setString(5, "");
			ps.setString(6, "");
			ps.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void savePlayer(Player player) {
		SRPGPlayer rpgP = SRPGPlayerUtils.getData(player);
		String query = "update player set LEVEL=?, POTENTIAL=?, CLASS=?, STAT_BUFF=?, EXP_BUFF=? where PLAYER=?";
		try {
			PreparedStatement ps = storage.getConnection().prepareStatement(query);
			ps.setInt(1, player.getLevel());
			ps.setString(2, mapPotentialToString(rpgP.getPotentials()));
			if (rpgP.hasElement()) {
				ps.setString(3, rpgP.getElement().name());
			}
			else ps.setString(3, "");
			ps.setString(4, listStatBuffToString(rpgP.getStatBuffs()));
			ps.setString(5, listExpBuffToString(rpgP.getExpBuffs()));
			ps.setString(6, player.getName());
			ps.executeUpdate();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static boolean hasData(String name) {
		String syntax = "select * from player where PLAYER=?";
		try {
			PreparedStatement st = storage.getConnection().prepareStatement(syntax);
			st.setString(1, name);
			ResultSet rs = st.executeQuery();
			while (rs.next()) {
				return true;
			}
			return false;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		
	}
	
	public static SRPGPlayer getData(String name) {
		if (!hasData(name)) {
			initData(name);
		}
		// Init
		int level = 0;
		Map<SRPGStat, Integer> stats = new HashMap<SRPGStat, Integer> ();
		Map<SRPGPotential, Integer> potentials = new HashMap<SRPGPotential, Integer> ();
		SElement element = null;
		List<SRPGStatBuff> statBuffs = new ArrayList<SRPGStatBuff> ();
		List<SRPGExpBuff> expBuffs = new ArrayList<SRPGExpBuff> ();
		
		// Run query
		String query = "select * from player where PLAYER=?";
		PreparedStatement ps;
		try {
			ps = storage.getConnection().prepareStatement(query);
			ps.setString(1, name);
			ResultSet rs = ps.executeQuery();
			
			// Set value
			while (rs.next()) {
				level = rs.getInt("LEVEL");
				potentials = mapPotentialFromString(rs.getString("POTENTIAL"));
				
				// Element
				String cls = rs.getString("CLASS");
				if (cls.length() == 0) element = null;
				else {
					boolean isElement = false;
					for (SElement e : SElement.values()) 
						if (e.name().equals(cls)) {
							isElement = true;
							element = e;
							break;
						}
					
					if (!isElement) {
						SRPGClass c = SRPGClass.valueOf(cls);
						for (SElement e : SElement.values()) if (e.getInherit() == c) element = e;
					}
				}
				
				statBuffs = listStatBuffFromString(rs.getString("STAT_BUFF"));
				expBuffs = listExpBuffFromString(rs.getString("EXP_BUFF"));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		// Player
		SRPGPlayer rpgP = new SRPGPlayer(level, stats, potentials, element, statBuffs, expBuffs);
		return rpgP;
	}
	
	// Methods toString and fromString
	
	private static String mapPotentialToString(Map<SRPGPotential, Integer> map) {
		String result = "";
		if (map.size() == 0) return result;
		for (SRPGPotential e : map.keySet()) {
			result += e.name() + ":" + map.get(e) + ";";
		}
		result = result.substring(0, result.length() - 1);
		return result;
	}
	
	private static Map<SRPGPotential, Integer> mapPotentialFromString(String s) {
		Map<SRPGPotential, Integer> stats = new LinkedHashMap<SRPGPotential, Integer> ();
		if (s.length() == 0) return stats;
		String[] list = s.split(";");
		for (int i = 0 ; i < list.length ; i ++) {
			stats.put(SRPGPotential.valueOf(list[i].split(":")[0]), Integer.parseInt(list[i].split(":")[1]));
		}
		return stats;
	}
	
	private static String statBuffToString(SRPGStatBuff buff) {
		String s = "";
		s += buff.getStat() + ":" + buff.getValue() + ":" + buff.isPercentBuff() + ":" + buff.getStart() + ":" + buff.getValue();
		return s;
	}
	
	private static String listStatBuffToString(List<SRPGStatBuff> list) {
		String s = "";
		if (list.size() == 0) return s;
		for (SRPGStatBuff buff : list) {
			s += statBuffToString(buff) + ";";
		}
		return s.substring(0, s.length() - 1);
	}
	
	private static SRPGStatBuff statBuffFromString(String s) {
		if (s.length() == 0) return null;
		String[] list = s.split(":");
		SRPGStatBuff buff = new SRPGStatBuff(SRPGStat.valueOf(list[0]), Integer.valueOf(list[1]), Boolean.valueOf(list[2]), Long.valueOf(list[3]), Long.valueOf(list[4]));
		return buff;
	}
	
	private static List<SRPGStatBuff> listStatBuffFromString(String s) {
		List<SRPGStatBuff> buffs = new ArrayList<SRPGStatBuff> ();
		if (s.length() == 0) return buffs;
		String[] list = s.split(";");
		for (String buff : list) {
			buffs.add(statBuffFromString(buff));
		}
		return buffs;
	}
	
	private static String expBuffToString(SRPGExpBuff buff) {
		String s = "";
		s += buff.getValue() + ":" + buff.getStart() + ":" + buff.getValue();
		return s;
	}
	
	private static String listExpBuffToString(List<SRPGExpBuff> list) {
		String s = "";
		if (list.size() == 0) return s;
		for (SRPGExpBuff buff : list) {
			s += expBuffToString(buff) + ";";
		}
		return s.substring(0, s.length() - 1);
	}
	
	private static SRPGExpBuff expBuffFromString(String s) {
		String[] list = s.split(":");
		SRPGExpBuff buff = new SRPGExpBuff(Integer.valueOf(list[0]), Long.valueOf(list[1]), Long.valueOf(list[2]));
		return buff;
	}
	
	private static List<SRPGExpBuff> listExpBuffFromString(String s) {
		List<SRPGExpBuff> buffs = new ArrayList<SRPGExpBuff> ();
		if (s.length() == 0) return buffs;
		String[] list = s.split(";");
		for (String buff : list) {
			buffs.add(expBuffFromString(buff));
		}
		return buffs;
	}
	
}
