package kdvn.sky2.rpg.core.player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;

import kdvn.sky2.rpg.core.actionbar.SRPGActionBar;
import kdvn.sky2.rpg.core.buff.SRPGExpBuff;
import kdvn.sky2.rpg.core.buff.SRPGStatBuff;
import kdvn.sky2.rpg.core.item.SRPGItem;
import kdvn.sky2.rpg.core.item.SRPGItemUtils;
import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.mob.SRPGMob;
import kdvn.sky2.rpg.core.party.PartyManager;
import kdvn.sky2.rpg.core.senchant.SRPGEnchant;
import kdvn.sky2.rpg.core.stat.SRPGStat;
import kdvn.sky2.rpg.core.utils.SRPGUtils;
import mk.plugin.sky2rpgcore.element.SElement;
import net.minecraft.server.v1_13_R2.SoundEffects;

public class SRPGPlayerUtils {
	
	public static final int MAX_LEVEL = 100;
	
	private static Set<String> biPhanDon = new HashSet<String> ();
	
	private static Map<String, SRPGPlayer> data = new HashMap<String, SRPGPlayer> ();
	
	public static SRPGPlayer getData(Player player) {
		return getData(player.getName());
	}
	
	public static SRPGPlayer getData(String name) {
		if (data.containsKey(name)) {
			return data.get(name);
		}
		return null;
	}
	
	public static double getStatValue(Player player, SRPGStat stat) {
		SRPGPlayer rpgP = getData(player);
		if (rpgP == null) return 0;
		return stat.pointsToValue(rpgP.getStat(player, stat));
	}
	
	public static void buffStat(Player player, SRPGStatBuff buff) {
		SRPGPlayer rpgP = getData(player);
		if (rpgP == null) return;
		List<SRPGStatBuff> buffs = rpgP.getStatBuffs();
		
		// Trùng stat thì tăng thời gian
		boolean has = false;
		for (SRPGStatBuff b : buffs) {
			if (b.getStat() == buff.getStat()) {
				b.addTime(buff.getTime());
				has = true;
				break;
			}
		}
		if (!has) buffs.add(buff);
		
		rpgP.setStatBuffs(buffs);
	}
	
	public static void buffExp(Player player, SRPGExpBuff buff) {
		SRPGPlayer rpgP = getData(player);
		if (rpgP == null) return;
		List<SRPGExpBuff> buffs = rpgP.getExpBuffs();
		
		// Trùng % thì tăng thời gian
		boolean has = false;
		for (SRPGExpBuff b : buffs) {
			if (b.getValue() == buff.getValue()) {
				b.addTime(buff.getTime());
				has = true;
				break;
			}
		}
		if (!has) buffs.add(buff);
		
		rpgP.setExpBuffs(buffs);
	}
	
	public static void updatePlayer(Player player) {
		// Sync update
		Object mo = null;
		if (player.hasMetadata("updatePlayer_mo")) {
			mo = player.getMetadata("updatePlayer_mo").get(0).value();
		} else {
			mo = new Object();
			player.setMetadata("updatePlayer_mo", new FixedMetadataValue(MainSky2RPGCore.getMain(), mo));
		}
		
		synchronized (mo) {
			SRPGPlayer rpgP = getData(player);
			if (rpgP == null) return;
			
			// Item
			Map<SRPGStat, Integer> itemStats = new LinkedHashMap<SRPGStat, Integer> ();
			List<ItemStack> items = SRPGUtils.getItemsInPlayer(player);
			
			for (SRPGStat stat : SRPGStat.values()) {
				int itemStat = stat.getMinValue();
				for (ItemStack item : items) {
					if (!SRPGItemUtils.isRPGItem(item)) continue;
					SRPGItem rpgI = SRPGItemUtils.fromItemStack(item);
					if (rpgI == null) continue;
					itemStat += rpgI.getCalculatedStat(player, stat);
				}
				itemStats.put(stat, itemStat);
				stat.set(player, itemStat);
			}
			rpgP.setItemStats(itemStats);
			
			// Check stat buff
			List<SRPGStatBuff> statBuffs = new ArrayList<SRPGStatBuff> (rpgP.getStatBuffs());
			for (SRPGStatBuff buff : rpgP.getStatBuffs()) {
				if (!buff.isStillEffective()) statBuffs.remove(buff);
			}
			rpgP.setStatBuffs(statBuffs);
			
			// Check exp buff
			List<SRPGExpBuff> expBuffs = new ArrayList<SRPGExpBuff> (rpgP.getExpBuffs());
			for (SRPGExpBuff buff : rpgP.getExpBuffs()) {
				if (!buff.isStillEffective()) expBuffs.remove(buff);
			}
			rpgP.setExpBuffs(expBuffs);
		}
		
	}
	
	public static void playerQuit(Player player) {
		Bukkit.getScheduler().runTaskAsynchronously(MainSky2RPGCore.getMain(), () -> {
			SRPGPlayerDatabase.savePlayer(player);
			data.remove(player.getName());
			SRPGUtils.sendToConsole("§6[Sky2RPGCore] Saved data of " + player.getName());
		}); 
	}
	
	public static void playerJoin(Player player) {
		Bukkit.getScheduler().runTaskAsynchronously(MainSky2RPGCore.getMain(), () -> {
			SRPGPlayer rpgP = SRPGPlayerDatabase.getData(player.getName());
			data.put(player.getName(), rpgP);
			updatePlayer(player);
			SRPGUtils.sendToConsole("§a[Sky2RPGCore] Loaded data of " + player.getName());
			setTabPrefix(player);
		}); 
	}
	
	// 1 level = 1 point
	public static int getRemainPotentialPoint(Player player) {
		int lv = player.getLevel();
		SRPGPlayer rpgP = getData(player.getName());
		int has = 0;
		for (int i : rpgP.getPotentials().values()) has += i;
		return lv - has; 
	}
	
	public static void addExp(Player player, int exp) {
		int level = player.getLevel() + 1;
		// Set exp bar
		while (exp > 0) {
			if (level >= MAX_LEVEL) return;
			int toExp = getExpToLevel(level);
			float more = new Double((double) exp / toExp).floatValue();
			float afterExpRate = more + player.getExp();
			
			if (afterExpRate > 1) {
				exp = exp - new Double((toExp * (1 - player.getExp()))).intValue();
				player.setLevel(level);
				player.setExp(0);
				level++;
			} else {
				exp = 0;
				player.setExp(afterExpRate);
			}
		}
	}
	
	public static void gainExp(Player player, SRPGMob mob, double damage, double maxHealth) {
		// Has buff
		String color = (getData(player).getExpBuff() != 0) ? "§6" : "§a";
		// Add
		double exp = ((double) damage / maxHealth) * mob.getMobType().getExpBonus() * mob.getLevel();
		exp = exp * (100 + getData(player).getExpBuff()) / 100;
		int expInt = new Double(exp).intValue();
		addExp(player, expInt);
		
		// Effect
		SRPGActionBar.send(player, color + "+" + expInt + " exp");
		SRPGUtils.sendSound(player, SoundEffects.ENTITY_EXPERIENCE_ORB_PICKUP, 0.5f, 1f);
		
		// Party exp share
		for (Player p : PartyManager.get().getNearByMembers(player, 50)) {
			int expParty = expInt / 10;
			if (expParty != 0) {
				addExp(p, expParty);
			}
		}
	}
	
	public static void changeElement(Player player, SElement element) {
		SRPGPlayer rpgP = getData(player);
		rpgP.setElement(element);
		player.sendMessage("§2Bạn sở hữu hệ §r" + element.getColor() + element.getName());
		player.sendTitle(element.getColor() + "§l" + element.getName().toUpperCase(), "§7Bạn đã chọn hệ", 10, 40, 10);
		SRPGUtils.broadcast("§2[Thông báo] §7Người chơi §f" + player.getName() + " §7chọn hệ §r" + element.getColor() + element.getName());
	}
	
	public static void removeClass(Player player) {
		SRPGPlayer rpgP = getData(player);
		rpgP.setElement(null);
	}
	
	public static String getChatPrefix(Player player) {
		int lv = player.getLevel();
		
		// Get color
		String s = getLevelColorCode(lv);
		
		// Prefix
		SElement cls = getData(player).getElement();
		if (cls == null) {
			return "§f[" + s + "Lv." + lv + "§f]§r";
		}
		s = "§f[" + cls.getColor() + cls.getName() + " " + s + "" + lv + "§f]§r";
		
		// Sky2XacMinh
//		if (Bukkit.getPluginManager().isPluginEnabled("Sky2XacMinh")) {
//			s = XacMinhUtils.getPrefixChat(player) + s;
//		}
		
		return s;
	}
	
	public static String getTabPrefix(Player player) {
		int lv = player.getLevel();
		
		// Get color
		String s = getLevelColorCode(lv);
		
		// Prefix
		s = s + "[Lv." + lv + "]§r";
		
		// Sky2XacMinh
//		if (Bukkit.getPluginManager().isPluginEnabled("Sky2XacMinh")) {
//			s = XacMinhUtils.getPrefixTab(player) + s;
//		}
		
		return s;
	}
	
	public static void setTabPrefix(Player player) {
		String prefix = getTabPrefix(player);
		player.setPlayerListName(prefix + " " + player.getName());
		
	}
	
	public static String getLevelColorCode(int lv) {
		Map<Integer, String> lvColor = new HashMap<Integer, String> ();
		lvColor.put(0, "§9");
		lvColor.put(20, "§a");
		lvColor.put(40, "§6");
		lvColor.put(60, "§e");
		int max = 0;
		String s = "§9";
		for (int i : lvColor.keySet()) {
			if (lv >= i) {
				if (i > max) {
					max = i;
					s = lvColor.get(i);
				}
			}
		}
		return s;
	}
	
	// Kill 50 quai: 4 phut 30 giay
	// Lv.1 => Lv.10: Giết 10 con quái cùng level
	// Lv.11 => Lv.20: Giết 100 con quái cùng level
	// Lv.21 => Lv.30: Giết 500 con quái cùng level
	// Lv.31 => Lv.50: Giết 2000 con quái cùng level 
	// Lv.51 => Lv.70: Giết 10000 con quái cùng level 
	// Lv.71 => Lv.90: Giết 50000 con quái cùng level 
	// Lv.91 => Lv.100: Giết 500000 con quái cùng level 
	public static int getExpToLevel(int level) {
		if (1 <= level && level <= 10) return level * 10;
		if (11 <= level && level <= 20) return level * 100;
		if (21 <= level && level <= 30) return level * 1000;
		if (31 <= level && level <= 50) return level * 10000;
		if (51 <= level && level <= 70) return level * 100000;
		if (71 <= level && level <= 90) return level * 1000000;
		if (91 <= level && level <= 100) return level * 10000000;
		return 0;
	}
	
	public static void levelUpEffet(Player player, int oldLevel, int newLevel) {
		Bukkit.getScheduler().runTaskAsynchronously(MainSky2RPGCore.getMain(), () -> {
			player.sendTitle(" §6§lLÊN CẤP RỒI !!!", "§7+" + (newLevel - oldLevel) + " điểm tiềm năng", 1 * 20, 2 * 20, 1 * 20);
			SRPGUtils.sendSound(player, SoundEffects.ENTITY_FIREWORK_ROCKET_TWINKLE, 1, 1);
			player.getWorld().spawnParticle(Particle.FIREWORKS_SPARK, player.getLocation(), 50, 1, 1, 1, 1);
			setTabPrefix(player);
		});
	}
	
	public static int getEnchant(SRPGEnchant enchant, Player player) {
		int lv = 0;
		for (ItemStack item : SRPGUtils.getItemsInPlayer(player)) {
			if (SRPGItemUtils.isRPGItem(item)) {
				SRPGItem rpgI = SRPGItemUtils.fromItemStack(item);
				lv += rpgI.getEnchant(enchant);
			}
		}
		return lv;
	}
	
	public static void applyPhanDonPlayer(Player damager, Player target, double damage, int point) {
		if (point <= 0) return;
		if (biPhanDon.contains(target.getName())) {
			biPhanDon.remove(target.getName());
			return;
		}
		biPhanDon.add(damager.getName());
		SRPGUtils.damageWithoutEvent(target, damager, (double) damage * point / 100);
		
		Location loc = target.getLocation();
		loc.add(loc.getDirection().multiply(1.3f));
		SRPGUtils.hologram(SRPGUtils.randomLoc(loc, 1), "§c§lPHẢN ĐÒN", 15, target);
		SRPGUtils.hologram(SRPGUtils.randomLoc(loc, 1), "§2§lPHẢN ĐÒN", 15, damager);
	}
	
	public static void applyPhanDonEntity(Player damaged, LivingEntity target, double damage, int point) {
		if (point <= 0) return;
		SRPGUtils.damageWithoutEvent(damaged, target, (double) damage * point / 100);
		Location loc = damaged.getLocation();
		loc.add(loc.getDirection().multiply(1.3f));
		SRPGUtils.hologram(SRPGUtils.randomLoc(loc, 1), "§c§lPHẢN ĐÒN", 15, damaged);
	}
	
}
