package kdvn.sky2.rpg.core.potential;

import java.util.HashMap;
import java.util.Map;

import kdvn.sky2.rpg.core.stat.SRPGStat;

public enum SRPGPotential {
	
	SUC_MANH("Sức mạnh") {
		@Override
		public Map<SRPGStat, Integer> getStats() {
			Map<SRPGStat, Integer> map = new HashMap<SRPGStat, Integer> ();
			map.put(SRPGStat.SAT_THUONG, 1);
			
			return map;
		}
	},
	THE_LUC("Thể lực") {
		@Override
		public Map<SRPGStat, Integer> getStats() {
			Map<SRPGStat, Integer> map = new HashMap<SRPGStat, Integer> ();
			map.put(SRPGStat.MAU, 3);
			map.put(SRPGStat.HOI_PHUC, 1);
			
			return map;
		}
	},
	NHANH_NHEN("Khéo léo") {
		@Override
		public Map<SRPGStat, Integer> getStats() {
			Map<SRPGStat, Integer> map = new HashMap<SRPGStat, Integer> ();
			map.put(SRPGStat.NE, 1);
			map.put(SRPGStat.CHI_MANG, 1);
			
			return map;
		}
	};
	
	public abstract Map<SRPGStat, Integer> getStats();
	
	private String name;
	
	private SRPGPotential(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
}
