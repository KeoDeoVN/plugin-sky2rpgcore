package kdvn.sky2.rpg.core.power;

import org.bukkit.entity.Player;

import me.clip.placeholderapi.expansion.PlaceholderExpansion;

public class SRPGPowerPlaceholder extends PlaceholderExpansion {

	@Override
	public String getAuthor() {
		return "MankaiStep";
	}

	@Override
	public String getIdentifier() {
		return "sky2";
	}

	@Override
	public String getVersion() {
		return "1.0";
	}
	
	@Override
    public String onPlaceholderRequest(Player player, String s){
		if (s.equalsIgnoreCase("player_power")) {
			return SRPGPowerUtils.calculatePower(player) + "";
		}
		return "";
	}

}
