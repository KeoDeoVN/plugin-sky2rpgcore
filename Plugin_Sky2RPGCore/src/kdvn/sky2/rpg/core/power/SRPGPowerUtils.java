package kdvn.sky2.rpg.core.power;

import org.bukkit.entity.Player;

import kdvn.sky2.rpg.core.player.SRPGPlayer;
import kdvn.sky2.rpg.core.player.SRPGPlayerUtils;
import kdvn.sky2.rpg.core.stat.SRPGStat;

public class SRPGPowerUtils {
	
	public static int calculatePower(Player player) {
		SRPGPlayer rpgP = SRPGPlayerUtils.getData(player);
		int power = 0; 
		if (rpgP == null) return power;
		for (SRPGStat stat : SRPGStat.values()) {
			power += rpgP.getStat(player, stat) * getPower(stat);
		}
		
		return power;
	}
	
	public static int getPower(SRPGStat rpgStat) {
		int power = 1;
		switch (rpgStat) {
			case SAT_THUONG: power = 6; break;
			case MAU: power = 6; break;
			case SUC_THU: power = 6; break;
			case NE: power = 6; break;
			case HOI_PHUC: power = 6; break;
			case HUT_MAU: power = 5; break;
			case CHI_MANG: power = 6; break;
			case PHAN_DON: power = 6; break;
			case XUYEN_GIAP: power = 6; break;
		default:
			power = 6;
			break;
		}
		
		return power * 54;
	}
	
}
