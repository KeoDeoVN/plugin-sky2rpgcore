package kdvn.sky2.rpg.core.price;

import org.bukkit.entity.Player;

import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import net.milkbowl.vault.economy.Economy;

public class MoneyAPI {
	
	public static double getMoney(Player player) {
		Economy eco = MainSky2RPGCore.getEcononomy();
		
		return eco.getBalance(player);
	}

	public static boolean moneyCost(Player player, double money) {
		Economy eco = MainSky2RPGCore.getEcononomy();
		double moneyOfPlayer = eco.getBalance(player);
		if (moneyOfPlayer < money) {
			return false;
		}
		eco.withdrawPlayer(player, money);
		return true;
		
	}
	
	public static void giveMoney(Player player, double money) {
		Economy eco = MainSky2RPGCore.getEcononomy();
		eco.depositPlayer(player, money);
	}
	
}
