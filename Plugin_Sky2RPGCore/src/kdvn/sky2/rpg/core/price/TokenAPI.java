package kdvn.sky2.rpg.core.price;

import java.util.OptionalLong;

import org.bukkit.entity.Player;

import kdvn.sky2.rpg.core.main.MainSky2RPGCore;

public class TokenAPI {
	
	public static boolean tokenCost(Player player, int value) {
		OptionalLong l = MainSky2RPGCore.getTokenManager().getTokens(player);
		if (l.getAsLong() < value) return false;
		MainSky2RPGCore.tokenM.setTokens(player, MainSky2RPGCore.getTokenManager().getTokens(player).getAsLong() - value);
		return true;
	}
	
	public static void give(Player player, int value) {
		MainSky2RPGCore.getTokenManager().setTokens(player, MainSky2RPGCore.getTokenManager().getTokens(player).getAsLong() + value);
	}
	
}
