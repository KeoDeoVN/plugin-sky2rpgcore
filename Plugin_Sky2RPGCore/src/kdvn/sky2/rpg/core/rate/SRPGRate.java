package kdvn.sky2.rpg.core.rate;

import mk.plugin.sky2rpgcore.tier.Tier;

public enum SRPGRate {
	
	THO(Tier.SO_CAP),	
	THUONG(Tier.TRUNG_CAP),	
	TRUNG(Tier.CAO_CAP),	
	CAO(Tier.CUC_PHAM),
	CUC_PHAM(Tier.HUYEN_THOAI);
	
	private Tier tier;
	
	
	private SRPGRate(Tier tier) {
		this.tier = tier;
	}
	
	public Tier getTier() {
		return this.tier;
	}
	
}
