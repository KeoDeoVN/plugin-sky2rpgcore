package kdvn.sky2.rpg.core.sachmayman;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import kdvn.sky2.rpg.core.utils.ItemStackUtils;

public enum SRPGSachMayMan {
	
	SACH_0("§7§lSách an toàn", 0, Material.BOOK),
	SACH_25("§9§lSách may mắn 25%", 25, Material.BOOK),
	SACH_50("§c§lSách may mắn 50%", 50, Material.BOOK),
	SACH_75("§6§lSách may mắn 75%", 75, Material.BOOK),
	SACH_100("§3§lSách may mắn 100%", 100, Material.BOOK);
	
	private String name;
	private int bonus;
	private Material material;
	
	private SRPGSachMayMan(String name, int bonus, Material material) {
		this.name = name;
		this.bonus = bonus;
		this.material = material;
	}
	
	public String getName() {
		return this.name;
	}
	
	public int getBonus() {
		return this.bonus;
	}
	
	public Material getMaterial() {
		return this.material;
	}
	
	public ItemStack getItem() {
		ItemStack item = new ItemStack(this.getMaterial());
		ItemStackUtils.setDisplayName(item, this.getName());
		ItemStackUtils.addEnchantEffect(item);
		item = ItemStackUtils.setTag(item, "sRPG.sachmayman", this.name());
		
		return item;
	}
	
	public static boolean isThatItem(ItemStack item) {
		return ItemStackUtils.hasTag(item, "sRPG.sachmayman");
	}
	
	public static SRPGSachMayMan fromItem(ItemStack item) {
		if (!isThatItem(item)) return null;
		return SRPGSachMayMan.valueOf(ItemStackUtils.getTag(item, "sRPG.sachmayman"));
	}
	
}
