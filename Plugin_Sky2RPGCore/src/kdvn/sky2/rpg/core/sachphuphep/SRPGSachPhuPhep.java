package kdvn.sky2.rpg.core.sachphuphep;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import kdvn.sky2.rpg.core.senchant.SRPGEnchant;
import kdvn.sky2.rpg.core.utils.ItemStackUtils;

public class SRPGSachPhuPhep {
	
	private SRPGEnchant enchant;
	
	public SRPGSachPhuPhep(SRPGEnchant enchant) {
		this.enchant = enchant;
	}
	
	public SRPGEnchant getEnchant() {
		return this.enchant;
	}
	
	public ItemStack getItem() {
		ItemStack item = new ItemStack(Material.ENCHANTED_BOOK);
		ItemStackUtils.setDisplayName(item, "§d§lSách phù phép §c§l" + enchant.getName());
		item = ItemStackUtils.setTag(item, "sRPG.sachphuphep", enchant.name());
		
		return item;
	}
	
	public static boolean isThatItem(ItemStack item) {
		return ItemStackUtils.hasTag(item, "sRPG.sachphuphep");
	}
	
	public static SRPGSachPhuPhep fromItem(ItemStack item) {
		if (!isThatItem(item)) return null;
		return new SRPGSachPhuPhep(SRPGEnchant.valueOf(ItemStackUtils.getTag(item, "sRPG.sachphuphep")));
	}
	
}
