package kdvn.sky2.rpg.core.senchant;

import kdvn.sky2.rpg.core.skill.ISRPGSkill;
import kdvn.sky2.rpg.core.skills.SRPGSkillChoang;
import kdvn.sky2.rpg.core.skills.SRPGSkillGiapLua;
import kdvn.sky2.rpg.core.skills.SRPGSkillKhoHeo;
import kdvn.sky2.rpg.core.skills.SRPGSkillMu;
import kdvn.sky2.rpg.core.skills.SRPGSkillNhanhChan;
import kdvn.sky2.rpg.core.skills.SRPGSkillTenNo;

public enum SRPGEnchant {
	
	GIAP_LUA("Giáp lửa", new SRPGSkillGiapLua()),
	KHO_HEO("Khô héo", new SRPGSkillKhoHeo()),
	MU("Mù", new SRPGSkillMu()),
	NHANH_CHAN("Nhanh chân", new SRPGSkillNhanhChan()),
	TEN_NO("Tên nổ", new SRPGSkillTenNo()),
	CHOANG("Choáng", new SRPGSkillChoang());
	
	private String name;
	private ISRPGSkill skill;
	
	private SRPGEnchant() {}
	private SRPGEnchant(String name, ISRPGSkill skill) {
		this.name = name;
		this.skill = skill;
	}
	
	public String getName() {
		return this.name;
	}
	
	public ISRPGSkill getISkill() {
		return this.skill;
	}
	
}
