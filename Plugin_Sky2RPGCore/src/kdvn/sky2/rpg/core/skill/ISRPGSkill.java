package kdvn.sky2.rpg.core.skill;

import org.bukkit.entity.Player;

public interface ISRPGSkill {
	
	public String getDesc(int lv);
	public boolean hasCombo();
	public void start(Player player, int lv);
	
}
