package kdvn.sky2.rpg.core.skill;

import kdvn.sky2.rpg.core.skills.SRPGSkillBaoKiem;
import kdvn.sky2.rpg.core.skills.SRPGSkillCuongPhong;
import kdvn.sky2.rpg.core.skills.SRPGSkillDiaChan;
import kdvn.sky2.rpg.core.skills.SRPGSkillDocTien;
import kdvn.sky2.rpg.core.skills.SRPGSkillGiangDon;
import kdvn.sky2.rpg.core.skills.SRPGSkillHoiPhuc;
import kdvn.sky2.rpg.core.skills.SRPGSkillKienCuong;
import kdvn.sky2.rpg.core.skills.SRPGSkillMuaTien;
import kdvn.sky2.rpg.core.skills.SRPGSkillNoiCong;
import kdvn.sky2.rpg.core.skills.SRPGSkillQuetKiem;
import kdvn.sky2.rpg.core.skills.SRPGSkillRiuSet;
import kdvn.sky2.rpg.core.skills.SRPGSkillSatQuy;
import kdvn.sky2.rpg.core.skills.SRPGSkillTanTheRagnarok;
import kdvn.sky2.rpg.core.skills.SRPGSkillTanXaTien;
import kdvn.sky2.rpg.core.skills.SRPGSkillTrungPhat;
import kdvn.sky2.rpg.core.skills.SRPGSkillXongPha;
import kdvn.sky2.rpg.core.utils.MinecraftItemType;

public enum SRPGSkill {
	
	SAT_QUY("Sát quỷ", new SRPGSkillSatQuy(), SRPGSkillType.TYPE_1),
	BAO_KIEM("Bão kiếm", new SRPGSkillBaoKiem(), SRPGSkillType.TYPE_2, MinecraftItemType.SWORD),
	QUET_KIEM("Quét kiếm", new SRPGSkillQuetKiem(), SRPGSkillType.TYPE_3, MinecraftItemType.SWORD),
	CUONG_PHONG("Cuồng phong", new SRPGSkillCuongPhong(), SRPGSkillType.TYPE_4, MinecraftItemType.SWORD),
	
	KIEN_CUONG("Kiên cường", new SRPGSkillKienCuong(), SRPGSkillType.TYPE_1),
	DIA_CHAN("Địa chấn", new SRPGSkillDiaChan(), SRPGSkillType.TYPE_2),
	NOI_CONG("Nội công", new SRPGSkillNoiCong(), SRPGSkillType.TYPE_3),
	TRUNG_PHAT("Trừng phạt", new SRPGSkillTrungPhat(), SRPGSkillType.TYPE_4),
	
	DOC_TIEN("Độc tiễn", new SRPGSkillDocTien(), SRPGSkillType.TYPE_1),
	TAN_XA_TIEN("Tán xạ tiễn", new SRPGSkillTanXaTien(), SRPGSkillType.TYPE_2, MinecraftItemType.BOW),
	HOI_PHUC("Hồi phục", new SRPGSkillHoiPhuc(), SRPGSkillType.TYPE_3),
	MUA_TIEN("Mưa tiễn", new SRPGSkillMuaTien(), SRPGSkillType.TYPE_4, MinecraftItemType.BOW),
	
	GIANG_DON("Giáng đòn", new SRPGSkillGiangDon(), SRPGSkillType.TYPE_1),
	RIU_SET("Rìu sét", new SRPGSkillRiuSet(), SRPGSkillType.TYPE_2, MinecraftItemType.AXE),
	XONG_PHA("Xông pha", new SRPGSkillXongPha(), SRPGSkillType.TYPE_3),
	TAN_THE_RAGNAROK("Tận thế Ragnarok", new SRPGSkillTanTheRagnarok(), SRPGSkillType.TYPE_4);
	
	private String name;
	private ISRPGSkill skill;
	private SRPGSkillType type;
	private MinecraftItemType itemRequired;
	
	private SRPGSkill() {}
	
	private SRPGSkill(String name, ISRPGSkill skill, SRPGSkillType type) {
		this.skill = skill;
		this.type = type;
		this.name = name;
	}
	
	private SRPGSkill(String name, ISRPGSkill skill, SRPGSkillType type, MinecraftItemType itemRequired) {
		this.skill = skill;
		this.type = type;
		this.name = name;
		this.itemRequired = itemRequired;
	}
	
	public String getName() {
		return this.name;
	}
	
	public ISRPGSkill getISkill() {
		return this.skill;
	}
	
	public SRPGSkillType getSkillType() {
		return this.type;
	}
	
	public MinecraftItemType getItemRequired() {
		return this.itemRequired;
	}
	
	public boolean requiredItem()  {
		return itemRequired != null;
	}
	
}
