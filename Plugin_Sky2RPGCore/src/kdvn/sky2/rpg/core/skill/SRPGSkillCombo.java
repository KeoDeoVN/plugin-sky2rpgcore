package kdvn.sky2.rpg.core.skill;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Maps;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.flags.Flags;
import com.sk89q.worldguard.protection.managers.RegionManager;

import kdvn.sky2.rpg.core.actionbar.SRPGActionBar;
import mk.plugin.sky2rpgcore.weapon.Weapon;
import mk.plugin.sky2rpgcore.weapon.WeaponUtils;

public class SRPGSkillCombo {
	
	private static HashMap<Player, List<Integer>> combo = new HashMap<Player, List<Integer>> ();
	
	public static boolean canCombo(Player player) {
		World world = player.getWorld();
		RegionManager rm = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(world));
		ApplicableRegionSet ar = rm.getApplicableRegions(BukkitAdapter.asBlockVector(player.getLocation()));
		boolean can = false;
		if (ar.testState(null, Flags.MOB_SPAWNING) || ar.testState(null, Flags.PVP))  {
			can = true;
		}
		
		return can;
	}
	
//	public static void comboExcute(Player player) {
//		SRPGPlayer rpgP = SRPGPlayerUtils.getData(player);
//		SRPGClass cls = rpgP.getSClass();
//		if (cls == null) return;
//		List<SRPGSkill> skillList = cls.getSkills();
//		for (SRPGSkill skill : skillList) {
//			if (!skill.getISkill().hasCombo()) {
//				continue;
//			}
//			if (balancingCombo(player, skill.getSkillType().getCombo())) {
//				if (!canCombo(player)) {
//					player.sendMessage("§cKhông thể thực thi §a" + skill.getName());
//					player.sendMessage("§cRa khỏi khu an toàn để combo!");
//					return;
//				}
//				
//				int point = cls.getLevel();
//				if (point == 0) return;
//				if (!SRPGSkillUtils.canExecute(player, skill)) return;
//				
//				if (SRPGSkillUtils.isDelayed(player, skill)) {
//					player.sendMessage("§c" + skill.getName() + " §6chưa hổi xong, còn " + SRPGSkillUtils.getTimeRemain(player, skill) + "s");
//					return;
//				}
//				if (skill.getItemRequired() != null) {
//					MinecraftItemType itemType = skill.getItemRequired();
//					if (!itemType.listMaterial.contains(player.getInventory().getItemInMainHand().getType())) {
//						player.sendMessage("§cYêu cầu cầm " + itemType.name + " khi dùng kỹ năng");
//						return;
//					}
//				}
//
//				skill.getISkill().start(player, point);
//				player.sendMessage("§6Thực thi kỹ năng: §e" + skill.getName());
//				SRPGSkillUtils.delay(player, skill);
//				return;
//			}
//		}
//	}
	
	private static Map<Player, Long> delay = Maps.newHashMap();
	
	public static void comboExcute(Player player) {
		if (balancingCombo(player, new int[] {0})) {
			if (!canCombo(player)) {
				player.sendMessage("§cKhông thể thực thi kỹ năng");
				player.sendMessage("§cRa khỏi khu an toàn để combo!");
				return;
			}
			
			ItemStack item = player.getInventory().getItemInMainHand();
			if (!WeaponUtils.isWeapon(item)) return;
			
			if (delay.containsKey(player)) {
				if (delay.get(player) > System.currentTimeMillis()) {
					player.sendMessage("§cKỹ năng chưa hồi xong");
					player.sendMessage("§cCòn §6" + ((delay.get(player) - System.currentTimeMillis()) / 1000 + 1) + "s"); 
					return;
				}
			}
			delay.put(player, System.currentTimeMillis() + 5000);
			
			Weapon w = WeaponUtils.fromItemStack(item);
			ISRPGSkill skill = w.getType().getSkill();
			
			player.sendTitle("", ChatColor.RED + "" + ChatColor.BOLD + "[" + w.getType().getSkillName() + "]", 0, 20, 0);
			skill.start(player, 1);
			
		}
	}
	
	public static int getTimeOfClick(Player player) {
		if (!combo.containsKey(player)) {
			return 0;
		}
		return combo.get(player).size();
	}

	public static void addOne(Player player, int i) {
		if (combo.containsKey(player)) {
			List<Integer> list = combo.get(player);
			list.add(i);
			combo.put(player, list);
		}
		else {
			List<Integer> list = new ArrayList<Integer> ();
			list.add(i);
			combo.put(player, list);
		}
	}
	
	public static void finishCombo(Player player) {
		combo.remove(player);
		SRPGActionBar.send(player, "");
	}
	
	public static boolean balancingCombo(Player player, int[] i) {
		if (!combo.containsKey(player)) {
			return false;
		}
		
		if (combo.get(player).size() != i.length) {
			return false;
		}
		boolean check = true;
		for (int i2 = 0 ; i2 < combo.get(player).size() ; i2 ++) {
			if (i[i2] != combo.get(player).get(i2)) {
				check = false;
				break;
			}
		}
		return check;
	}
	
	public static void sendTitleCombo(Player player) {
		List<String> comboList = new ArrayList<String> ();
		for (int i : combo.get(player)) {
			if (i == 0) {
				comboList.add("§a§lTrái§f");
			}
			else comboList.add("§a§lPhải§f");
		}
		String mess = "§c§lShift + §r";
		for (String s : comboList) {
			mess += " + " + s;
		}
		mess += " + ";
		mess = "§f" + mess;
		SRPGActionBar.send(player, mess);
		
	}
	

	private static List<Player> isCombo = new ArrayList<Player> ();
	
	public static boolean isPlayerComboed(Player player) {
		if (isCombo.contains(player)) {
			return true;
		} else return false;
	}
	
	public static void addCombo(Player player) {
		if (!isPlayerComboed(player)) {
			isCombo.add(player);
		}
	}
	
	public static void huyCombo(Player player) {
		if (isPlayerComboed(player)) {
			isCombo.remove(player);
		}
		finishCombo(player);
	}
	
}
