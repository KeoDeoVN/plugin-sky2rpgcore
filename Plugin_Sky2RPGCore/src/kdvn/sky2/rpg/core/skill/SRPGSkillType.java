package kdvn.sky2.rpg.core.skill;

public enum SRPGSkillType {
	
	TYPE_1(new int[]{0}, 0, 0),
	TYPE_2(new int[]{0,0}, 4, 10),
	TYPE_3(new int[]{0,0,0}, 8, 20),
	TYPE_4(new int[]{0,0,0,0}, 20, 30);
	
	private int cooldown = 0;
	private int[] combo;
	private int level;
	
	private SRPGSkillType(int[] combo, int cooldown, int level) {
		this.combo = combo;
		this.cooldown = cooldown;
		this.level = level;
	}

	public int getCooldown() {
		return cooldown;
	}

	public int[] getCombo() {
		return combo;
	}

	public int getLevel() {
		return level;
	}
	
}
