package kdvn.sky2.rpg.core.skill;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.entity.Player;

public class SRPGSkillUtils {
	
	private static Map<Player, Map<SRPGSkill, Long>> delayed = new  HashMap<Player, Map<SRPGSkill, Long>> ();
	
	public static String comboToString(int[] cb) {
		String combo = "";
		for (int i = 0 ; i < cb.length ; i++) {
			String s = cb[i] == 0 ? "T" : "P";
			combo += s;
		}

		String result = combo.replaceAll("T", "Trái ").replaceAll("P", "Phải ");
		if (combo.equalsIgnoreCase("")) return "Không cần";
		return result;
	}
	
//	public static List<SRPGSkill> getAvailableSkills(Player player) {
//		SRPGPlayer rpgP = SRPGPlayerUtils.getData(player);
//		SRPGClass cls = rpgP.getSClass();
//		List<SRPGSkill> skills = new ArrayList<SRPGSkill> ();
//		if (cls == null) return skills;
//		cls.getSkills().forEach(skill -> {
//			if (player.getLevel() >= skill.getSkillType().getLevel()) skills.add(skill);
//		});
//		return skills;
//	}
	
//	public static boolean canExecute(Player player, SRPGSkill skill) {
//		return getAvailableSkills(player).contains(skill);
//	}
	
	public static int getTimeRemain(Player player, SRPGSkill skill) {
		if (isDelayed(player, skill)) {
			return skill.getSkillType().getCooldown() - new Double((System.currentTimeMillis() - delayed.get(player).get(skill)) / 1000  * 100).intValue() / 100;
		}
		return 0;
	}
	
	public static boolean isDelayed(Player player, SRPGSkill skill) {
		if (delayed.containsKey(player)) {
			Map<SRPGSkill, Long> map = delayed.get(player);
			if (map.containsKey(skill)) {
				long time = map.get(skill);
				if (System.currentTimeMillis() - time >= skill.getSkillType().getCooldown() * 1000) {
					map.remove(skill);
					delayed.put(player, map);
					return false;
				}
				else return true;
			}
		}
		
		return false;
	}
	
	public static void delay(Player player, SRPGSkill skill) {
		Map<SRPGSkill, Long> map = new HashMap<SRPGSkill, Long>  ();
		if (delayed.containsKey(player)) {
			map = delayed.get(player);
		}
		map.put(skill, System.currentTimeMillis());
		delayed.put(player, map);
	}
	
}
