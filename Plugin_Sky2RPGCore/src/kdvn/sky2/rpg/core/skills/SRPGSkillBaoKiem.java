package kdvn.sky2.rpg.core.skills;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.player.SRPGPlayerUtils;
import kdvn.sky2.rpg.core.skill.ISRPGSkill;
import kdvn.sky2.rpg.core.stat.SRPGStat;
import kdvn.sky2.rpg.core.utils.SRPGUtils;
import net.minecraft.server.v1_13_R2.SoundEffects;

public class SRPGSkillBaoKiem implements ISRPGSkill {
	
	private static Map<String, Integer> times = new HashMap<String, Integer> ();
	
	@Override
	public String getDesc(int lv) {
		return "Chọc kiếm về trước, lần 3 lốc xoáy gây " + (100 + 20 * lv) + "% dame" ;
	}

	@Override
	public boolean hasCombo() {
		return true;
	}

	@Override
	public void start(Player player, int lv) {
		if (!times.containsKey(player.getName())) {
			times.put(player.getName(), 1);
			kiem(player, lv);
		}
		else if (times.get(player.getName()) == 1) {
			times.put(player.getName(), 2);
			kiem(player, lv);
			long a = System.currentTimeMillis();
			new BukkitRunnable() {
				@Override
				public void run() {
					if (!times.containsKey(player.getName()) || times.get(player.getName()) == 1) {
						this.cancel();
						return;
					}
					if (System.currentTimeMillis() - a >= 10 * 1000) {
						times.remove(player.getName());
						this.cancel();
						return;
					}
					player.playSound(player.getLocation(), Sound.ENTITY_GHAST_SHOOT, 0.5f, 1.5f);
					player.getWorld().spawnParticle(Particle.SWEEP_ATTACK, player.getLocation(), 1, 0, 0, 0, 1);
					player.getWorld().spawnParticle(Particle.SWEEP_ATTACK, player.getLocation().add(0, 0.5, 0), 1, 0, 0, 0, 1);
					player.getWorld().spawnParticle(Particle.SWEEP_ATTACK, player.getLocation().add(0, 1.5, 0), 1, 0, 0, 0, 1);
					player.getWorld().spawnParticle(Particle.SWEEP_ATTACK, player.getLocation().add(0, 2, 0), 1, 0, 0, 0, 1);
				}
			}.runTaskTimerAsynchronously(MainSky2RPGCore.getMain(), 0, 7);
		}
		else if (times.get(player.getName()) == 2) {
			times.remove(player.getName());
			loc(player, lv);
		}
	}
	
	public void kiem(Player player, int lv) {
		double damage =	SRPGPlayerUtils.getStatValue(player, SRPGStat.SAT_THUONG) * 0.2 * lv; 
		Bukkit.getScheduler().runTaskAsynchronously(MainSky2RPGCore.getMain(), () -> {
			Location mainLoc = player.getLocation().add(0, 0.9, 0);
			SRPGUtils.sendSound(player, SoundEffects.ENTITY_PLAYER_ATTACK_SWEEP, 1, 1);
			for (int i = 0 ; i < 6; i ++) {
				Location loc =  mainLoc.clone().add(mainLoc.getDirection().multiply(i));
				player.getWorld().spawnParticle(Particle.SPIT, loc, 3, 0, 0, 0, 0);
				loc.getWorld().getNearbyEntities(loc, 1, 1, 1).forEach(e -> {
					if (e instanceof LivingEntity && e != player) {
						Bukkit.getScheduler().runTask(MainSky2RPGCore.getMain(), () -> {
							SRPGUtils.damage((LivingEntity) e, player, damage, 5, MainSky2RPGCore.getMain());
						});
					}
				});
			}
		});
	}
	
	public void loc(Player player, int lv) {
		double damage =	SRPGPlayerUtils.getStatValue(player, SRPGStat.SAT_THUONG) * 0.2 * lv;  
		Location location = player.getLocation();
		new BukkitRunnable() {
			int i = 0;
			@Override
			public void run() {
				i++;
				if (i >= 15) {
					this.cancel();
					return;
				}
				Location newLocation = location.clone().add(location.getDirection().multiply(i * 1.8));
				player.playSound(newLocation, Sound.ENTITY_GHAST_SHOOT, 0.5f, 1.5f);

				int i = 0;
				List<Location> list = getListLocation(newLocation);
				for (i = 0 ; i < list.size() ; i ++) {
					player.getWorld().spawnParticle(Particle.SWEEP_ATTACK, list.get(i), new Double(i / 3).intValue(), i*0.05f, i*0.05f, i*0.05f, i*0.01f);
				}
				for (Entity e : location.getWorld().getNearbyEntities(newLocation, 2, 5, 2)) {
					if (e instanceof LivingEntity && e != player) {
						new BukkitRunnable() {
							@Override
							public void run() {
								if (e.hasMetadata("NPC")) return;
								SRPGUtils.damage((LivingEntity) e, player, damage, 5, MainSky2RPGCore.getMain());
								e.setVelocity(new Vector(0, 0.7, 0));
							}
						}.runTask(MainSky2RPGCore.getMain());
					}
				}
			}
		}.runTaskTimerAsynchronously(MainSky2RPGCore.getMain(), 0, 2);
	}
	
	private List<Location> getListLocation(Location main) {
		List<Location> list = new ArrayList<Location> ();
		Location newLocation = main.clone().add(0, -2, 0);
		for (int i = 1 ; i < 15; i ++) {
			list.add(newLocation.clone().add(0, 0.5f * i, 0));
		}
		return list;
	}
	
}
