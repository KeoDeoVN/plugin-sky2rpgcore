package kdvn.sky2.rpg.core.skills;

import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import kdvn.sky2.rpg.core.skill.ISRPGSkill;

public class SRPGSkillChoang implements ISRPGSkill {

	@Override
	public String getDesc(int lv) {
		return "Gây choáng kẻ địch khi tấn công trong " + 0.5 * lv + " giây";
	}

	@Override
	public boolean hasCombo() {
		return false;
	}

	@Override
	public void start(Player player, int lv) {
		
	}
	
	public void doIt(Player target, int lv) {
		target.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, lv * 20 / 2, 2));
	}

	
	
}
