package kdvn.sky2.rpg.core.skills;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.player.SRPGPlayerUtils;
import kdvn.sky2.rpg.core.skill.ISRPGSkill;
import kdvn.sky2.rpg.core.stat.SRPGStat;
import kdvn.sky2.rpg.core.utils.SRPGUtils;

public class SRPGSkillCuongPhong implements ISRPGSkill {

	@Override
	public String getDesc(int lv) {
		return "Chém ra " + (1 + lv * 2) + " cơn lốc về trước, gây " + (100 + 20 * lv) + "% dame";
	}

	@Override
	public boolean hasCombo() {
		return true;
	}

	@Override
	public void start(Player player, int lv) {
		int amount = lv == 1 ? 3 : 5;
		
		double angleBetweenArrows = (92 / (amount - 1)) * Math.PI / 180;
		double pitch = (player.getLocation().getPitch() + 90) * Math.PI / 180;
		double yaw = (player.getLocation().getYaw() + 90 - 92 / 2) * Math.PI / 180;
		double sZ = Math.cos(pitch);

		for (int i = 0; i < amount; i++) { 	
			double nX = Math.sin(pitch)	* Math.cos(yaw + angleBetweenArrows * i);
			double nY = Math.sin(pitch)* Math.sin(yaw + angleBetweenArrows * i);
			Vector newDir = new Vector(nX, sZ, nY);

			double damage =	SRPGPlayerUtils.getStatValue(player, SRPGStat.SAT_THUONG) * 0.2 * lv;
			
			loc(player, damage, newDir);
		}
	}
	
	public void loc(Player player, double damage, Vector v) {
		Location location = player.getLocation();
		new BukkitRunnable() {
			int i = 0;
			@Override
			public void run() {
				i++;
				if (i >= 15) {
					this.cancel();
					return;
				}
				Location newLocation = location.clone().add(v.normalize().multiply(i * 1.8));
				player.playSound(newLocation, Sound.ENTITY_GHAST_SHOOT, 0.5f, 1.5f);

				int i = 0;
				List<Location> list = getListLocation(newLocation);
				for (i = 0 ; i < list.size() ; i ++) {
//					ParticleAPI.sendParticle(EnumParticle.SWEEP_ATTACK, list.get(i), i*0.05f, i*0.05f, i*0.05f, i*0.01f, new Double(i / 3).intValue());
					player.getWorld().spawnParticle(Particle.SWEEP_ATTACK, list.get(i), new Double(i / 3).intValue(), i*0.05f, i*0.05f, i*0.05f, i*0.01f);
				}
				for (Entity e : location.getWorld().getNearbyEntities(newLocation, 2, 5, 2)) {
					if (e instanceof LivingEntity && e != player) {
						new BukkitRunnable() {
							@Override
							public void run() {
								if (e.hasMetadata("NPC")) return;
								SRPGUtils.damage((LivingEntity) e, player, damage, 20, MainSky2RPGCore.getMain());
								e.setVelocity(new Vector(0, 0.7, 0));
							}
						}.runTask(MainSky2RPGCore.getMain());
					}
				}
			}
		}.runTaskTimerAsynchronously(MainSky2RPGCore.getMain(), 0, 2);
	}
	
	private List<Location> getListLocation(Location main) {
		List<Location> list = new ArrayList<Location> ();
		Location newLocation = main.clone().add(0, -2, 0);
		for (int i = 1 ; i < 15; i ++) {
			list.add(newLocation.clone().add(0, 0.5f * i, 0));
		}
		return list;
	}
}
