package kdvn.sky2.rpg.core.skills;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.player.SRPGPlayerUtils;
import kdvn.sky2.rpg.core.skill.ISRPGSkill;
import kdvn.sky2.rpg.core.stat.SRPGStat;
import kdvn.sky2.rpg.core.utils.SRPGUtils;

public class SRPGSkillDiaChan implements ISRPGSkill {
	
	@Override
	public String getDesc(int lv) {
		return "Rung động mặt đất, địa chấn gây " + (100 + 10 * lv) + "% dame mỗi giây";
	}

	@Override
	public boolean hasCombo() {
		return true;
	}

	@Override
	public void start(Player player, int lv) {
		double damage =	SRPGPlayerUtils.getStatValue(player, SRPGStat.SAT_THUONG) * 0.1 * lv; 
		
		new BukkitRunnable() {
			int i = 0;
			@Override
			public void run() {
				i++;
				if (i >= 6) {
					this.cancel();
					return;
				}
				
				// Get location
				Location temp = player.getLocation().clone();
				int k = 0;
				while (temp.getBlock().getType() == Material.AIR) {
					k++;
					if (k > 10) {
						this.cancel();
						break;
					}
					temp = temp.add(0,-1,0);
				}
				
				// Radius = 3	
				for (int j = 1 ; j <= 6 ; j++) {
					SRPGUtils.createCircle(Particle.BLOCK_CRACK, temp.clone().add(0, 1.5, 0), j * 0.5, temp.clone().add(0, -1, 0).getBlock().getBlockData());
//					SRPGUtils.createCircle(Particle.LAVA, location, j * 0.5);
					SRPGUtils.createCircle(Particle.CRIT, temp.clone().add(0, 1.5, 0), j * 0.5);
				}
				player.getWorld().playSound(temp, Sound.ENTITY_GENERIC_EXPLODE, 0.2f, 0.2f);

				
				for (Entity e : temp.getWorld().getNearbyEntities(temp, 3, 3, 3)) {
					if (e instanceof LivingEntity && e != player) {
						new BukkitRunnable() {
							@Override
							public void run() {	
								if (e.hasMetadata("NPC")) return;
								SRPGUtils.damage((LivingEntity) e, player, damage, 5, MainSky2RPGCore.getMain());
								e.setVelocity(new Vector(0, 0.5, 0));
							}
						}.runTask(MainSky2RPGCore.getMain());
					}
				}
			}
		}.runTaskTimerAsynchronously(MainSky2RPGCore.getMain(), 0, 20);
	}
	
}
