package kdvn.sky2.rpg.core.skills;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import kdvn.sky2.rpg.core.skill.ISRPGSkill;

public class SRPGSkillDocTien implements ISRPGSkill {

	@Override
	public String getDesc(int lv) {
		return "Gây độc cấp " + lv + " cho kẻ trúng tên";
	}

	@Override
	public boolean hasCombo() {
		return false;
	}

	@Override
	public void start(Player player, int lv) {
		
	}
	
	public void doIt(LivingEntity e, int lv) {
		e.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 2 * lv * 20, lv));
	}
}
