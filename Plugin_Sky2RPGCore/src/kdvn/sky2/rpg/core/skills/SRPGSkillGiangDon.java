package kdvn.sky2.rpg.core.skills;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import kdvn.sky2.rpg.core.skill.ISRPGSkill;

public class SRPGSkillGiangDon implements ISRPGSkill {
	
	@Override
	public String getDesc(int lv) {
		return "Làm chậm kẻ địch cấp độ " + lv + " khi tấn công";
	}

	@Override
	public boolean hasCombo() {
		return false;
	}

	@Override
	public void start(Player player, int lv) {	
	}
	
	public void doIt(LivingEntity e, int lv) {
		e.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 2 * lv * 20, lv));
	}

	
}
