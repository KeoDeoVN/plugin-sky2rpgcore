package kdvn.sky2.rpg.core.skills;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import kdvn.sky2.rpg.core.skill.ISRPGSkill;

public class SRPGSkillGiapLua implements ISRPGSkill {

	@Override
	public String getDesc(int lv) {
		return "Gây cháy kẻ tấn công bạn trong " + lv + " giây";
	}

	@Override
	public boolean hasCombo() {
		return false;
	}

	@Override
	public void start(Player player, int lv) {
	}
	
	public void ignite(LivingEntity damager, int lv) {
		damager.setFireTicks(lv * 20);
	}

}
