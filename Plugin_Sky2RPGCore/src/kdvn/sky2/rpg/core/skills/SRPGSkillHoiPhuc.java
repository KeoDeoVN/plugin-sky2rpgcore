package kdvn.sky2.rpg.core.skills;

import org.bukkit.entity.Player;

import kdvn.sky2.rpg.core.party.PartyManager;
import kdvn.sky2.rpg.core.player.SRPGPlayerUtils;
import kdvn.sky2.rpg.core.skill.ISRPGSkill;
import kdvn.sky2.rpg.core.stat.SRPGStat;
import kdvn.sky2.rpg.core.utils.SRPGUtils;

public class SRPGSkillHoiPhuc implements ISRPGSkill {
	

	@Override
	public String getDesc(int lv) {
		return "Hồi phục " + (lv * 20) + "% máu cho thành viên party bán kính 50 blocks";
	}

	@Override
	public boolean hasCombo() {
		return true;
	}

	@Override
	public void start(Player player, int lv) {
		PartyManager.get().getNearByMembers(player, 50).forEach(p -> {
			double maxHealth = SRPGPlayerUtils.getStatValue(p, SRPGStat.MAU);
			SRPGUtils.addHealth(player, maxHealth * 0.2 * lv);
		});
			
		
		
	}

	
}
