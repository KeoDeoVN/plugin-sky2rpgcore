package kdvn.sky2.rpg.core.skills;

import org.bukkit.entity.Player;

import kdvn.sky2.rpg.core.skill.ISRPGSkill;

public class SRPGSkillKienCuong implements ISRPGSkill {
	
	@Override
	public String getDesc(int lv) {
		return "Giảm " + (10 * lv) + "% sát thương nhận vào";
	}

	@Override
	public boolean hasCombo() {
		return false;
	}

	@Override
	public void start(Player player, int lv) {
		
	}
	
	public double change(double damage, int lv) {
		return damage * (1 - 0.1 * lv);
	}
}
