package kdvn.sky2.rpg.core.skills;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import kdvn.sky2.rpg.core.skill.ISRPGSkill;

public class SRPGSkillMu implements ISRPGSkill {
	
	@Override
	public String getDesc(int lv) {
		return "Gây mù cho kẻ bạn tấn công trong " + lv + " giây";
	}

	@Override
	public boolean hasCombo() {
		return false;
	}

	@Override
	public void start(Player player, int lv) {
	}
	
	public void doIt(LivingEntity entity, int lv) {
		entity.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 20 * lv, 1));
	}


	
}
