package kdvn.sky2.rpg.core.skills;

import java.util.Random;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.block.Block;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.skill.ISRPGSkill;
import kdvn.sky2.rpg.core.utils.SRPGUtils;

public class SRPGSkillMuaTien implements ISRPGSkill {

	@Override
	public String getDesc(int lv) {
		return "Mưa tên ở khu chỉ định trong " + (5 + 5 * lv) + " giây";
	}

	@Override
	public boolean hasCombo() {
		return true;
	}

	@Override
	public void start(Player player, int lv) {
		Block block = player.getTargetBlock((Set<Material>) null, 100);
		if (block == null || block.getType() == Material.AIR) {
			player.sendMessage("§cKhoảng cách quá xa");
			return;
		}
		Location loc = block.getLocation();
		
		int times = 5 + 5 * lv;
		
		// Circle
		new BukkitRunnable() {
			int i = 0;
			@Override
			public void run() {
				i++;
				if (i >= times + 1) this.cancel();
				SRPGUtils.createCircle(Particle.FLAME, loc.clone().add(0, 1.5, 0), 5);
			}
		}.runTaskTimerAsynchronously(MainSky2RPGCore.getMain(), 0, 20);
		
		// Arrow
		new BukkitRunnable() {
			int i = 0;
			@Override
			public void run() {
				i++;
				if (i >= times * 1.5) this.cancel();
				for (int i = 0 ; i < 5 ; i++) {
					Location main = loc.clone().add(0, 40, 0);
					double y = main.getY() + (double) (new Random().nextInt(10000) - 5000) / 1000;
					double x = main.getX() + (double) (new Random().nextInt(10000) - 5000) / 1500;
					double z = main.getZ() + (double) (new Random().nextInt(10000) - 5000) / 1500;
					main.setX(x);
					main.setZ(z);
					main.setY(y);
					Arrow a = player.getWorld().spawnArrow(main, new Vector(0, -10, 0), 5, 5);
					a.setShooter(player);
					a.setCritical(true);
					
					Bukkit.getScheduler().runTaskLater(MainSky2RPGCore.getMain(), () -> {
						a.remove();
					}, 20);
				}
			}
		}.runTaskTimer(MainSky2RPGCore.getMain(), 0, 7);
		
		
		
	}

}
