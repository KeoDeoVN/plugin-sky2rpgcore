package kdvn.sky2.rpg.core.skills;

import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import kdvn.sky2.rpg.core.skill.ISRPGSkill;

public class SRPGSkillNhanhChan implements ISRPGSkill {
	
	@Override
	public String getDesc(int lv) {
		return "Tăng tốc khi tấn công trong " + (double) lv / 2 + " giây";
	}

	@Override
	public boolean hasCombo() {
		return false;
	}

	@Override
	public void start(Player player, int lv) {
		player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 10 * lv, 1));
	}
	
}
