package kdvn.sky2.rpg.core.skills;

import org.bukkit.entity.Player;

import kdvn.sky2.rpg.core.skill.ISRPGSkill;

public class SRPGSkillNhayDoi implements ISRPGSkill {

	@Override
	public String getDesc(int lv) {
		return "Nhảy đôi hơn";
	}

	@Override
	public boolean hasCombo() {
		return false;
	}

	@Override
	public void start(Player player, int lv) {
		player.setVelocity(player.getVelocity().multiply(2).setY(1));
	}

}
