package kdvn.sky2.rpg.core.skills;

import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.player.SRPGPlayerUtils;
import kdvn.sky2.rpg.core.skill.ISRPGSkill;
import kdvn.sky2.rpg.core.stat.SRPGStat;
import kdvn.sky2.rpg.core.utils.SRPGUtils;

public class SRPGSkillNoiCong implements ISRPGSkill {
	
	@Override
	public String getDesc(int lv) {
		return "Dùng nội công tạo vụ nổ, gây " + (100 + 20 * lv) + "% dame";
	}

	@Override
	public boolean hasCombo() {
		return true;
	}

	@Override
	public void start(Player player, int lv) {
		Location loc = player.getLocation();
		double damage =	SRPGPlayerUtils.getStatValue(player, SRPGStat.SAT_THUONG) * 0.2 * lv; 
		
		player.playSound(player.getLocation(), Sound.ENTITY_GENERIC_EXPLODE, 1, 1);
		player.getWorld().spawnParticle(Particle.EXPLOSION_HUGE, loc, 1, 0, 0, 0, 0);
		
		for (Entity e : loc.getWorld().getNearbyEntities(loc, 5, 5, 5)) {
			if (e instanceof LivingEntity && e != player) {
				Vector v = e.getLocation().subtract(player.getLocation()).toVector().normalize().add(new Vector(0, 1, 0));
				e.setVelocity(v);
				
				new BukkitRunnable() {
					@Override
					public void run () {
						if (e.hasMetadata("NPC")) return;
						SRPGUtils.damage((LivingEntity) e, player, damage, 5, MainSky2RPGCore.getMain());
					}
				}.runTask(MainSky2RPGCore.getMain());
				
			}
		}
	}
	
}
