package kdvn.sky2.rpg.core.skills;

import org.bukkit.Bukkit;
import org.bukkit.Particle;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.player.SRPGPlayerUtils;
import kdvn.sky2.rpg.core.skill.ISRPGSkill;
import kdvn.sky2.rpg.core.stat.SRPGStat;
import kdvn.sky2.rpg.core.utils.SRPGUtils;
import net.minecraft.server.v1_13_R2.SoundEffects;

public class SRPGSkillQuetKiem implements ISRPGSkill {

	@Override
	public String getDesc(int lv) {
		return "Chém xung quanh " + (100 + 20 * lv) + "% dame" ;
	}

	@Override
	public boolean hasCombo() {
		return true;
	}

	@Override
	public void start(Player player, int lv) {
		double damage =	SRPGPlayerUtils.getStatValue(player, SRPGStat.SAT_THUONG) * 0.2 * lv; 
		player.getLocation().getWorld().getNearbyEntities(player.getLocation(), 5, 5, 5).forEach(e -> {
			if (e instanceof LivingEntity && e != player) {
				Bukkit.getScheduler().runTask(MainSky2RPGCore.getMain(), () -> {
					SRPGUtils.damage((LivingEntity) e, player, damage, 5, MainSky2RPGCore.getMain());	
				});

				Bukkit.getScheduler().runTaskAsynchronously(MainSky2RPGCore.getMain(), () -> {
					player.getWorld().spawnParticle(Particle.SWEEP_ATTACK, e.getLocation().add(0, 0.5, 0), 0, 0, 0, 0, 10);
					SRPGUtils.sendSound(player, SoundEffects.ENTITY_PLAYER_ATTACK_SWEEP, 1, 1);
				});
			}
		});
	}
}
