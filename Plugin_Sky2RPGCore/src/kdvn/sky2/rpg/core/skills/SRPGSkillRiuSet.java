package kdvn.sky2.rpg.core.skills;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.player.SRPGPlayerUtils;
import kdvn.sky2.rpg.core.skill.ISRPGSkill;
import kdvn.sky2.rpg.core.stat.SRPGStat;
import kdvn.sky2.rpg.core.utils.SRPGUtils;

public class SRPGSkillRiuSet implements ISRPGSkill {
	

	@Override
	public String getDesc(int lv) {
		return "Gọi sét xuống, gây " + (100 + 20 * lv) + "% dame";
	}

	@Override
	public boolean hasCombo() {
		return true;
	}

	@Override
	public void start(Player player, int lv) {
		double damage =	SRPGPlayerUtils.getStatValue(player, SRPGStat.SAT_THUONG) * 0.2 * lv;
		
		Location location = player.getLocation();
		Location temp = location.clone().add(location.getDirection().multiply(5).setY(0));
		Bukkit.getScheduler().runTaskAsynchronously(MainSky2RPGCore.getMain(), () -> {
			temp.getWorld().strikeLightningEffect(temp);
			temp.getWorld().strikeLightningEffect(temp);
		});
		Bukkit.getScheduler().runTask(MainSky2RPGCore.getMain(), ()  -> {
			for (Entity e : temp.getWorld().getNearbyEntities(temp, 2, 10, 2)) {
				if (e instanceof LivingEntity && e != player) {
					SRPGUtils.damage((LivingEntity) e, player, damage, 20, MainSky2RPGCore.getMain());
				}
			}
		});
	}

	
}
