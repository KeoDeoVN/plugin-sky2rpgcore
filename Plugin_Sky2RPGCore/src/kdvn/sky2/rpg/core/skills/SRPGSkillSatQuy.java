package kdvn.sky2.rpg.core.skills;

import org.bukkit.entity.Player;

import kdvn.sky2.rpg.core.skill.ISRPGSkill;

public class SRPGSkillSatQuy implements ISRPGSkill {

	@Override
	public String getDesc(int lv) {
		return "Gây " + (100 + 20 * lv) + "% dame khi tấn công quái";
	}

	@Override
	public boolean hasCombo() {
		return false;
	}

	@Override
	public void start(Player player, int lv) {
		
	}
	
	public double changeDamage(double damage, int lv) {
		return damage * (1 + 0.2 * lv); 
	}
	
	
	
}
