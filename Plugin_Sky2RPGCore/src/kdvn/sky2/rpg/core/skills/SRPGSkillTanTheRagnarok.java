package kdvn.sky2.rpg.core.skills;

import org.bukkit.Bukkit;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import kdvn.sky2.rpg.core.buff.SRPGStatBuff;
import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.player.SRPGPlayerUtils;
import kdvn.sky2.rpg.core.skill.ISRPGSkill;
import kdvn.sky2.rpg.core.stat.SRPGStat;

public class SRPGSkillTanTheRagnarok implements ISRPGSkill {

	@Override
	public String getDesc(int lv) {
		return "Tăng 50% tất cả chỉ số trong " + (2 + 5 * lv) + " giây";
	}

	@Override
	public boolean hasCombo() {
		return true;
	}

	@Override
	public void start(Player player, int lv) {
		player.playSound(player.getLocation(), Sound.ENTITY_DRAGON_FIREBALL_EXPLODE, 1, 1);
		int time = (2 + 5 * lv);
		for (SRPGStat stat : SRPGStat.values()) {
			SRPGPlayerUtils.buffStat(player, new SRPGStatBuff(stat, 50, true, System.currentTimeMillis(), time * 1000));
		}
		long a = System.currentTimeMillis();
		new BukkitRunnable() {
			@Override
			public void run() {
				if (System.currentTimeMillis() - a >= time * 1000) this.cancel();
				player.getWorld().spawnParticle(Particle.FLAME, player.getLocation().add(0, 1, 0), 20, 0.5f, 0.5f, 0.5f, 0);
			}
		}.runTaskTimerAsynchronously(MainSky2RPGCore.getMain(), 0, 4);
		Bukkit.getScheduler().runTaskLaterAsynchronously(MainSky2RPGCore.getMain(), () -> {
			SRPGPlayerUtils.updatePlayer(player);
		}, (time + 2) * 20);
	}
	
}
