package kdvn.sky2.rpg.core.skills;

import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import kdvn.sky2.rpg.core.skill.ISRPGSkill;
import kdvn.sky2.rpg.core.utils.SRPGUtils;

public class SRPGSkillTenNo implements ISRPGSkill {

	@Override
	public String getDesc(int lv) {
		return "Tỉ lệ gây nổ diện rộng khi trúng";
	}

	@Override
	public boolean hasCombo() {
		return false;
	}

	@Override
	public void start(Player player, int lv) {
		// TODO Auto-generated method stub
		
	}
	
	public void doIt(LivingEntity target, int lv) {
		if (target.hasMetadata("NPC")) return;
		if (!SRPGUtils.rate(50)) return;
		double radius = 1.5 * 2;
		target.setVelocity(new Vector(0, 0.5, 0));
		Location loc = target.getLocation();
		loc.getWorld().spawnParticle(Particle.EXPLOSION_LARGE, loc, 1, 1, 1, 1 ,1);
		loc.getWorld().playSound(loc, Sound.ENTITY_GENERIC_EXPLODE, 0.8f, 0.8f);
		target.getNearbyEntities(radius, radius, radius).forEach(entity -> {
			if (entity instanceof LivingEntity) {
				if (entity.hasMetadata("NPC")) return;
				if (entity != target) {
					Vector v = entity.getLocation().subtract(loc).toVector().normalize().setY(0.5);
					entity.setVelocity(v);
				}
			}
		});
	}
	
	
	
}
