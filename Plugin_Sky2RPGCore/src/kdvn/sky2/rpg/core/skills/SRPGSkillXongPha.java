package kdvn.sky2.rpg.core.skills;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.player.SRPGPlayerUtils;
import kdvn.sky2.rpg.core.skill.ISRPGSkill;
import kdvn.sky2.rpg.core.stat.SRPGStat;
import kdvn.sky2.rpg.core.utils.SRPGUtils;

public class SRPGSkillXongPha implements ISRPGSkill {
	

	@Override
	public String getDesc(int lv) {
		return "Phi đến và hất tung mục tiêu, gây " + (100 + 20 * lv) + "% dame";
	}

	@Override
	public boolean hasCombo() {
		return true;
	}

	@Override
	public void start(Player player, int lv) {
		double damage =	SRPGPlayerUtils.getStatValue(player, SRPGStat.SAT_THUONG) * 0.2 * lv; 
		player.setVelocity(player.getLocation().getDirection().multiply(3).setY(0.2f));
		
		new BukkitRunnable() {
			int i = 0;
			@Override
			public void run() {
				i++;
				if (i >= 30) this.cancel();
				List<LivingEntity> list = new ArrayList<LivingEntity> ();
				for (Entity e : player.getWorld().getNearbyEntities(player.getLocation(), 1, 1, 1)) {
					if (e instanceof LivingEntity && e != player) list.add((LivingEntity) e);
				}
				
				new BukkitRunnable () {
					@Override
					public void run() {
						for (LivingEntity e : list) {
							if (e.hasMetadata("NPC")) continue;
							e.setVelocity(e.getLocation().getDirection().multiply(0.01).add(new Vector(0, 0.7, 0)));
							SRPGUtils.damage(e, player, damage, 5, MainSky2RPGCore.getMain());
						}
					}
				}.runTask(MainSky2RPGCore.getMain());
				
			}
		}.runTaskTimerAsynchronously(MainSky2RPGCore.getMain(), 0, 1);
	}
	
}
