package kdvn.sky2.rpg.core.storage;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import kdvn.sky2.rpg.core.main.MainSky2RPGCore;

public class SRPGMySQL {
	
	private Connection connection;
	
	public SRPGMySQL(String host, String port, String name, String user, String password) {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			String url = "jdbc:mysql://" + host + ":" + port + "/" + name + "?autoReconnect=true";
			this.connection = DriverManager.getConnection(url, user, password);
			MainSky2RPGCore.getMain().getLogger().info("Successful database access");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public Connection getConnection() {
		return this.connection;
	}
	
	public boolean ifTableExist(String name) {
		try {
			ResultSet tables = this.connection.getMetaData().getTables(null, null, name, null);
			if (tables.next()) return true;
			return false;
		} catch (SQLException e) {
			return false;
		}
	}
	
}
