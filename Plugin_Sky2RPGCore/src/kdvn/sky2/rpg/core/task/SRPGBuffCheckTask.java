package kdvn.sky2.rpg.core.task;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

import kdvn.sky2.rpg.core.buff.SRPGExpBuff;
import kdvn.sky2.rpg.core.buff.SRPGStatBuff;
import kdvn.sky2.rpg.core.player.SRPGPlayer;
import kdvn.sky2.rpg.core.player.SRPGPlayerUtils;

public class SRPGBuffCheckTask extends BukkitRunnable  {

	@Override
	public void run() {
		Bukkit.getOnlinePlayers().forEach(player -> {
			SRPGPlayer rpgP = SRPGPlayerUtils.getData(player);
			if (rpgP == null) return;
			List<SRPGStatBuff> statBuffs = new ArrayList<SRPGStatBuff> (rpgP.getStatBuffs());
			statBuffs.forEach(buff -> {
				if (!buff.isStillEffective()) {
					rpgP.removeStatBuff(buff);
					player.sendMessage("§aMột buff chỉ số của bạn đã hết hạn!");
				}
			});
			List<SRPGExpBuff> expBuffs = new ArrayList<SRPGExpBuff> (rpgP.getExpBuffs());
			expBuffs.forEach(buff -> {
				if (!buff.isStillEffective()) {
					rpgP.removeExpBuff(buff);
					player.sendMessage("§aMột kinh nghiệm chỉ số của bạn đã hết hạn!");
				}
			});
		});
	}

}
