package kdvn.sky2.rpg.core.task;

import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

import kdvn.sky2.rpg.core.player.SRPGPlayerUtils;
import kdvn.sky2.rpg.core.stat.SRPGStat;
import kdvn.sky2.rpg.core.utils.SRPGUtils;

public class SRPGHealTask extends BukkitRunnable {

	@Override
	public void run() {
		Bukkit.getOnlinePlayers().forEach(player -> {
			if (player.isDead()) return;
			double value = SRPGPlayerUtils.getStatValue(player, SRPGStat.HOI_PHUC);
			SRPGUtils.addHealth(player, value);
		});
	}

}
