package kdvn.sky2.rpg.core.thuochoiphuc;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.flags.Flags;
import com.sk89q.worldguard.protection.managers.RegionManager;

import kdvn.sky2.rpg.core.utils.ItemStackUtils;

public enum SRPGThuocHoiPhuc {
	
	THUONG("Thường", 5),
	TRUNG("Trung", 10),
	CAO("Cao", 20);
	
	private int health;
	private String name;
	
	private SRPGThuocHoiPhuc(String name, int health) {
		this.name = name;
		this.health = health;
	}
	
	public int getHealth() {
		return this.health;
	}
	
	public String getName() {
		return this.name;
	}
	
	public ItemStack createItem() {
		ItemStack item = new ItemStack(Material.DRAGON_BREATH);
		ItemStackUtils.setDisplayName(item, "§a§lThuốc hồi phục §6§l" + this.name);
		ItemStackUtils.addLoreLine(item, "§f+ " + this.health + " HP");
		return ItemStackUtils.setTag(item, "sRPG.thuochoiphuc", this.name());
	}
	
	public static boolean isThatItem(ItemStack item) {
		return ItemStackUtils.hasTag(item, "sRPG.thuochoiphuc");
	}
	
	public static SRPGThuocHoiPhuc fromItem(ItemStack item) {
		if (!isThatItem(item)) return null;
		return SRPGThuocHoiPhuc.valueOf(ItemStackUtils.getTag(item, "sRPG.thuochoiphuc"));
	}
	
	public static boolean canUse(Location location) {
		RegionManager rm = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(location.getWorld()));
		ApplicableRegionSet ar = rm.getApplicableRegions(BukkitAdapter.asBlockVector(location));
		if (ar.testState(null, Flags.PVP)) return false;
		return true;
	}
	
}
