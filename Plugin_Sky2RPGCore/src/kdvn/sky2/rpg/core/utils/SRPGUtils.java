package kdvn.sky2.rpg.core.utils;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.attribute.Attribute;
import org.bukkit.block.data.BlockData;
import org.bukkit.craftbukkit.v1_13_R2.CraftWorld;
import org.bukkit.craftbukkit.v1_13_R2.entity.CraftPlayer;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import mk.plugin.sky2rpgcore.jewelry.util.JewelryUtils;
import net.minecraft.server.v1_13_R2.ChatMessage;
import net.minecraft.server.v1_13_R2.EntityArmorStand;
import net.minecraft.server.v1_13_R2.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_13_R2.PacketPlayOutNamedSoundEffect;
import net.minecraft.server.v1_13_R2.PacketPlayOutSpawnEntityLiving;
import net.minecraft.server.v1_13_R2.SoundCategory;
import net.minecraft.server.v1_13_R2.SoundEffect;

public class SRPGUtils {
	
	public static Map<LivingEntity, Long> damaged = new HashMap<LivingEntity, Long> ();
	
	public static String getOreName(Material material) {
		switch (material) {
		case IRON_INGOT:
			return "§f§lSắt hiếm";
		case GOLD_INGOT:
			return "§e§lVàng hiếm";
		case DIAMOND:
			return "§b§lKim cương hiếm";
		case EMERALD:
			return "§a§lLục bảo hiếm";
		case COAL:
			return "§7§lThan hiếm";
		case REDSTONE:
			return "§c§lĐá đỏ hiếm";
		case LAPIS_LAZULI:
			return "§9§lLưu ly hiếm";
		default:
			return "§7§l" + chuHoaDau(material.name().replace("_", " ") + " hiếm");
		}
	}
	
	public static List<LivingEntity> getLivingEntities(Player player, Location location, double x, double y, double z) {
		List<LivingEntity> list = Lists.newArrayList();
		location.getWorld().getNearbyEntities(location, 5, 5, 5).stream().filter(e -> e instanceof LivingEntity && e != player).collect(Collectors.toList()).forEach(e -> {
			list.add((LivingEntity) e);
		});;
		return list;
	}
	
	public static void damageWithoutEvent(Player player, LivingEntity entity,  double damage) {
		double currentHealth = entity.getHealth();
		if (currentHealth <= damage) {
			entity.damage(currentHealth, player);
		}
		else {
			currentHealth -= damage;
			entity.setHealth(currentHealth);
			entity.damage(0);
		}
	}
	
	
	public static ItemStack getBlackSlot() {
		ItemStack other = new ItemStack(Material.BLACK_STAINED_GLASS_PANE, 1);
		ItemMeta meta = other.getItemMeta();
		meta.setDisplayName(" ");
		meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		other.setItemMeta(meta);
		return other;
	}
	
	public static ItemStack getGreenSlot() {
		ItemStack other = new ItemStack(Material.GREEN_STAINED_GLASS_PANE, 1);
		ItemMeta meta = other.getItemMeta();
		meta.setDisplayName(" ");
		meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		other.setItemMeta(meta);
		return other;
	}
	
	@SuppressWarnings("deprecation")
	public static ItemStack getTexture(Material m, int data) {
		return new ItemStack(m, 1, (short) data);
	}
	
	public static ItemStack getTexture(Material m, int data, String name) {
		@SuppressWarnings("deprecation")
		ItemStack item = new ItemStack(m, 1, (short) data);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(name);
		item.setItemMeta(meta);
		
		return item;
	}
	
	public static String chuHoaDau(String s) {
		return s.substring(0, 1).toUpperCase() + s.substring(1);
	}
	
	public static double round(double i) {
		DecimalFormat df = new DecimalFormat("#.##"); 
		String s = df.format(i).replace(",", ".");
		double newDouble = Double.valueOf(s);
		
		return newDouble;
	}
	
	public static void syncDamage(LivingEntity e, Player player, double damage, int tick) {
		damage(e, player, damage, tick, null);
	}
	
	public static void damage(LivingEntity e, Player player, double damage, int tick, Plugin plugin) {
		// Check delay
		if (damaged.containsKey(e)) {
			if (damaged.get(e) > System.currentTimeMillis()) return;
		}
		damaged.put(e, System.currentTimeMillis() + (tick * 1000 / 20));
		
		e.setMetadata("notattack", new FixedMetadataValue(MainSky2RPGCore.getMain(), ""));
		// Damage
		e.damage(damage, player);
	}
	
	public static void addHealth(Player player, double amount) {
		double currentHealth = player.getHealth();
		double maxHealth = player.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue();
		if (player.isDead()) return;
		player.setHealth(Math.min(maxHealth, Math.max(0, Math.min(currentHealth + amount, maxHealth))));
	}
	
	public static void createCircle(Particle particle, Location location, double radius) {
		int amount = new Double(radius * 20).intValue();
		double increment = (2 * Math.PI) / amount;
        ArrayList<Location> locations = new ArrayList<Location>();
        
        for (int i = 0 ; i < amount ; i++) {
            double angle = i * increment;
            double x = location.getX() + (radius * Math.cos(angle));
            double z = location.getZ() + (radius * Math.sin(angle));
            locations.add(new Location(location.getWorld(), x, location.getY(), z));
        }
        
        for (Location l : locations) {
//        	ParticleAPI.sendParticle(e, l, 0, 0, 0, 0, 1);
        	location.getWorld().spawnParticle(particle, l, 1, 0, 0, 0, 0);
        }
	}
	
	public static void createCircle(Particle particle, Location location, double radius, BlockData data) {
		int amount = new Double(radius * 20).intValue();
		double increment = (2 * Math.PI) / amount;
        ArrayList<Location> locations = new ArrayList<Location>();
        
        for (int i = 0 ; i < amount ; i++) {
            double angle = i * increment;
            double x = location.getX() + (radius * Math.cos(angle));
            double z = location.getZ() + (radius * Math.sin(angle));
            locations.add(new Location(location.getWorld(), x, location.getY(), z));
        }
        
        for (Location l : locations) {
//        	ParticleAPI.sendParticle(e, l, 0, 0, 0, 0, 1);
        	location.getWorld().spawnParticle(particle, l, 1, 0, 0, 0, 0, data);
        }
	}
	
	public static String toString(Map<String, String> map) {
		String s = "";
		for (String s1 : map.keySet()) {
			String s2 = map.get(s1);
			s += s1 + "," + s2 + ";";
		}
		try {
			s = s.substring(0, s.length() - 1);
		}
		catch (StringIndexOutOfBoundsException  e) {
			return s;
		}

		return s;
	}
	
	public static Map<String, String> fromString(String s) {
		Map<String, String> map = new HashMap<String, String> ();
		try {
			String[] a = s.split(";");
			for (String element : a) {
				String[] value = element.split(",");
				try {
					map.put(value[0], value[1]);
				}
				catch (Exception e) {
					return map;
				}
			}
			return map;
		}
		catch (Exception e) {
			return map;
		}

	}

	public static void sendToConsole(String s) {
		Bukkit.getConsoleSender().sendMessage(s);
	}
	
	
	public static boolean rate(double tiLe) {
		if (tiLe >= 100) return true;
		double rate = tiLe * 100;
		int random = new Random().nextInt(10000);
		if (random < rate) {
			return true;
		} else return false;
	}
	
	public static void giveItem(Player player, ItemStack item) {
		PlayerInventory inv = player.getInventory();
		boolean full = inv.firstEmpty() == -1;
		if (!full) {
			inv.addItem(item);
		} 
//		else {
//			Bukkit.getScheduler().runTask(MainEricRPG.main, () -> {
//				player.getWorld().dropItem(player.getLocation(), item);
//			});
//		}
		
	}
	
	public static double random(double min, double max) {
		return (new Random().nextInt(new Double((max - min) * 1000).intValue()) + min * 1000) / 1000;
	}
	
	public static int randomInt(int min, int max) {
		return new Random().nextInt(max - min + 1) + min;
	}
	
	public static double getMaxHealth(Player player) {
		return player.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue();
	}
	
	public static String boldAll(String ss) {
		String s = ss;
		int i = -1;
		while (i < s.length() - 1) {
			i++;
			char c = s.charAt(i);
			if (c == "§".toCharArray()[0]) {
				s = s.substring(0, i + 2) + "§l" + s.substring(i + 2, s.length());
				i+=2;
			}
		}
		return s;
	}
	
	public static String formatNumber(String ss) {
		String s = ss;
		for (double d : getListDoubleInString(ss)) {
			s = s.replace(d + "", round(d) + "");
		}
		return s;
	}
	
	public static double getDoubleInString(String s) {
		Pattern p = Pattern.compile("(\\d+(?:\\.\\d+))");
		Matcher m = p.matcher(s);
		while(m.find()) {
			double d = Double.parseDouble(m.group(1));
			return d;
		}
		return 0;
	}
	
	public static List<Double> getListDoubleInString(String s) {
		List<Double> list = new ArrayList<Double> ();
		Pattern p = Pattern.compile("(\\d+(?:\\.\\d+))");
		Matcher m = p.matcher(s);
		while(m.find()) {
			double d = Double.parseDouble(m.group(1));
			list.add(d);
		}
		return list;
	}
	

	public static Location randomLoc(Location loc, double max) {
		Vector direct1 = loc.getDirection().clone().setY(0);
		Vector direct2 = direct1.clone().setX(direct1.getZ()).setZ(direct1.getX() * -1f);
		
		double ranY = (new Random().nextInt(new Double(max * 1000).intValue()) - max * 500) / 1000;
		double ranM = (new Random().nextInt(new Double((max * 1000)).intValue()) - max / 2 * 1000)  / 1000;
		Location result = loc.clone();
		result.setY(ranY + loc.getY());
		result.add(direct2.multiply(ranM));
		
		return result;
	}

	public static int getIntInString(String s) {
		String intValue = s.replaceAll("[^0-9]", "");
		try {
			return Integer.parseInt(intValue);
		}
		catch (Exception e) {
			return 0;
		}	
	}
	
	
	public static  double getDistance(Player player, Location loc) {
		if (player.getWorld() != loc.getWorld()) return -1;
		return player.getLocation().distance(loc);
	}
	
	public static Player getNearestPlayer(Location loc) {
		double distance = Double.MAX_VALUE;
		Player player = null;
		for (Player p : Bukkit.getOnlinePlayers()) {
			if (p.getGameMode() == GameMode.CREATIVE) continue;
			double d = getDistance(p, loc);
			if (d < distance && d != -1) {
				player = p;
				distance = d;
			}
		}
		return player;
	}
	
	public static void broadcast(String mess) {
		Bukkit.getOnlinePlayers().forEach(p -> {
			p.sendMessage(mess);
		});
		Bukkit.getConsoleSender().sendMessage(mess);
	}
	
	public static void broadcast(String mess, String permission) {
		Bukkit.getOnlinePlayers().forEach(p -> {
			if (!p.hasPermission(permission)) return;
			p.sendMessage(mess);
		});
		Bukkit.getConsoleSender().sendMessage(mess);
	}
	
	
	public static List<String> guiTutorial(Map<Integer, String> tut, int rows) {
		List<String> t = new ArrayList<String> ();
		List<Integer> slots = new ArrayList<Integer> (tut.keySet());
		
		int count = 1;
		
		for (int i = 0 ; i < rows ; i++) {
			String line = "";
			for (int j = 9 * i ; j < 9 * (i + 1) ; j ++) {
				if (slots.contains(j)) {
					line += "§f" + getCircleNumber(count);
					count++;
				} else line += "§8▉";
			}
			t.add(line);
		}
		t.add("");
		Collections.sort(slots);
		int size = slots.size();
		for (int i = 1 ; i <= size ; i++) {
			t.add("§f" + getCircleNumber(i) + ": §7" + tut.get(slots.get(i - 1)));
		}
		
		return t;
	}
	
	public static void sendMessToOp(String mess) {
		Bukkit.getOnlinePlayers().forEach(p -> {
			if (p.hasPermission("seechat.*")) {
				p.sendMessage(mess);
			}
		});
	}
	
	private static String getCircleNumber(int i) {
		switch (i) {
		case 1:
			return "①";
		case 2:
			return "②";
		case 3:
			return "③";
		case 4:
			return "④";
		case 5:
			return "⑤";
		case 6:
			return "⑥";
		case 7:
			return "⑦";
		case 8:
			return "⑧";
		case 9:
			return "⑨";
		default:
			return "";
		}
	}
	
	public static String getEnchantFormat(Enchantment e, int lv) {
		Map<Enchantment, String> map = new HashMap<Enchantment, String> ();
		map.put(Enchantment.ARROW_DAMAGE, "Sức mạnh");
		map.put(Enchantment.ARROW_FIRE, "Lửa");
		map.put(Enchantment.ARROW_INFINITE, "Vô hạn");
		map.put(Enchantment.ARROW_KNOCKBACK, "Tên bật lùi");
		map.put(Enchantment.BINDING_CURSE, "Nguyền");
		map.put(Enchantment.DAMAGE_ALL, "Sắc nét");
		map.put(Enchantment.DAMAGE_ARTHROPODS, "Hại chân đốt");
		map.put(Enchantment.DAMAGE_UNDEAD, "Hại thây ma");
		map.put(Enchantment.DEPTH_STRIDER, "Đi dưới nước");
		map.put(Enchantment.DIG_SPEED, "Đào nhanh");
		map.put(Enchantment.DURABILITY, "Độ bền");
		map.put(Enchantment.FIRE_ASPECT, "Khía cạnh lửa");
		map.put(Enchantment.FROST_WALKER, "Chân băng");
		map.put(Enchantment.KNOCKBACK, "Bật lùi");
		map.put(Enchantment.LOOT_BONUS_BLOCKS, "Gia tài");
		map.put(Enchantment.LOOT_BONUS_MOBS, "Nhặt");
		map.put(Enchantment.LUCK, "May mắn");
		map.put(Enchantment.LURE, "Nhử");
		map.put(Enchantment.MENDING, "Sửa chữa");
		map.put(Enchantment.OXYGEN, "Oxi");
		map.put(Enchantment.PROTECTION_ENVIRONMENTAL, "Bảo vệ");
		map.put(Enchantment.PROTECTION_EXPLOSIONS, "Bảo vệ nổ");
		map.put(Enchantment.PROTECTION_FALL, "Bảo vệ rơi");
		map.put(Enchantment.PROTECTION_FIRE, "Bảo vệ lửa");
		map.put(Enchantment.PROTECTION_PROJECTILE, "Bảo vệ vật bắn");
		map.put(Enchantment.SILK_TOUCH, "Mềm mại");
		map.put(Enchantment.SWEEPING_EDGE, "Quét cạnh");
		map.put(Enchantment.THORNS, "Gai");
		map.put(Enchantment.VANISHING_CURSE, "Nguyền");
		map.put(Enchantment.WATER_WORKER, "Làm việc nước");
		map.put(Enchantment.LOYALTY, "Trung thành");
		map.put(Enchantment.CHANNELING, "Chớp điện");
		map.put(Enchantment.RIPTIDE, "Sóng dữ");
		map.put(Enchantment.IMPALING, "Xiên vào thịt");
		
		return map.get(e) + " " + soLaMa(lv);
	}
	
	public static String getVNPotionName(PotionEffectType type) {
		Map<PotionEffectType, String> map = Maps.newHashMap();
		map.put(PotionEffectType.ABSORPTION, "Hấp thụ");
		map.put(PotionEffectType.BLINDNESS, "Gây mù");
		map.put(PotionEffectType.CONDUIT_POWER, "Sức mạnh thủy triều");
		map.put(PotionEffectType.CONFUSION, "Nhầm lẫn");
		map.put(PotionEffectType.DAMAGE_RESISTANCE, "Kháng cự");
		map.put(PotionEffectType.DOLPHINS_GRACE, "Cá heo");
		map.put(PotionEffectType.FAST_DIGGING, "Đào nhanh");
		map.put(PotionEffectType.FIRE_RESISTANCE, "Kháng lửa");
		map.put(PotionEffectType.GLOWING, "Phát sáng");
		map.put(PotionEffectType.HARM, "Hại");
		map.put(PotionEffectType.HEAL, "Hồi phục");
		map.put(PotionEffectType.HEALTH_BOOST, "Tăng máu");
		map.put(PotionEffectType.HUNGER, "Gây đói");
		map.put(PotionEffectType.INCREASE_DAMAGE, "Tăng sát thương");
		map.put(PotionEffectType.INVISIBILITY, "Vô hình");
		map.put(PotionEffectType.JUMP, "Nhảy cao");
		map.put(PotionEffectType.LEVITATION, "Lơ lửng");
		map.put(PotionEffectType.LUCK, "May mắn");
		map.put(PotionEffectType.NIGHT_VISION, "Nhìn đêm");
		map.put(PotionEffectType.POISON, "Gây độc");
		map.put(PotionEffectType.REGENERATION, "Hồi phục");
		map.put(PotionEffectType.SATURATION, "Thấm vào");
		map.put(PotionEffectType.SLOW, "Gây chậm");
		map.put(PotionEffectType.SLOW_DIGGING, "Đào chậm");
		map.put(PotionEffectType.SLOW_FALLING, "Rơi chậm");
		map.put(PotionEffectType.SPEED, "Tốc độ");
		map.put(PotionEffectType.UNLUCK, "Đen đủi");
		map.put(PotionEffectType.WATER_BREATHING, "Thở dưới nước");
		map.put(PotionEffectType.WEAKNESS, "Yếu đuối");
		map.put(PotionEffectType.WITHER, "Khô héo");
		
		return map.get(type);
	}
	
	public static String soLaMa(int lv) {
		Map<Integer, String> map = new HashMap<Integer, String> ();
		map.put(1, "I");
		map.put(2, "II");
		map.put(3, "III");
		map.put(4, "IV");
		map.put(5, "V");
		map.put(6, "VI");
		map.put(7, "VII");
		map.put(8, "VIII");
		map.put(9, "IX");
		map.put(10, "X");
		map.put(11, "XI");
		map.put(12, "XII");
		map.put(13, "XIII");
		map.put(14, "XIV");
		map.put(15, "XV");
		map.put(16, "XVI");
		map.put(17, "XVII");
		map.put(18, "XVIII");
		map.put(19, "XIX");
		map.put(20, "XX");
		
		if (map.containsKey(lv)) return map.get(lv);
		return "" + lv;
	}
	
	public static List<String> toList(String s, int length, String start) {
	List<String> result = new ArrayList<String> ();
	if (s == null) return result;
	if (!s.contains(" ")) {
		result.add(s);
		return result;
	}
	
	
	String[] words = s.split(" ");
	int l = 0;
	String line = "";
	for (int i = 0 ; i < words.length ; i ++) {
		l += words[i].length();
		if (l > length) {
			result.add(line.substring(0, line.length() - 1));
			l = words[i].length();
			line = "";
			line += words[i] + " ";
		}
		else {
			line += words[i] + " ";
		}
	}
	
	if (!line.equalsIgnoreCase(" ")) result.add(line);
	
	for (int i = 0 ; i < result.size() ; i++) {
		result.set(i, start + result.get(i));
	}
	
	return result;
	}
	
	public static void sendSound(Player player, SoundEffect sound, float volume, float pitch) {
		SoundEffect se = sound;
		PacketPlayOutNamedSoundEffect packet = new PacketPlayOutNamedSoundEffect(se, SoundCategory.PLAYERS, player.getLocation().getX(), player.getLocation().getY(), player.getLocation().getZ(), volume, pitch);
		((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
	}
	
	public static void hologram(Location location, String message, int tick, Player player) {
		EntityArmorStand as = new EntityArmorStand(((CraftWorld) location.getWorld()).getHandle());
		as.setInvisible(true);
		as.setCustomName(new ChatMessage(message));
		as.setCustomNameVisible(true);
		as.setPosition(location.getX(), location.getY() - 0.5, location.getZ());
		
		((CraftPlayer) player).getHandle().playerConnection.sendPacket(new PacketPlayOutSpawnEntityLiving(as));
		
		Bukkit.getScheduler().runTaskLaterAsynchronously(MainSky2RPGCore.getMain(), () -> {
			((CraftPlayer) player).getHandle().playerConnection.sendPacket(new PacketPlayOutEntityDestroy(as.getId()));
		}, tick);
	}
	
	@SuppressWarnings("rawtypes")
	public static String mapToString(Map<Enum, Integer> map) {
		String result = "";
		for (Enum e : map.keySet()) {
			result += e.name() + ":" + map.get(e) + ";";
		}
		result = result.substring(0, result.length() - 1);
		return result;
	}
	
	public static List<ItemStack> getItemsInPlayer(Player player) {
		List<ItemStack> list = new ArrayList<ItemStack> ();
		
		// Armor
		for (ItemStack item : player.getInventory().getArmorContents()) {
			if (item != null && item.getType() != Material.AIR) list.add(item);
		}
		
		// Hand
		ItemStack itemHand = player.getInventory().getItemInMainHand();
		if (itemHand != null && itemHand.getType() != Material.AIR) list.add(itemHand);
		
		// Offhand
//		ItemStack offHand = player.getInventory().getItemInOffHand();
//		if (offHand != null) {
//			MinecraftItemType type = MinecraftItemType.getType(offHand.getType());
//			if (type == MinecraftItemType.AXE || type == MinecraftItemType.SHIELD || type == MinecraftItemType.SWORD) {
//				if (offHand != null && offHand.getType() != Material.AIR) list.add(offHand);
//			}
//		}
		
		// Jewelry
		list.addAll(JewelryUtils.getData(player).values());
		
		return list;
	}
	
	
}