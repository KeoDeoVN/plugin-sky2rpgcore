package mk.plugin.eternalcore.feature.dabaoho;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import kdvn.sky2.rpg.core.utils.ItemStackUtils;

public class EKeepGem {
	
	private static final String NAME = "§a§lĐá bảo hộ";
	
	public static ItemStack getItem() {
		ItemStack item = new ItemStack(Material.DIAMOND);
		ItemStackUtils.setDisplayName(item, NAME);
		ItemStackUtils.addLoreLine(item, "§7§oĐể ở trong kho đồ, có tác dụng");
		ItemStackUtils.addLoreLine(item, "§7§ogiữ đồ, kinh nghiệm khi chết và");
		ItemStackUtils.addLoreLine(item, "§7§otiêu hao một viên cho mỗi lần");
		ItemStackUtils.addEnchantEffect(item);
		return item;
	}
	
	public static boolean isThatItem(ItemStack item) {
		return ItemStackUtils.getName(item).contains(NAME);
	}
	
}
