package mk.plugin.eternalcore.feature.set;

import java.util.List;
import java.util.Map;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import kdvn.sky2.rpg.core.buff.SRPGStatBuff;
import kdvn.sky2.rpg.core.item.SRPGItemUtils;
import kdvn.sky2.rpg.core.stat.SRPGStat;
import kdvn.sky2.rpg.core.utils.SRPGUtils;

public class ESetUtils {
	
	private static List<ESet> Sets = Lists.newArrayList();
	
	public static void loadSets(FileConfiguration config) {
		Sets.clear();
		config.getConfigurationSection("set").getKeys(false).forEach(id -> {
			String name = config.getString("set." + id + ".name");
			List<String> items = config.getStringList("set." + id + ".items");
			List<String> descs = config.getStringList("set." + id + ".descs");
			List<List<SRPGStatBuff>> buffs = Lists.newArrayList();
			config.getStringList("set." + id + ".buffs").forEach(s -> {
				List<SRPGStatBuff> list = Lists.newArrayList();
				for (String c : s.split(";")) {
					SRPGStat stat = SRPGStat.valueOf(c.split(":")[0].toUpperCase());
					double value = Double.valueOf(c.split(":")[1]);
					list.add(new SRPGStatBuff(stat, value, true, 0l, 0l));
				}
				buffs.add(list);
			});
			Sets.add(new ESet(name, items, buffs, descs));
		});
	}
	
	public static List<String> getItems(Player player) {
		List<String> list = Lists.newArrayList();
		SRPGUtils.getItemsInPlayer(player).forEach(item -> {
			if (SRPGItemUtils.isRPGItem(item)) list.add(SRPGItemUtils.fromItemStack(item).getName());
		});
		return list;
	}
	
	public static boolean isEquip(Player player, String item) {
		return getItems(player).contains(item);
	}
	
	public static Map<ESet, Integer> getSets(Player player) {
		List<String> items = getItems(player);
		Map<ESet, Integer> map = Maps.newHashMap();
		
		Sets.forEach(set -> {
			int count = 0;
			for (String item : items) {
				if (set.getItems().contains(item)) count++;
			}
			if (count > 1) map.put(set, count);
		});
		
		return map;
	}
	
	public static int getAmount(Player player, ESet set) {
		return getSets(player).getOrDefault(set, 1);
	}
	
	public static boolean isInAnySet(String name) {
		for (ESet set : Sets) {
			if (set.getItems().contains(name)) return true;
		}
		return false;
	}
	
	public static ESet getSetFromItem(String name) {
		for (ESet set : Sets) {
			if (set.getItems().contains(name)) return set;
		}
		return null;
	}
	
}
