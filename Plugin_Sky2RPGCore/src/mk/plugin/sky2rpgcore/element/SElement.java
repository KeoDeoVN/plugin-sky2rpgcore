package mk.plugin.sky2rpgcore.element;

import org.bukkit.Particle;

import kdvn.sky2.rpg.core.buff.SRPGStatBuff;
import kdvn.sky2.rpg.core.classes.SRPGClass;
import kdvn.sky2.rpg.core.stat.SRPGStat;

public enum SElement {
	
	KIM(SRPGClass.THAN_RIU, "Kim", "§7", Particle.CRIT_MAGIC) {
		@Override
		public SElement getCounter() {
			return SElement.HOA;
		}

		@Override
		public SRPGStatBuff getBuff() {
			return new SRPGStatBuff(SRPGStat.SUC_THU, 10, true, 0, 0);
		}
	},
	MOC(SRPGClass.CUNG_THU, "Mộc", "§a", Particle.VILLAGER_HAPPY) {
		@Override
		public SElement getCounter() {
			return SElement.KIM;
		}

		@Override
		public SRPGStatBuff getBuff() {
			return new SRPGStatBuff(SRPGStat.HUT_MAU, 10, true, 0, 0);
		}
	},
	THUY(null, "Thủy", "§b", Particle.WATER_WAKE) {
		@Override
		public SElement getCounter() {
			return SElement.THO;
		}

		@Override
		public SRPGStatBuff getBuff() {
			return new SRPGStatBuff(SRPGStat.TOC_DANH, 10, true, 0, 0);
		}
	},
	HOA(SRPGClass.KIEM_KHACH, "Hỏa", "§c", Particle.FLAME) {
		@Override
		public SElement getCounter() {
			return SElement.THUY;
		}

		@Override
		public SRPGStatBuff getBuff() {
			return new SRPGStatBuff(SRPGStat.SAT_THUONG, 10, true, 0, 0);
		}
	},
	THO(SRPGClass.CHIEN_BINH, "Thổ", "§6", Particle.CRIT) {
		@Override
		public SElement getCounter() {
			return SElement.MOC;
		}

		@Override
		public SRPGStatBuff getBuff() {
			return new SRPGStatBuff(SRPGStat.MAU, 10, true, 0, 0);
		}
	},
	
	
	KIM_S(SRPGClass.THAN_RIU_PLUS, "Kim+", "§7", Particle.CRIT_MAGIC) {
		@Override
		public SElement getCounter() {
			return SElement.HOA;
		}

		@Override
		public SRPGStatBuff getBuff() {
			return new SRPGStatBuff(SRPGStat.SUC_THU, 25, true, 0, 0);
		}
	},
	MOC_S(SRPGClass.CUNG_THU_PLUS, "Mộc+", "§a", Particle.VILLAGER_HAPPY) {
		@Override
		public SElement getCounter() {
			return SElement.KIM;
		}

		@Override
		public SRPGStatBuff getBuff() {
			return new SRPGStatBuff(SRPGStat.HUT_MAU, 25, true, 0, 0);
		}
	},
	THUY_S(null, "Thủy+", "§b", Particle.WATER_WAKE) {
		@Override
		public SElement getCounter() {
			return SElement.THO;
		}

		@Override
		public SRPGStatBuff getBuff() {
			return new SRPGStatBuff(SRPGStat.TOC_DANH, 25, true, 0, 0);
		}
	},
	HOA_S(SRPGClass.KIEM_KHACH_PLUS, "Hỏa+", "§c", Particle.FLAME) {
		@Override
		public SElement getCounter() {
			return SElement.THUY;
		}

		@Override
		public SRPGStatBuff getBuff() {
			return new SRPGStatBuff(SRPGStat.SAT_THUONG, 25, true, 0, 0);
		}
	},
	THO_S(SRPGClass.CHIEN_BINH_PLUS, "Thổ+", "§6", Particle.CRIT) {
		@Override
		public SElement getCounter() {
			return SElement.MOC;
		}

		@Override
		public SRPGStatBuff getBuff() {
			return new SRPGStatBuff(SRPGStat.MAU, 25, true, 0, 0);
		}
	},;
	
	private SRPGClass inherit;
	private String name;
	private String color;
	private Particle p;
	
	private SElement(SRPGClass inherit, String name, String color, Particle p) {
		this.inherit = inherit;
		this.name = name;
		this.color = color;
		this.p = p;
	}
	
	public abstract SElement getCounter();
	public abstract SRPGStatBuff getBuff();
	
	public SRPGClass getInherit() {
		return this.inherit;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getColor() {
		return this.color;
	}
	
	public Particle getParticle() {
		return this.p;
	}
	
}
