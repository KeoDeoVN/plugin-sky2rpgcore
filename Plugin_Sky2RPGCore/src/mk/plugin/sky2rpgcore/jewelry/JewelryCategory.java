package mk.plugin.sky2rpgcore.jewelry;

public enum JewelryCategory {
	
	RING,
	BRACELET,
	NECKLACE
	
}
