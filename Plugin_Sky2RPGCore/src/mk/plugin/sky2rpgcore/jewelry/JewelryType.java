package mk.plugin.sky2rpgcore.jewelry;

import java.util.List;
import java.util.Map;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import kdvn.sky2.rpg.core.stat.SRPGStat;
import mk.plugin.sky2rpgcore.jewelry.passives.PHonUoc;

public enum JewelryType {
	
	NHANDINHHON(JewelryCategory.RING, "WOW Nhẫn đính hôn", "Hôn ước", new PHonUoc(), Material.WOODEN_AXE, "Nhẫn giúp tăng khả năng hồi phục khi vợ chồng gần nhau", Lists.newArrayList(1, 2, 3, 4, 5), Lists.newArrayList(SRPGStat.SAT_THUONG, SRPGStat.HOI_PHUC), Lists.newArrayList(10, 10));
	
	private JewelryCategory category;
	private String name;
	private Material m;
	private List<Integer> dur;
	private Map<SRPGStat, Integer> basics;
	private String desc;
	private Passive passive;
	private String passiveName;
	
	private JewelryType(JewelryCategory category, String name, String passiveName, Passive passive, Material m, String desc, List<Integer> dur, List<SRPGStat> stats, List<Integer> svalues) {
		this.category = category;
		this.name = name;
		this.passiveName = passiveName;
		this.m = m;
		this.desc = desc;
		this.dur = dur;
		this.basics = Maps.newHashMap();
		for (int i = 0 ; i < stats.size() ; i++) {
			basics.put(stats.get(i), svalues.get(i));
		}
		this.passive = passive;
	}

	public JewelryCategory getCategory() {
		return this.category;
	}
	
	public String getName() {
		return name;
	}

	public Material getMaterial() {
		return m;
	}

	public String getDesc() {
		return this.desc;
	}
	
	public List<Integer> getDurabilities() {
		return dur;
	}
	
	public ItemStack getTexture(int degree) {
		return new ItemStack(m, dur.get(degree) - 1);
	}
	
	public Map<SRPGStat, Integer> getStats() {
		return this.basics;
	}
	
	public Passive getPassive() {
		return this.passive;
	}
	
	public String getPassiveName() {
		return this.passiveName;
	}
	
}
