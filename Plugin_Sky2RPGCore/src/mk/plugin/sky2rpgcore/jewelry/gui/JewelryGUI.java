package mk.plugin.sky2rpgcore.jewelry.gui;

import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.inventory.meta.ItemMeta;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.utils.GUIUtils;
import kdvn.sky2.rpg.core.utils.ItemStackUtils;
import mk.plugin.sky2rpgcore.jewelry.Jewelry;
import mk.plugin.sky2rpgcore.jewelry.JewelryCategory;
import mk.plugin.sky2rpgcore.jewelry.util.JewelryUtils;
import net.md_5.bungee.api.ChatColor;

public class JewelryGUI {
	
	public static final int SIZE = 18;
	
	public static final String TITLE = ChatColor.translateAlternateColorCodes('&', "&c&lTÚI TRANG SỨC");
	
	public static void openGUI(Player player) {
		Inventory inv = Bukkit.createInventory(null, SIZE, TITLE);
		player.openInventory(inv);
		
		Bukkit.getScheduler().runTaskAsynchronously(MainSky2RPGCore.getMain(), () -> {
			for (int i = 0 ; i < SIZE ; i++) {
				inv.setItem(i, GUIUtils.getBlackSlot());
			}
			getSlots().forEach((i, c) -> {
				inv.setItem(i, getBlankSlot().get(c));
			});
			JewelryUtils.getData(player).forEach((i, item) -> {
				inv.setItem(i, item);
			});
		});
	}
	
	@SuppressWarnings("deprecation")
	public static void eventClick(InventoryClickEvent e) {
		if (!e.getView().getTitle().equals(TITLE)) return;	
//		Player player = (Player) e.getWhoClicked();
		int slot = e.getSlot();
		
		if (e.getClickedInventory() == e.getWhoClicked().getOpenInventory().getTopInventory()) {
			e.setCancelled(true);
			ItemStack current = e.getCurrentItem();
			if (getSlots().keySet().contains(slot) || JewelryUtils.isJewelry(current)) {
				ItemStack cursor = e.getCursor();
				if (JewelryUtils.isJewelry(cursor)) {
					// Check slot
					Jewelry j = JewelryUtils.fromItemStack(cursor);
					if (getSlots().get(slot) != j.getType().getCategory()) return;
					if (isBlankSlot(current)) {
						e.setCursor(null);
					} else 	e.setCursor(current);
					e.setCurrentItem(cursor);
				}
				else {
					if (JewelryUtils.isJewelry(current)) {
						Jewelry j = JewelryUtils.fromItemStack(current);
						e.setCursor(current);
						e.setCurrentItem(getBlankSlot().get(j.getType().getCategory()));
					}
				}
			}
		}
		

//		if (e.getClickedInventory() == e.getView().getBottomInventory()) {
//			if (e.getClick() == ClickType.SHIFT_LEFT) {
//				e.setCancelled(true);
//				player.playSound(player.getLocation(), Sound.BLOCK_LEVER_CLICK, 1, 1);
//				Inventory bInv = player.getOpenInventory().getBottomInventory();
//				ItemStack clickedItem = bInv.getItem(slot);
//				if (JewelryUtils.isJewelry(clickedItem)) {
//					Jewelry j = JewelryUtils.fromItemStack(clickedItem);
//					if (!GUIUtils.placeItem(clickedItem, slot, e.getView().getTopInventory(), bInv, getSlots(j.getType().getCategory()))) {
//						player.sendMessage("§cĐã để Trang sức rồi rồi");
//						return;
//					}
//				}
//			}
//		}
		
	}
	
	public static void eventClose(InventoryCloseEvent e) {
		if (!e.getView().getTitle().equals(TITLE)) return;	
		Map<Integer, ItemStack> data = Maps.newHashMap();
		Inventory inv = e.getInventory();
		Player player = (Player) e.getPlayer();
		
		if (!GUIUtils.checkCheck(player)) return;
		
		getSlots().forEach((i, c) -> {
			ItemStack item = inv.getItem(i);
			if (isBlankSlot(item)) return;
			if (GUIUtils.isItemSlot(item)) return;
			data.put(i, item);
		});
		
		JewelryUtils.saveData(player, data);
	}
	
	
	
	public static Map<Integer, JewelryCategory> getSlots() {
		Map<Integer, JewelryCategory> map = Maps.newHashMap();
		map.put(2, JewelryCategory.RING);
		map.put(11, JewelryCategory.RING);
		map.put(4, JewelryCategory.NECKLACE);
		map.put(13, JewelryCategory.NECKLACE);
		map.put(6, JewelryCategory.BRACELET);
		map.put(15, JewelryCategory.BRACELET);
		return map;
	}
	
	public static List<Integer> getSlots(JewelryCategory c) {
		List<Integer> list = Lists.newArrayList();
		getSlots().forEach((i, ctg) -> {
			if (ctg == c) list.add(i);
		});
		return list;
	}
	
	public static Map<JewelryCategory, ItemStack> getBlankSlot() {
		Map<JewelryCategory, ItemStack> m = Maps.newHashMap();
		
		// RING
		ItemStack item = new ItemStack(Material.WOODEN_HOE);
		ItemMeta meta = item.getItemMeta();
		((Damageable) meta).setDamage(1);
		meta.setUnbreakable(true);
		meta.setDisplayName(ChatColor.GREEN.toString() + ChatColor.ITALIC + ChatColor.BOLD + "Ô để Nhẫn");
		meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
		item.setItemMeta(meta);
		m.put(JewelryCategory.RING, ItemStackUtils.setTag(item, "jslot", ""));
		
		// NECKLACE
		item = new ItemStack(Material.WOODEN_HOE);
		meta = item.getItemMeta();
		((Damageable) meta).setDamage(2);
		meta.setUnbreakable(true);
		meta.setDisplayName(ChatColor.GREEN.toString() + ChatColor.ITALIC + ChatColor.BOLD + "Ô để Vòng cổ");
		meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
		item.setItemMeta(meta);
		m.put(JewelryCategory.NECKLACE, ItemStackUtils.setTag(item, "jslot", ""));
		
		// BRACELET
		item = new ItemStack(Material.WOODEN_HOE);
		meta = item.getItemMeta();
		((Damageable) meta).setDamage(3);
		meta.setUnbreakable(true);
		meta.setDisplayName(ChatColor.GREEN.toString() + ChatColor.ITALIC + ChatColor.BOLD + "Ô để Vòng tay");
		meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
		item.setItemMeta(meta);
		m.put(JewelryCategory.BRACELET, ItemStackUtils.setTag(item, "jslot", ""));
		
		return m;
	}
	
	public static boolean isBlankSlot(ItemStack item) {
		if (item == null) return false;
		return ItemStackUtils.hasTag(item, "jslot");
	}
	
}
