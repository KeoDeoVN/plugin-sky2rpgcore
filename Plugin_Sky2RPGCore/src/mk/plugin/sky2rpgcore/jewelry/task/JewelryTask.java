package mk.plugin.sky2rpgcore.jewelry.task;

import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

import kdvn.sky2.rpg.core.utils.SRPGUtils;
import mk.plugin.sky2rpgcore.jewelry.Jewelry;
import mk.plugin.sky2rpgcore.jewelry.util.JewelryUtils;

public class JewelryTask extends BukkitRunnable {

	@Override
	public void run() {
		Bukkit.getOnlinePlayers().forEach(player -> {
			SRPGUtils.getItemsInPlayer(player).forEach(item -> {
				if (JewelryUtils.isJewelry(item)) {
					Jewelry j = JewelryUtils.fromItemStack(item);
					j.getType().getPassive().doTask(player);
				}
			});
		});
	}

}
