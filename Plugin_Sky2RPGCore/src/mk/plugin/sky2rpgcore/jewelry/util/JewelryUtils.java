package mk.plugin.sky2rpgcore.jewelry.util;

import java.util.Map;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.inventory.meta.ItemMeta;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import kdvn.sky2.rpg.core.item.SItem;
import kdvn.sky2.rpg.core.item.SRPGItemUtils;
import kdvn.sky2.rpg.core.utils.ItemStackUtils;
import mk.plugin.playerdata.storage.PlayerData;
import mk.plugin.playerdata.storage.PlayerDataAPI;
import mk.plugin.sky2rpgcore.jewelry.Jewelry;
import mk.plugin.sky2rpgcore.jewelry.JewelryType;
import mk.plugin.sky2rpgcore.jewelry.gui.JewelryGUI;
import mk.plugin.sky2rpgcore.weapon.Grade;

public class JewelryUtils {
	
	public static Map<Integer, ItemStack> getData(Player player) {
		Map<Integer, ItemStack> m = Maps.newHashMap();
		PlayerData pd = PlayerDataAPI.getPlayerData(player);
		for (int i = 0 ; i < JewelryGUI.SIZE ; i++) {
			if (pd.hasData("sky2rpgcore-jewelry-" + i)) {
				m.put(i, ItemStackUtils.toItemStack(pd.getValue("sky2rpgcore-jewelry-" + i)));
			}
		}
		return m;
	}
	
	public static void saveData(Player player, Map<Integer, ItemStack> m) {
		PlayerData pd = PlayerDataAPI.getPlayerData(player);
		for (int i = 0 ; i < JewelryGUI.SIZE ; i++) {
			if (pd.hasData("sky2rpgcore-jewelry-" + i)) {
				pd.remove("sky2rpgcore-jewelry-" + i);
			}
		}
		m.forEach((i, item) -> {
			pd.set("sky2rpgcore-jewelry-" + i, ItemStackUtils.toBase64ItemStack(item));
		});
		PlayerDataAPI.saveData(player);
	}
	
	public static boolean isJewelry(ItemStack item) {
		if (ItemStackUtils.isNull(item)) return false;
		return ItemStackUtils.hasTag(item, "sRPG.jewelrytype");
	}
	
	public static void update(Player player, ItemStack is, Jewelry jewelry) {
		SRPGItemUtils.updateItem(player, is, jewelry.getItem());
	}
	
	public static ItemStack set(Player player, ItemStack is, Jewelry jewelry) {
		is = SRPGItemUtils.setWithoutUpdate(player, is, jewelry.getItem());
		is = ItemStackUtils.setTag(is, "sRPG.jewelrytype", jewelry.getType().name());
		
		is.setType(jewelry.getType().getMaterial());
		ItemMeta meta = is.getItemMeta();
		if (meta instanceof Damageable) {
			((Damageable) meta).setDamage(jewelry.getType().getDurabilities().get(jewelry.getItem().getGrade().getNumber() - 1));
		}
		is.setItemMeta(meta);
		
		update(player, is, jewelry);
		
		return is;
	}
	
	public static Jewelry fromItemStack(ItemStack is) {
		SItem item = SRPGItemUtils.fromItemStack(is);
		JewelryType type = JewelryType.valueOf(ItemStackUtils.getTag(is, "sRPG.jewelrytype"));
		return new Jewelry(type, item);
	}
	
	public static ItemStack getJewelry(Player player, JewelryType type) {
		ItemStack is = new ItemStack(Material.WOODEN_SWORD);
		ItemStackUtils.setDisplayName(is, "cc");
		SItem si = new SItem(null, 0, 2, Maps.newHashMap(), Lists.newArrayList(), Maps.newHashMap(), null, Grade.I, 0);
		Jewelry w = new Jewelry(type, si);
		return JewelryUtils.set(player, is, w);
	}

	
	
	
}
