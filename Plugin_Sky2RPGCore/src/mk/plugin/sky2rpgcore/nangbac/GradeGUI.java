package mk.plugin.sky2rpgcore.nangbac;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import kdvn.sky2.rpg.core.item.SItem;
import kdvn.sky2.rpg.core.item.SRPGItem;
import kdvn.sky2.rpg.core.item.SRPGItemUtils;
import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.utils.GUIUtils;
import kdvn.sky2.rpg.core.utils.ItemStackUtils;
import kdvn.sky2.rpg.core.utils.SRPGUtils;
import mk.plugin.sky2rpgcore.weapon.Grade;
import net.minecraft.server.v1_13_R2.SoundEffects;

public class GradeGUI {
	
	public static final int ITEM_SLOT = 11;
	public static final int GS_SLOT = 13;
	public static final int RESULT_SLOT = 15;
	public static final int BUTTON_SLOT = 23;
	
	public static String TITLE = "§c§lTĂNG BẬC VẬT PHẨM";
	
	public static void openGUI(Player player, ItemStack item) {
		Inventory inv = Bukkit.createInventory(null, 36, TITLE);
		
		Bukkit.getScheduler().runTaskAsynchronously(MainSky2RPGCore.getMain(), () -> {
			SRPGUtils.sendSound(player, SoundEffects.BLOCK_ENDER_CHEST_OPEN, 1, 1);
			for (int i = 0 ; i < inv.getSize() ;  i++) {
				inv.setItem(i, GUIUtils.getBlackSlot());
			}
			inv.setItem(ITEM_SLOT, GUIUtils.getItemSlot(new ItemStack(Material.LIME_STAINED_GLASS_PANE, 1), "§aÔ chứa §oVật phẩm"));
			inv.setItem(GS_SLOT, GUIUtils.getItemSlot(new ItemStack(Material.BLUE_STAINED_GLASS_PANE, 1), "§aÔ chứa §oĐá tăng bậc"));
			inv.setItem(RESULT_SLOT, GUIUtils.getItemSlot(new ItemStack(Material.PINK_STAINED_GLASS_PANE, 1), "§aÔ chứa §oSản phẩm"));
			inv.setItem(BUTTON_SLOT, GUIUtils.getButton(Arrays.asList(new String[] {"§aĐiểm nhận được: §f0 exp", "§6Click để thực hiện"})));
			if (item != null) {
				inv.setItem(ITEM_SLOT, item);
				SRPGItem rpgI = SRPGItemUtils.fromItemStack(item);
				ItemStack clone = item.clone();
				ItemStackUtils.setDisplayName(clone, ItemStackUtils.getName(clone) + " §e(Sản phẩm)");
				rpgI.setGrade(GradeUtils.getNextGrade(rpgI.getGrade()));;
				clone = SRPGItemUtils.setItem(player, clone, rpgI);
				inv.setItem(RESULT_SLOT, clone);
			}
		});
		
		player.openInventory(inv);
	}
	
	public static void openGUI(Player player) {
		openGUI(player, null);
	}
	
	public static void eventClick(InventoryClickEvent e) {
		if (e.getClickedInventory() == null) return;
		Inventory inv = e.getWhoClicked().getOpenInventory().getTopInventory();
		if (!e.getView().getTitle().equals(TITLE)) return;
		e.setCancelled(true);
		Player player = (Player) e.getWhoClicked();
		int slot = e.getSlot();
		
		// Action on top inv
		if (e.getClickedInventory() == player.getOpenInventory().getTopInventory()) {
			if (slot == BUTTON_SLOT) {
				// Get Item
				ItemStack item = GUIUtils.getItem(inv, ITEM_SLOT);
				if (item == null) {
					player.sendMessage("§cThiếu Vật phẩm!");
					return;
				}
				// Get DCH
				ItemStack dchI = GUIUtils.getItem(inv, GS_SLOT);
				if (dchI == null) {
					player.sendMessage("§cThiếu Đá tăng bậc!");
					return;
				}
				
				// Object
				SRPGItem rpgI = SRPGItemUtils.fromItemStack(item);
				int exp = dchI.getAmount() * 100;
				
				// Do
				player.sendTitle("§a§lTHÀNH CÔNG ^_^", "§6§l+" + exp + " exp cho trang bị", 0, 15, 0);
				SRPGUtils.sendSound(player, SoundEffects.ENTITY_FIREWORK_ROCKET_LAUNCH, 1, 1);
				rpgI.setGradeExp(rpgI.getGradeExp() + exp);
				
				ItemStack i = SRPGItemUtils.setItem(player, item, rpgI);
				SRPGUtils.giveItem(player, i);
				

				// Call event
				Bukkit.getPluginManager().callEvent(new SkyNangBacEvent(player, item, rpgI.getGrade()));
				
				// Close and reopen
				inv.setItem(GS_SLOT, null);
				inv.setItem(ITEM_SLOT, null);
				player.closeInventory();
				Bukkit.getScheduler().runTaskLater(MainSky2RPGCore.getMain(), () -> {
					openGUI(player);
				}, 10);
				
			}
		} 
		// Action on bottom inv
		else {
			if (e.getClick() == ClickType.SHIFT_LEFT) {
				SRPGUtils.sendSound(player, SoundEffects.BLOCK_LEVER_CLICK, 1f, 1f);
				Inventory bInv = player.getOpenInventory().getBottomInventory();
				ItemStack clickedItem = bInv.getItem(slot);
				
				// Item
				if (SRPGItemUtils.isRPGItem(clickedItem)) {
					SRPGItem rpgI = SRPGItemUtils.fromItemStack(clickedItem);
					if (rpgI.getGrade() == Grade.V) {
						player.sendMessage("§cBậc tối đa, không thể tăng thêm");
						return;
					}
					if (!GUIUtils.placeItem(clickedItem, slot, inv, bInv, ITEM_SLOT)) {
						player.sendMessage("§cĐã để Vật phẩm rồi");
						return;
					}
					
					// Update result
					Bukkit.getScheduler().runTaskAsynchronously(MainSky2RPGCore.getMain(), () -> {
						ItemStack clone = clickedItem.clone();
						ItemStackUtils.setDisplayName(clone, ItemStackUtils.getName(clone) + " §e(Sản phẩm)");
						rpgI.setGrade(GradeUtils.getNextGrade(rpgI.getGrade()));;
						clone = SRPGItemUtils.setItem(player, clone, rpgI);
						inv.setItem(RESULT_SLOT, clone);
					});
				}
				
				else if (GradeStone.isThatItem(clickedItem)) {
					if (!GUIUtils.placeItem(clickedItem, clickedItem.getAmount(), slot, inv, bInv, GS_SLOT)) {
						player.sendMessage("§cĐã để Đá tăng bậc rồi");
						return;
					}
				}
				
				// Update button
				ItemStack item = GUIUtils.getItem(inv, ITEM_SLOT);
				ItemStack dchI = GUIUtils.getItem(inv, GS_SLOT);
				if (item != null) {
					SItem si = SRPGItemUtils.fromItemStack(item);
					int exp = 0;
					if (dchI != null) exp = dchI.getAmount() * 100;
					inv.setItem(BUTTON_SLOT, GUIUtils.getButton(Arrays.asList(new String[] {"§aĐiểm nhận được: §f" + exp + " exp", "§aĐiểm hiện tại: §f" + si.getGradeExp() + "/" + GradeUtils.getExpTo(GradeUtils.getNextGrade(si.getGrade())), "§6Click để thực hiện"})));
				}
				
			} else player.sendMessage("§cẤn §fShift + Chuột trái §cđể đặt item");
		}
	}
	
	public static void eventClose(InventoryCloseEvent e) {
		Player player = (Player) e.getPlayer();
		Inventory inv = e.getInventory();
		if (!e.getView().getTitle().equals(TITLE)) return;
		
		if (!GUIUtils.checkCheck(player)) return;
		
		List<ItemStack> items = new ArrayList<ItemStack> ();
		items.add(inv.getItem(GS_SLOT));
		items.add(inv.getItem(ITEM_SLOT));
		
		items.forEach(item -> {
			if (item == null || ItemStackUtils.hasTag(item, GUIUtils.SLOT_ITEM_TAG)) return;
			SRPGUtils.giveItem(player, item);
		});
	}
	
}
