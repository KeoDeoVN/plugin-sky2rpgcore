package mk.plugin.sky2rpgcore.nangbac;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import kdvn.sky2.rpg.core.utils.ItemStackUtils;

public class GradeStone {
		
	private static final String NAME = "§a§lĐá nâng bậc";
	
	public static ItemStack getItem() {
		ItemStack item = new ItemStack(Material.EMERALD);
		ItemStackUtils.setDisplayName(item, NAME);
		ItemStackUtils.addLoreLine(item, "§7§oCó tác dụng tăng bậc cho trang bị");
		ItemStackUtils.addEnchantEffect(item);
		return item;
	}
	
	public static boolean isThatItem(ItemStack item) {
		return ItemStackUtils.getName(item).contains(NAME);
	}
	
	
}
