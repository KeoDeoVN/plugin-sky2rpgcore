package mk.plugin.sky2rpgcore.nangbac;

import mk.plugin.sky2rpgcore.weapon.Grade;

public class GradeUtils {
	
	public static int getExpRequirement(Grade grade) {
		switch (grade) {
		case I: return 0;
		case II: return 100;
		case III: return 500;
		case IV: return 900;
		case V: return 1500;
		}
		return 0;
	}
	
	public static int getExpTo(Grade grade) {
		int exp = 0;
		for (Grade g : Grade.values()) {
			if (g.getNumber() <= grade.getNumber()) exp += getExpRequirement(g);
		}
		return exp;
	}
	
	public static Grade getNextGrade(Grade grade) {
		int lv = grade.getNumber();
		for (Grade g : Grade.values()) {
			if (g.getNumber() == lv + 1) return g;
		}
		return null;
	}
	
}
