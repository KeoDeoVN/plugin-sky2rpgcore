package mk.plugin.sky2rpgcore.nangbac;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;
import org.bukkit.inventory.ItemStack;

import mk.plugin.sky2rpgcore.weapon.Grade;

public class SkyNangBacEvent extends PlayerEvent {
	
	private ItemStack item;
	private Grade newGrade;
	
	public SkyNangBacEvent(Player who, ItemStack item, Grade newGrade) {
		super(who);
		this.item = item;
		this.newGrade = newGrade;
	}

	public ItemStack getItem() {
		return this.item;
	}
	
	public Grade getNewGrade() {
		return this.newGrade;
	}
	
	private static final HandlerList handlers = new HandlerList();
	
	public static HandlerList getHandlerList() {
		return handlers;
	}
	
	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

}
