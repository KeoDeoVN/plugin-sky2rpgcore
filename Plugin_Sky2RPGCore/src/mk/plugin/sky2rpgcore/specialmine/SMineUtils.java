package mk.plugin.sky2rpgcore.specialmine;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Item;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.utils.ItemStackUtils;
import kdvn.sky2.rpg.core.utils.SRPGUtils;

public class SMineUtils {
	
	public static ItemStack getItem(Material m) {
		String name = SRPGUtils.getOreName(m);
		ItemStack item = new ItemStack(m);
		ItemStackUtils.setDisplayName(item, name);
		ItemStackUtils.addLoreLine(item, "§7§oĐây là một quặng hiếm");
		return item;
	}
	
	public static Material getOre(Material m) {
		switch (m) {
			case COAL_ORE: return Material.COAL;
			case IRON_ORE: return Material.IRON_INGOT;
			case GOLD_ORE: return Material.GOLD_INGOT;
			case DIAMOND_ORE: return Material.DIAMOND;
			case EMERALD_ORE: return Material.EMERALD;
			case LAPIS_ORE: return Material.LAPIS_LAZULI;
			case REDSTONE_ORE: return Material.REDSTONE;
			default: return null;
		}
	}
	
	public static void playerBreak(BlockBreakEvent e) {
		Block block = e.getBlock();
		if (MainSky2RPGCore.SPECIAL_MINE_WORLDS.contains(block.getWorld().getName())) {
			Material from = block.getType();
			Material ore = getOre(from);
			if (ore != null) {
				e.setCancelled(true);
				ItemStack item = getItem(ore);
				Item i = block.getWorld().dropItem(block.getLocation(), item);
				i.setCustomName(ItemStackUtils.getName(item));
				i.setCustomNameVisible(true);
				block.setType(Material.STONE);
				
				new BukkitRunnable() {			
					@Override
					public void run() {
						while (block.getType() == Material.STONE) {
							block.setType(from);
							return;
						}
						this.cancel();
					}
				}.runTaskTimer(MainSky2RPGCore.getMain(), 600, 20);
				
			}
		}
	}
	
}
