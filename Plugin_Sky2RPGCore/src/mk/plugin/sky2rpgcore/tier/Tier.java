package mk.plugin.sky2rpgcore.tier;

public enum Tier {
	
	SO_CAP("Sơ cấp", "§7", "I", 1),
	TRUNG_CAP("Trung cấp", "§9", "II", 2),
	CAO_CAP("Cao cấp", "§c", "III", 3),
	CUC_PHAM("Cực phẩm", "§6", "IV", 4),
	HUYEN_THOAI("Huyền thoại", "§e", "V", 5);
	
	private String color;
	private String icon;
	private String name;
	private int bonus;
	
	private Tier(String name, String color, String icon, int bonus) {
		this.color = color;
		this.icon = icon;
		this.name = name;
		this.bonus = bonus;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getColor() {
		return this.color;
	}
	
	public String getIcon() {
		return this.icon;
	}
	
	public int getBonus() {
		return this.bonus;
	}
	
}
