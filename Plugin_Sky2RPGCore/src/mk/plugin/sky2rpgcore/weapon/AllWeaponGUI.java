package mk.plugin.sky2rpgcore.weapon;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.utils.GUIUtils;

public class AllWeaponGUI {
	
	public static final String TITLE = "§c§lVŨ KHÍ SKYRPG";
	
	public static void openGUI(Player player, int page) {
		int isize = 54;
		int amount = WeaponType.values().length * 5;
		int max = (amount / isize + 1);
		int end = amount - 1;
		if (page > max) {
			player.sendMessage("§4Không tồn tại trang " + page);
			return;
		}
		
		Inventory inv = Bukkit.createInventory(null, isize, TITLE + " [" + page + "/" + max + "]");
		player.openInventory(inv);
		
		Bukkit.getScheduler().runTaskAsynchronously(MainSky2RPGCore.getMain(), () -> {
			for (int i = 0 ; i < isize ; i++) {
				inv.setItem(i, GUIUtils.getBlackSlot());
			}
			int i = 0;
			while (i <= end + 1) {
				int j = 0;
				while (j < 5 && i <= end) {
					j++;
					inv.setItem(i % isize, WeaponUtils.getWeapon(player, WeaponType.values()[i / 5], Grade.fromNumber(j), 3 * j));
					i++;
				}
			}
		});
	}
	
	public static void eventClick(InventoryClickEvent e) {
		if (e.getView().getTitle().contains(TITLE)) e.setCancelled(true);
	}
	
}
