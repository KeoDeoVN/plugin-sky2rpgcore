package mk.plugin.sky2rpgcore.weapon;

public enum Grade {
	
	I(1, 1, "§7"),
	II(2, 1.4, "§9"),
	III(3, 1.8, "§c"),
	IV(4, 2.2, "§6"),
	V(5, 2.6, "§e");
	
	private int n;
	private double bonus;
	private String color;
	
	private Grade(int n, double bonus, String color) {
		this.n = n;
		this.bonus = bonus;
		this.color = color;
	}
	
	public int getNumber() {
		return this.n;
	}
	
	public double getBonus() {
		return this.bonus;
	}
	
	public String getColor() {
		return this.color;
	}
	
	public static Grade fromNumber(int number) {
		for (Grade g : values()) {
			if (g.getNumber() == number) return g;
		}
		return null;
	}
	
}
