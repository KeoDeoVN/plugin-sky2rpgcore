package mk.plugin.sky2rpgcore.weapon;

import java.util.Map;

import com.google.common.collect.Maps;

import kdvn.sky2.rpg.core.item.SItem;
import kdvn.sky2.rpg.core.stat.SRPGStat;

public class Weapon {

	private WeaponType type;
	private SItem item;

	public Weapon(WeaponType type, SItem item) {
		this.type = type;
		this.item = item.clone();
		this.item.setName(type.getName());
		this.item.setDesc(type.getDesc());
		Map<SRPGStat, Integer> stats = Maps.newHashMap();
		type.getStats().forEach((stat, value) -> {
			stats.put(stat, value);
		});
		this.item.setStats(stats);
	}
	
	public WeaponType getType() {
		return this.type;
	}
	
	
	public SItem getItem() {
		return this.item;
	}
	

}
