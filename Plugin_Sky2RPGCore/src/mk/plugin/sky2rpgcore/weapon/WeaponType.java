package mk.plugin.sky2rpgcore.weapon;

import java.util.List;
import java.util.Map;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import kdvn.sky2.rpg.core.skill.ISRPGSkill;
import kdvn.sky2.rpg.core.stat.SRPGStat;
import mk.plugin.sky2rpgcore.weapon.skill.WSBaoKiem;
import mk.plugin.sky2rpgcore.weapon.skill.WSChiaCat;
import mk.plugin.sky2rpgcore.weapon.skill.WSDapRiu;
import mk.plugin.sky2rpgcore.weapon.skill.WSDienTinh;
import mk.plugin.sky2rpgcore.weapon.skill.WSKhieuKhich;
import mk.plugin.sky2rpgcore.weapon.skill.WSQuetKiem;
import mk.plugin.sky2rpgcore.weapon.skill.WSTanXaTien;
import mk.plugin.sky2rpgcore.weapon.skill.WSThauXuong;
import mk.plugin.sky2rpgcore.weapon.skill.WSThienPhat;

public enum WeaponType {
	
	// NORMAL
	STICK("NOR Gậy","Khiêu khích", new WSKhieuKhich(), Material.WOODEN_SWORD, "Vũ khi cơ bản, yếu, dùng chơi chơi thôi", Lists.newArrayList(31, 32, 33, 34, 35), Lists.newArrayList(SRPGStat.SAT_THUONG, SRPGStat.TOC_DANH, SRPGStat.CHI_MANG, SRPGStat.HUT_MAU), Lists.newArrayList(5, 5, 5, 5)),
	
	// VIP
	SWORD("VIP Kiếm", "Quét kiếm",new WSQuetKiem(), Material.WOODEN_SWORD, "Vũ khí cận chiến phổ thông với sức mạnh cân bằng", Lists.newArrayList(1, 2, 3, 4, 5), Lists.newArrayList(SRPGStat.SAT_THUONG, SRPGStat.TOC_DANH, SRPGStat.CHI_MANG, SRPGStat.XUYEN_GIAP), Lists.newArrayList(10, 10, 10, 10)),
	AXE("VIP Rìu", "Đập rìu", new WSDapRiu(), Material.WOODEN_SWORD, "Vũ khí cận chiến nặng gây nhiều sát thương hơn", Lists.newArrayList(6, 7, 8, 9, 10), Lists.newArrayList(SRPGStat.SAT_THUONG, SRPGStat.TOC_DANH, SRPGStat.CHI_MANG, SRPGStat.XUYEN_GIAP), Lists.newArrayList(15, 10, 10, 5)),
	BOW("VIP Cung", "Tán xạ tiễn", new WSTanXaTien(), Material.WOODEN_SWORD, "Vũ khí nhẹ tầm xa với đặc điểm tốc độ cao", Lists.newArrayList(11, 12, 13, 14, 15), Lists.newArrayList(SRPGStat.SAT_THUONG, SRPGStat.TOC_DANH, SRPGStat.CHI_MANG, SRPGStat.XUYEN_GIAP), Lists.newArrayList(10, 15, 10, 5)),
	
	// WOW
	SPEAR("WOW Thương", "Thấu xương", new WSThauXuong(), Material.WOODEN_SWORD, "Vũ khí cận chiến mạnh với độ chính xác cao", Lists.newArrayList(16, 17, 18, 19, 20), Lists.newArrayList(SRPGStat.SAT_THUONG, SRPGStat.TOC_DANH, SRPGStat.CHI_MANG, SRPGStat.XUYEN_GIAP), Lists.newArrayList(20, 10, 20, 10)),
	THIENKIEM("WOW Thiên kiếm", "Thiên phạt", new WSThienPhat(), Material.WOODEN_SWORD, "Tên đầy đủ là Thuận thiên kiếm với truyền thuyết oai hùng", Lists.newArrayList(36, 37, 38, 39, 40), Lists.newArrayList(SRPGStat.SAT_THUONG, SRPGStat.TOC_DANH, SRPGStat.CHI_MANG, SRPGStat.XUYEN_GIAP), Lists.newArrayList(20, 20, 20, 5)),
	
	// SUPER
	SCYTHE("SUPER Lưỡi hái", "Chia cắt", new WSChiaCat(), Material.WOODEN_SWORD, "Vũ khí của tử thần với sức mạnh to lớn", Lists.newArrayList(21, 22, 23, 24, 25), Lists.newArrayList(SRPGStat.SAT_THUONG, SRPGStat.TOC_DANH, SRPGStat.CHI_MANG, SRPGStat.XUYEN_GIAP), Lists.newArrayList(30, 15, 25, 15)),
	KATANA("SUPER Katana", "Bão kiếm", new WSBaoKiem(), Material.WOODEN_SWORD, "Vũ khí của ninja, samurai với sự ảo diệu và sắc bén ", Lists.newArrayList(26, 27, 28, 29, 30), Lists.newArrayList(SRPGStat.SAT_THUONG, SRPGStat.TOC_DANH, SRPGStat.CHI_MANG, SRPGStat.XUYEN_GIAP), Lists.newArrayList(25, 30, 15, 15)),
	AIKIEM("SUPER Ái kiếm", "Lưới tình", new WSDienTinh(), Material.WOODEN_SWORD, "Vũ khí đặc biệt, hiệu quả trong giao tranh", Lists.newArrayList(41, 42, 43, 44, 45),Lists.newArrayList(SRPGStat.SAT_THUONG, SRPGStat.TOC_DANH, SRPGStat.CHI_MANG, SRPGStat.HUT_MAU), Lists.newArrayList(35, 15, 10, 25));
	
	// LEGEND
	;
	
	private String name;
	private Material m;
	private List<Integer> dur;
	private Map<SRPGStat, Integer> basics;
	private String desc;
	private ISRPGSkill skill;
	private String skillName;
	
	private WeaponType(String name, String skillName, ISRPGSkill skill, Material m, String desc, List<Integer> dur, List<SRPGStat> stats, List<Integer> svalues) {
		this.name = name;
		this.skillName = skillName;
		this.m = m;
		this.desc = desc;
		this.dur = dur;
		this.basics = Maps.newHashMap();
		for (int i = 0 ; i < stats.size() ; i++) {
			basics.put(stats.get(i), svalues.get(i));
		}
		this.skill = skill;
	}

	public String getName() {
		return name;
	}

	public Material getMaterial() {
		return m;
	}

	public String getDesc() {
		return this.desc;
	}
	
	public List<Integer> getDurabilities() {
		return dur;
	}
	
	public ItemStack getTexture(int degree) {
		return new ItemStack(m, dur.get(degree) - 1);
	}
	
	public Map<SRPGStat, Integer> getStats() {
		return this.basics;
	}
	
	public ISRPGSkill getSkill() {
		return this.skill;
	}
	
	public String getSkillName() {
		return this.skillName;
	}
}
