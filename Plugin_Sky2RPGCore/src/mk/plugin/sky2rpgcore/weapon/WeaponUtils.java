package mk.plugin.sky2rpgcore.weapon;


import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.inventory.meta.ItemMeta;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import kdvn.sky2.rpg.core.item.SItem;
import kdvn.sky2.rpg.core.item.SRPGItemUtils;
import kdvn.sky2.rpg.core.utils.ItemStackUtils;

public class WeaponUtils {
	
	public static boolean isWeapon(ItemStack item) {
		if (ItemStackUtils.isNull(item)) return false;
		return ItemStackUtils.hasTag(item, "sRPG.weapontype");
	}
	
	public static void update(Player player, ItemStack is, Weapon weapon) {
		SRPGItemUtils.updateItem(player, is, weapon.getItem());
	}
	
	public static ItemStack set(Player player, ItemStack is, Weapon weapon) {
		is = SRPGItemUtils.setWithoutUpdate(player, is, weapon.getItem());
		is = ItemStackUtils.setTag(is, "sRPG.weapontype", weapon.getType().name());
		
		is.setType(weapon.getType().getMaterial());
		ItemMeta meta = is.getItemMeta();
		if (meta instanceof Damageable) {
			((Damageable) meta).setDamage(weapon.getType().getDurabilities().get(weapon.getItem().getGrade().getNumber() - 1));
		}
		is.setItemMeta(meta);
		
		update(player, is, weapon);
		
		return is;
	}
	
	public static Weapon fromItemStack(ItemStack is) {
		SItem item = SRPGItemUtils.fromItemStack(is);
		WeaponType type = WeaponType.valueOf(ItemStackUtils.getTag(is, "sRPG.weapontype"));
		return new Weapon(type, item);
	}
	
	public static ItemStack getWeapon(Player player, WeaponType type) {
		ItemStack is = new ItemStack(Material.WOODEN_SWORD);
		ItemStackUtils.setDisplayName(is, "cc");
		SItem si = new SItem(null, 0, 2, Maps.newHashMap(), Lists.newArrayList(), Maps.newHashMap(), null, Grade.I, 0);
		Weapon w = new Weapon(type, si);
		return WeaponUtils.set(player, is, w);
	}
	
	public static ItemStack getWeapon(Player player, WeaponType type, Grade grade, int level) {
		ItemStack is = new ItemStack(Material.WOODEN_SWORD);
		ItemStackUtils.setDisplayName(is, "cc");
		SItem si = new SItem(null, 0, 2, Maps.newHashMap(), Lists.newArrayList(), Maps.newHashMap(), null, Grade.I, 0);
		si.setLevel(level);
		si.setGrade(grade);
		Weapon w = new Weapon(type, si);
		return WeaponUtils.set(player, is, w);
	}
	
}
