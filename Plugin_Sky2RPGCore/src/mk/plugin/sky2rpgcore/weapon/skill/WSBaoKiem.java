package mk.plugin.sky2rpgcore.weapon.skill;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.skill.ISRPGSkill;
import kdvn.sky2.rpg.core.utils.SRPGUtils;

public class WSBaoKiem implements ISRPGSkill {

	@Override
	public String getDesc(int lv) {
		return null;
	}

	@Override
	public boolean hasCombo() {
		return false;
	}

	@Override
	public void start(Player player, int lv) {
		double damage =	0;  
		Location location = player.getLocation();
		new BukkitRunnable() {
			int i = 0;
			@Override
			public void run() {
				i++;
				if (i >= 15) {
					this.cancel();
					return;
				}
				Location newLocation = location.clone().add(location.getDirection().multiply(i * 1.8));
				player.playSound(newLocation, Sound.ENTITY_GHAST_SHOOT, 0.5f, 1.5f);

				int i = 0;
				List<Location> list = getListLocation(newLocation);
				for (i = 0 ; i < list.size() ; i ++) {
					player.getWorld().spawnParticle(Particle.SWEEP_ATTACK, list.get(i), new Double(i / 3).intValue(), i*0.05f, i*0.05f, i*0.05f, i*0.01f);
				}
				for (Entity e : location.getWorld().getNearbyEntities(newLocation, 2, 5, 2)) {
					if (e instanceof LivingEntity && e != player) {
						if (e.hasMetadata("NPC")) continue;
						new BukkitRunnable() {
							@Override
							public void run() {
								SRPGUtils.damage((LivingEntity) e, player, damage, 5, MainSky2RPGCore.getMain());
								e.setVelocity(new Vector(0, 0.7, 0));
							}
						}.runTask(MainSky2RPGCore.getMain());
					}
				}
			}
		}.runTaskTimerAsynchronously(MainSky2RPGCore.getMain(), 0, 2);
	}
	
	private List<Location> getListLocation(Location main) {
		List<Location> list = new ArrayList<Location> ();
		Location newLocation = main.clone().add(0, -2, 0);
		for (int i = 1 ; i < 15; i ++) {
			list.add(newLocation.clone().add(0, 0.5f * i, 0));
		}
		return list;
	}

}
