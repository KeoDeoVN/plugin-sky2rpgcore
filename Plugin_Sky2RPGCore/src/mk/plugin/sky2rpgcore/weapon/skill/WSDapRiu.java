package mk.plugin.sky2rpgcore.weapon.skill;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_13_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;

import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.skill.ISRPGSkill;
import kdvn.sky2.rpg.core.utils.SRPGUtils;
import net.minecraft.server.v1_13_R2.PacketPlayOutAnimation;

public class WSDapRiu implements ISRPGSkill {

	@Override
	public String getDesc(int lv) {
		return null;
	}

	@Override
	public boolean hasCombo() {
		return false;
	}

	@Override
	public void start(Player player, int lv) {
		double damage = 0;
		
		player.spawnParticle(Particle.SWEEP_ATTACK, player.getLocation().add(0, 1.2, 0).add(player.getLocation().getDirection().multiply(1.2)), 1, 0, 0, 0, 0);
		player.getWorld().playSound(player.getLocation(), Sound.ENTITY_GHAST_SHOOT, 1f, 1f);
		
		PacketPlayOutAnimation packet = new PacketPlayOutAnimation(((CraftPlayer) player).getHandle(), (byte) 0);
		((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
		
		Location main = player.getLocation().add(player.getLocation().getDirection().multiply(1));
		SRPGUtils.getLivingEntities(player, main, 3, 3, 3).forEach(le -> {
			if (le.hasMetadata("NPC")) return;
			Bukkit.getScheduler().runTask(MainSky2RPGCore.getMain(),() -> {
				SRPGUtils.syncDamage(le, player, damage, 5);
			});

			player.spawnParticle(Particle.CRIT_MAGIC, le.getLocation(), 10, 0.2, 0.2, 0.2, 1);
			player.spawnParticle(Particle.CRIT, le.getLocation(), 10, 0.2, 0.2, 0.2, 1);
			le.setVelocity(le.getLocation().subtract(player.getLocation()).toVector().normalize().multiply(1.2).setY(1));
		});
	}

}
