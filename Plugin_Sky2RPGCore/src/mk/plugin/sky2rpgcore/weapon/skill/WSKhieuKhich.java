package mk.plugin.sky2rpgcore.weapon.skill;

import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;

import kdvn.sky2.rpg.core.skill.ISRPGSkill;

public class WSKhieuKhich implements ISRPGSkill {

	@Override
	public String getDesc(int lv) {
		return null;
	}

	@Override
	public boolean hasCombo() {
		return false;
	}

	@Override
	public void start(Player player, int lv) {
		player.getNearbyEntities(5, 5, 5).forEach(entity -> {
			if (entity instanceof Monster) {
				((Monster) entity).setTarget(player);
			}
		});
	}

}
