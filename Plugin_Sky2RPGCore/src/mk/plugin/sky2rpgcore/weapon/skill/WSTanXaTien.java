package mk.plugin.sky2rpgcore.weapon.skill;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.skill.ISRPGSkill;
import kdvn.sky2.rpg.core.utils.SRPGUtils;
import net.minecraft.server.v1_13_R2.SoundEffects;

public class WSTanXaTien implements ISRPGSkill {

	@Override
	public String getDesc(int lv) {
		return null;
	}

	@Override
	public boolean hasCombo() {
		return false;
	}

	@Override
	public void start(Player player, int lv) {
		int amount = 9;
		
		double angleBetweenArrows = (45 / (amount - 1)) * Math.PI / 180;
		double pitch = (player.getLocation().getPitch() + 90) * Math.PI / 180;
		double yaw = (player.getLocation().getYaw() + 90 - 45 / 2) * Math.PI / 180;
		double sZ = Math.cos(pitch);

		List<Arrow> as = new ArrayList<Arrow> ();
		
		for (int i = 0; i < amount; i++) { 	
			double nX = Math.sin(pitch)	* Math.cos(yaw + angleBetweenArrows * i);
			double nY = Math.sin(pitch)* Math.sin(yaw + angleBetweenArrows * i);
			Vector newDir = new Vector(nX, sZ, nY);

			Arrow arrow = player.launchProjectile(Arrow.class);
			arrow.setShooter(player);
			arrow.setVelocity(newDir.normalize().multiply(3f));
			arrow.setFireTicks(1000);
			as.add(arrow);
		}
		
		SRPGUtils.sendSound(player, SoundEffects.ENTITY_ARROW_SHOOT, 1, 1);
		
		new BukkitRunnable() {
			@Override
			public void run() {
				for (Arrow a : as) {
					a.remove();
				}
			}
		}.runTaskLater(MainSky2RPGCore.getMain(), 10);
	}

}
