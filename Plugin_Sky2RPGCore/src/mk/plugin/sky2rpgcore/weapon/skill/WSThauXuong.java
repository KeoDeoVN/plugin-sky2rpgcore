package mk.plugin.sky2rpgcore.weapon.skill;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_13_R2.entity.CraftPlayer;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.skill.ISRPGSkill;
import kdvn.sky2.rpg.core.utils.SRPGUtils;
import net.minecraft.server.v1_13_R2.PacketPlayOutAnimation;

public class WSThauXuong implements ISRPGSkill {

	@Override
	public String getDesc(int lv) {
		return null;
	}

	@Override
	public boolean hasCombo() {
		return false;
	}

	@Override
	public void start(Player player, int lv) {
		double damage = 0;
		
		new BukkitRunnable() {
			int i = 0;
			@Override
			public void run() {
				i++;
				if (i > 3) {
					this.cancel();
					return;
				}
				PacketPlayOutAnimation packet = new PacketPlayOutAnimation(((CraftPlayer) player).getHandle(), (byte) 0);
				((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
				Location mainLoc = player.getLocation().add(0, 0.9, 0);
				player.playSound(player.getLocation(), Sound.BLOCK_ANVIL_PLACE, 0.2f, 1.1f);
				for (int i = 0 ; i < 10; i ++) {
					Location loc =  mainLoc.clone().add(mainLoc.getDirection().multiply(i));
					player.getWorld().spawnParticle(Particle.CRIT, loc, 4, 0.1f, 0.1f, 0.1f, 0);
					player.getWorld().spawnParticle(Particle.CRIT_MAGIC, loc, 4, 0.1f, 0.1f, 0.1f, 0);
					loc.getWorld().getNearbyEntities(loc, 1, 1, 1).forEach(e -> {
						if (e instanceof LivingEntity && e != player) {
							Bukkit.getScheduler().runTask(MainSky2RPGCore.getMain(), () -> {
								SRPGUtils.damage((LivingEntity) e, player, damage, 5, MainSky2RPGCore.getMain());
							});
						}
					});
				}
			}
			
		}.runTaskTimerAsynchronously(MainSky2RPGCore.getMain(), 0, 10);
		
		
	}

}
