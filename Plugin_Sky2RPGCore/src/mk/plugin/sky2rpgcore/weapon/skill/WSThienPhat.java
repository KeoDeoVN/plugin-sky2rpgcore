package mk.plugin.sky2rpgcore.weapon.skill;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.skill.ISRPGSkill;
import kdvn.sky2.rpg.core.utils.SRPGUtils;

public class WSThienPhat implements ISRPGSkill {

	@Override
	public String getDesc(int lv) {
		return null;
	}

	@Override
	public boolean hasCombo() {
		return false;
	}

	@Override
	public void start(Player player, int lv) {
		double damage =	0;
		
		new BukkitRunnable () {
			Location lo = player.getLocation();
			int i = 0;
			@Override
			public void run() {
				if (i == 4) {
					this.cancel();
				}
				i++;
				int j = 0;
				Location temp = lo.add(lo.getDirection().multiply(i * 0.8)).clone();
				while (temp.getBlock().getType() == Material.AIR) {
					j++;
					if (j > 10) {
						this.cancel();
						break;
					}
					temp = temp.add(0,-1,0);
				}
				player.getWorld().spawnParticle(Particle.EXPLOSION_LARGE, temp.clone().add(0, 0.7, 0), 1, 0, 0, 0, 0);
				
				final Location tempL = temp;
				
				new BukkitRunnable () {
					@Override
					public void run() {
						player.getWorld().strikeLightningEffect(tempL);
					}
				}.runTaskAsynchronously(MainSky2RPGCore.getMain());

				new BukkitRunnable() {
					@Override
					public void run() {
						for (Entity e : tempL.getWorld().getNearbyEntities(tempL, 2, 10, 2)) {
							if (e instanceof LivingEntity && e != player) {
								SRPGUtils.damage((LivingEntity) e, player, damage, 20, MainSky2RPGCore.getMain());
							}
						}
					}
				}.runTask(MainSky2RPGCore.getMain());

			}
		}.runTaskTimerAsynchronously(MainSky2RPGCore.getMain(), 0, 5);
	}

}
