package mk.plugn.sky2rpgcore.itemgui;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Lists;

import kdvn.sky2.rpg.core.gui.chuyenhoa.SRPGGUIChuyenHoa;
import kdvn.sky2.rpg.core.gui.cuonghoa.SRPGGUICuongHoa;
import kdvn.sky2.rpg.core.gui.duclo.SRPGGUIDucLo;
import kdvn.sky2.rpg.core.gui.enchant.SRPGGUIEnchant;
import kdvn.sky2.rpg.core.gui.epda.SRPGGUIEpDa;
import kdvn.sky2.rpg.core.gui.tachda.SRPGGUITachDa;
import kdvn.sky2.rpg.core.gui.tachphuphep.SRPGGUITachPhuPhep;
import kdvn.sky2.rpg.core.item.SItem;
import kdvn.sky2.rpg.core.item.SItemUtils;
import kdvn.sky2.rpg.core.item.SRPGItemUtils;
import kdvn.sky2.rpg.core.main.MainSky2RPGCore;
import kdvn.sky2.rpg.core.power.SRPGPowerUtils;
import kdvn.sky2.rpg.core.stat.SRPGStat;
import kdvn.sky2.rpg.core.utils.GUIUtils;
import kdvn.sky2.rpg.core.utils.ItemStackUtils;
import kdvn.sky2.rpg.core.utils.SRPGUtils;
import mk.plugin.sky2rpgcore.nangbac.GradeGUI;
import net.minecraft.server.v1_13_R2.SoundEffects;

public class SItemGUI {
	
	public static final int ITEM_SLOT = 22;
	public static final int INFO_SLOT_1 = 10;
	public static final int INFO_SLOT_2 = 19;
	public static final int INFO_SLOT_3 = 28;
	public static final int INFO_SLOT_4 = 37;
	public static final int NANGBAC_SLOT = 15;
	public static final int CUONGHOA_SLOT = 16;
	public static final int CHUYENHOA_SLOT = 25;
	public static final int KHAMNGOC_SLOT = 24;
	public static final int TACHNGOC_SLOT = 33;
	public static final int DUCLO_SLOT = 34;
	public static final int PHUPHEP_SLOT = 42;
	public static final int TACHPP_SLOT = 43;
	
	public static final String TITLE = "§2§lTHÔNG TIN TRANG BỊ";
	
	public static void openGUI(Player player, ItemStack item) {
		Inventory inv = Bukkit.createInventory(null, 54, TITLE);
		player.openInventory(inv);
		
		Bukkit.getScheduler().runTaskAsynchronously(MainSky2RPGCore.getMain(), () -> {
			SRPGUtils.sendSound(player, SoundEffects.BLOCK_ENDER_CHEST_OPEN, 1, 1);
			for (int i = 0 ; i < inv.getSize() ;  i++) {
				inv.setItem(i, GUIUtils.getBlackSlot());
			}
			inv.setItem(ITEM_SLOT, GUIUtils.getItemSlot(new ItemStack(Material.LIME_STAINED_GLASS_PANE, 1), "§aÔ chứa §oVật phẩm"));
			SItem si = SItemUtils.fromItemStack(item);
			if (item != null) inv.setItem(ITEM_SLOT, item);
			inv.setItem(INFO_SLOT_1, getInfo1(player, si));
			inv.setItem(INFO_SLOT_2, getInfo2(player, si));
			inv.setItem(INFO_SLOT_3, getInfo3(player, si));
			inv.setItem(INFO_SLOT_4, getInfo4(player, si));
			inv.setItem(NANGBAC_SLOT, getNangBac());
			inv.setItem(CUONGHOA_SLOT, getCuongHoa());
			inv.setItem(CHUYENHOA_SLOT, getChuyenHoa());
			inv.setItem(KHAMNGOC_SLOT, getKhamNgoc());
			inv.setItem(TACHNGOC_SLOT, getTachNgoc());
			inv.setItem(DUCLO_SLOT, getDucLo());
			inv.setItem(PHUPHEP_SLOT, getPhuPhep());
			inv.setItem(TACHPP_SLOT, getTachPhuPhep());
		});
	}
	
	public static void openGUI(Player player) {
		openGUI(player, null);
	}
	
	public static void eventClick(InventoryClickEvent e) {
		if (e.getClickedInventory() == null) return;
		if (!e.getView().getTitle().equals(TITLE)) return;
		e.setCancelled(true);
		Inventory inv = e.getWhoClicked().getOpenInventory().getTopInventory();
		
		Player player = (Player) e.getWhoClicked();
		int slot = e.getSlot();
		
		ItemStack item = GUIUtils.getItem(inv, ITEM_SLOT);
		
		boolean canPress = true;
		if (item != null && !SItemUtils.isRPGItem(item)) canPress = false; 
		
		if (!canPress) {
			player.sendMessage("§cVật phẩm không đúng, hãy để lại");
			return;
		}
		
		// Action on top inv
		if (e.getClickedInventory() == player.getOpenInventory().getTopInventory()) {
			switch (slot) {
				case NANGBAC_SLOT:
					inv.setItem(ITEM_SLOT, null);
					GradeGUI.openGUI(player, item);
					break;
				case CHUYENHOA_SLOT:
					inv.setItem(ITEM_SLOT, null);
					SRPGGUIChuyenHoa.openGUI(player, item);
					break;
				case CUONGHOA_SLOT:
					inv.setItem(ITEM_SLOT, null);
					SRPGGUICuongHoa.openGUI(player, item);
					break;
				case DUCLO_SLOT:
					inv.setItem(ITEM_SLOT, null);
					SRPGGUIDucLo.openGUI(player, item);
					break;
				case KHAMNGOC_SLOT:
					inv.setItem(ITEM_SLOT, null);
					SRPGGUIEpDa.openGUI(player, item);
					break;
				case PHUPHEP_SLOT:
					inv.setItem(ITEM_SLOT, null);
					SRPGGUIEnchant.openGUI(player, item);
					break;
				case TACHNGOC_SLOT:
					inv.setItem(ITEM_SLOT, null);
					SRPGGUITachDa.openGUI(player, item);
					break;
				case TACHPP_SLOT:
					inv.setItem(ITEM_SLOT, null);
					SRPGGUITachPhuPhep.openGUI(player, item);
					break;
			}
		} 
		// Action on bottom inv
		else {
			if (e.getClick() == ClickType.SHIFT_LEFT) {
				SRPGUtils.sendSound(player, SoundEffects.BLOCK_LEVER_CLICK, 1f, 1f);
				Inventory bInv = player.getOpenInventory().getBottomInventory();
				ItemStack clickedItem = bInv.getItem(slot);
				
				// Item
				if (SRPGItemUtils.isRPGItem(clickedItem)) {
					SItem si = SItemUtils.fromItemStack(clickedItem);
					if (!GUIUtils.placeItem(clickedItem, slot, inv, bInv, ITEM_SLOT)) {
						player.sendMessage("§cĐã để Vật phẩm rồi");
						return;
					}
					inv.setItem(INFO_SLOT_1, getInfo1(player, si));
					inv.setItem(INFO_SLOT_2, getInfo2(player, si));
					inv.setItem(INFO_SLOT_3, getInfo3(player, si));
					inv.setItem(INFO_SLOT_4, getInfo4(player, si));
				}				
			} else player.sendMessage("§cẤn §fShift + Chuột trái §cđể đặt item");
		}
	}
	
	public static void eventClose(InventoryCloseEvent e) {
		Player player = (Player) e.getPlayer();
		Inventory inv = e.getInventory();
		if (!e.getView().getTitle().equals(TITLE)) return;
		
		if (!GUIUtils.checkCheck(player)) return;
		
		List<ItemStack> items = new ArrayList<ItemStack> ();
		items.add(inv.getItem(ITEM_SLOT));
		
		items.forEach(item -> {
			if (item == null || ItemStackUtils.hasTag(item, GUIUtils.SLOT_ITEM_TAG)) return;
			SRPGUtils.giveItem(player, item);
		});
	}
	
	public static ItemStack getInfo1(Player player, SItem si) {
		ItemStack item = new ItemStack(Material.PAPER);
		ItemStackUtils.setDisplayName(item, "§e§lThông tin 1");
		if (si == null) return item;
		String name = si.getName();
		String bac = si.getGrade().name();
		int level = si.getLevel();
		List<String> lore = Lists.newArrayList();
		lore.add("§aTên trang bị: §f" + name);
		lore.add("§aBậc trang bị: §f" + bac + "/V");
		lore.add("§aCấp độ trang bị: §f" + level + "/15");
		ItemStackUtils.setLore(item, lore);
		return item;
	}
	
	public static ItemStack getInfo2(Player player, SItem si) {
		ItemStack item = new ItemStack(Material.APPLE);
		ItemStackUtils.setDisplayName(item, "§e§lThông tin 2");
		if (si == null) return item;
		List<String> lore = Lists.newArrayList();
		for (SRPGStat stat : SRPGStat.values()) {
			lore.add("§a" + stat.getName() + ": §f" + si.getStat(stat) + " §7(+" + si.getBonusStat(player, stat) + ")");
		}
		ItemStackUtils.setLore(item, lore);
		return item;
	}
	
	public static ItemStack getInfo3(Player player, SItem si) {
		ItemStack item = new ItemStack(Material.BLAZE_POWDER);
		ItemStackUtils.setDisplayName(item, "§e§lThông tin 3");
		if (si == null) return item;
		ItemStackUtils.setDisplayName(item, "§e§lThông tin 3");
		List<String> lore = Lists.newArrayList();
		int power = 0;
		for (SRPGStat stat : SRPGStat.values()) power += SRPGPowerUtils.getPower(stat) * si.getCalculatedStat(player, stat);
		lore.add("§aLực chiến trang bị: §f" + power);
		ItemStackUtils.setLore(item, lore);
		return item;
	}
	
	public static ItemStack getInfo4(Player player, SItem si) {
		ItemStack item = new ItemStack(Material.BOOK);
		ItemStackUtils.setDisplayName(item, "§e§lThông tin 4");
		if (si == null) return item;
		ItemStackUtils.setDisplayName(item, "§e§lThông tin 4");
		List<String> lore = Lists.newArrayList();
		lore.add("§aĐể tăng sức mạnh trang bị bạn có thể: ");
		lore.add("§6 1. Nâng bậc");
		lore.add("§6 2. Cường hóa");
		lore.add("§6 3. Khảm ngọc");
		lore.add("§6 4. Phù phép");
		ItemStackUtils.setLore(item, lore);
		return item;
	}
	
	public static ItemStack getNangBac() {
		ItemStack item = new ItemStack(Material.END_PORTAL_FRAME);
		ItemStackUtils.setDisplayName(item, "§a§lNâng bậc");
		List<String> lore = Lists.newArrayList();
		lore.add("§7§oClick để mở GUI");
		ItemStackUtils.setLore(item, lore);
		return item;
	}
	
	public static ItemStack getCuongHoa() {
		ItemStack item = new ItemStack(Material.ANVIL);
		ItemStackUtils.setDisplayName(item, "§a§lCường hóa");
		List<String> lore = Lists.newArrayList();
		lore.add("§7§oClick để mở GUI");
		ItemStackUtils.setLore(item, lore);
		return item;
	}
	
	public static ItemStack getChuyenHoa() {
		ItemStack item = new ItemStack(Material.BREWING_STAND);
		ItemStackUtils.setDisplayName(item, "§a§lChuyển hóa");
		List<String> lore = Lists.newArrayList();
		lore.add("§7§oClick để mở GUI");
		ItemStackUtils.setLore(item, lore);
		return item;
	}
	
	public static ItemStack getKhamNgoc() {
		ItemStack item = new ItemStack(Material.IRON_NUGGET);
		ItemStackUtils.setDisplayName(item, "§a§lKhảm ngọc");
		List<String> lore = Lists.newArrayList();
		lore.add("§7§oClick để mở GUI");
		ItemStackUtils.setLore(item, lore);
		return item;
	}
	
	public static ItemStack getTachNgoc() {
		ItemStack item = new ItemStack(Material.CAULDRON);
		ItemStackUtils.setDisplayName(item, "§a§lTách ngọc");
		List<String> lore = Lists.newArrayList();
		lore.add("§7§oClick để mở GUI");
		ItemStackUtils.setLore(item, lore);
		return item;
	}
	
	public static ItemStack getDucLo() {
		ItemStack item = new ItemStack(Material.HOPPER);
		ItemStackUtils.setDisplayName(item, "§a§lĐục lỗ");
		List<String> lore = Lists.newArrayList();
		lore.add("§7§oClick để mở GUI");
		ItemStackUtils.setLore(item, lore);
		return item;
	}
	
	public static ItemStack getPhuPhep() {
		ItemStack item = new ItemStack(Material.ENCHANTED_BOOK);
		ItemStackUtils.setDisplayName(item, "§a§lPhù phép");
		List<String> lore = Lists.newArrayList();
		lore.add("§7§oClick để mở GUI");
		ItemStackUtils.setLore(item, lore);
		return item;
	}
	
	public static ItemStack getTachPhuPhep() {
		ItemStack item = new ItemStack(Material.BOOK);
		ItemStackUtils.setDisplayName(item, "§a§lTách phù phép");
		List<String> lore = Lists.newArrayList();
		lore.add("§7§oClick để mở GUI");
		ItemStackUtils.setLore(item, lore);
		return item;
	}
	
}
